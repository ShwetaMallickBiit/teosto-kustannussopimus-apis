/*
Description
            Class synchronizes Asiakas and Esityspaikka fields on Esityspaikka object based on inactive Esityspaikkahistoria records
*/
global class KAYT_Batch_synkEP_EPHistoriaInaktiiv implements Database.stateful, Database.Batchable<sObject>{

    public Database.QueryLocator start(Database.BatchableContext bc) {

        String soql = '';
        soql = 'SELECT Id, Asiakas__c, Sopimus__c ' +
               'FROM Esityspaikka__c ' +
               'WHERE (Sopimus__c != null OR Asiakas__c != null) AND Id NOT IN(SELECT Esityspaikka__c FROM Esityspaikkahistoria__c WHERE x_Aktiivinen__c = TRUE)';
        return Database.getQueryLocator(soql);
    }

    public void execute(Database.BatchableContext bc, List<Esityspaikka__c> eps) {

        for (Esityspaikka__c ep : eps) {
            ep.Asiakas__c = null;
            ep.Sopimus__c = null;
        }

        try {
            update eps;
        }
        catch(Exception e) {
            System.debug(e.getMessage());
            Loki__c errorLog = new Loki__c(Objekti__c = 'Esityspaikka__c / Esitypaikkahistoria__c', Toiminnallisuus__c = 'KAYT_Batch_synkEP_EPHistoriaInaktiiv', Tyyppi__c = 'Virhe', Virheviesti__c = e.getMessage());
            insert errorLog;
        }

    }
    public void finish(Database.BatchableContext bc) {

    }
}