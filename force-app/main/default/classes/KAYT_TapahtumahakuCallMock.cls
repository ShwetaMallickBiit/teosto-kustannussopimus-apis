// Tatjana Podvisocka; 17/10/2017

	@isTest
	public class KAYT_TapahtumahakuCallMock implements HttpCalloutMock{
    
        public String type;
	    public KAYT_TapahtumahakuCallMock(final String type){
	             this.type = type;
	    }
 	public HttpResponse respond(HTTPRequest req)
    {
        HttpResponse res = new HttpResponse();
        res.setStatusCode(200);        
        res.setHeader('Content-Type', 'application/json');
        
        res.setStatusCode(200); 
        string body1='[{"tilnro":"1529260", "alkupvm":"2017-03-11", "loppupvm":"2017-03-11", "tilnimi":"TSK Turku Accordeon", "paatilnro":"0","asiakasnro":"70017491",'+
        '"kayttajanro":"70060635","lomakeno":"49807","tuote":"TAPLUPA","tuoteostoid":"815AF23C7AB3D964C22580DD00255CA9","tiltyyppi":"", "luonne":"", "lahde":"WL"}]';
        string body2='[]';
        res.setBody(type=='Pass' ? body1 : body2);
              
        return res;
    }   
    
}