@isTest

public with sharing class SELFADMIN_Test {

    @isTest
    static void getAdministrationNotificationsForPerioidTest() {
        System.Debug('Running getAdministrationNotificationsForPerioidTest');

        Test.setMock(HttpCalloutMock.class, new MockResponseNotificationsList());

        Test.startTest();
        try{
            SELFADMIN_Notification_Controller.getAdministrationNotificationsForPerioid(Date.today(), Date.today(), 'OH');
        } catch (Exception e) {
            System.assert (false, 'should not reach here. ' + e.getMessage());
        }
        Test.stopTest();

        try {
            Itsehallinnointi__c ih = [SELECT Name FROM Itsehallinnointi__c WHERE Tunniste__c = '100101'];
            System.assertEquals('Nalle maailmalla', ih.Name, 'IH has wrong name.');
        } catch (Exception e) {
            System.assert (false, 'should not reach here. ' + e.getMessage());
        }

    }

    @isTest
    static void getAdministrationNotificationByCustomerNumberTest() {
        System.Debug('Running getAdministrationNotificationByCustomerNumberTest');

        Test.setMock(HttpCalloutMock.class, new MockResponseNotificationsList());

        Test.startTest();
        try{
            SELFADMIN_Notification_Controller.getAdministrationNotificationByCustomerNumber(123, 'P');
        } catch (Exception e) {
            System.assert (false, 'should not reach here. ' + e.getMessage());
        }
        Test.stopTest();

        try {
            Itsehallinnointi__c ih = [SELECT Name FROM Itsehallinnointi__c WHERE Tunniste__c = '100101'];
            System.assertEquals('Nalle maailmalla', ih.Name, 'IH has wrong name.');
        } catch (Exception e) {
            System.assert (false, 'should not reach here. ' + e.getMessage());
        }

    }

    @isTest
    static void updateNotificationStatusTest() {
        System.Debug('Running updateNotificationStatusTest');

        Test.setMock(HttpCalloutMock.class, new MockResponseUpdateNotificationStatus());

        Itsehallinnointi__c ih = new Itsehallinnointi__c(Name = 'Ih name', Status__c = 'Draft', NotificationId__c = 12345, Tunniste__c = '12345');
        insert ih;

        Test.startTest();
        try{
            Boolean ret = SELFADMIN_Notification_Controller.updateNotificationStatus(ih);
            System.assertEquals(true, ret, 'Updating notification status failed.');
        } catch (Exception e) {
            System.assert (false, 'should not reach here. ' + e.getMessage());
        }
        Test.stopTest();
    }

    @isTest
    static void updateEmailSendTest() {
        System.Debug('Running updateEmailSendTest');

        Test.setMock(HttpCalloutMock.class, new MockResponseUpdateEmailSend());

        Itsehallinnointi__c ih = new Itsehallinnointi__c(Name = 'Ih name', Status__c = 'Draft', NotificationId__c = 12345, Tunniste__c = '12345');
        insert ih;

        Test.startTest();
        try{
            Boolean ret = SELFADMIN_Notification_Controller.updateEmailSend(ih, new List<String> {'1', '2', '3'});
            System.assertEquals(true, ret, 'Updating email send failed.');
        } catch (Exception e) {
            System.assert (false, 'should not reach here. ' + e.getMessage());
        }
        Test.stopTest();
    }


    @isTest
    static void notificationBatchTest() {
        System.Debug('Running notificationBatchTest');

        Test.setMock(HttpCalloutMock.class, new MockResponseNotificationsList());
        List<Itsehallinnointisahkoposti__mdt> metadatas = 
            [SELECT MasterLabel, DeveloperName, Haku__c, Sahkopostilomake_FIN__c, Sahkopostilomake_SWE__c, Sahkopostilomake_EN__c, LahetaSahkopostiin__c, Lahetysviive__c, EraajonKoko__c, LahettavaSahkopostiosoite__c 
             FROM Itsehallinnointisahkoposti__mdt
             WHERE Aktiivinen__c = TRUE AND LahetaSahkopostiin__c = TRUE LIMIT 1];
        Account acc = GEN_TestFactory.createAccountTekija();
        Sopimustieto__c sop = GEN_TestFactory.createSopimusTekija(acc.Id);
        Itsehallinnointi__c ih = GEN_TestFactory.createItsehallinnointi(acc.Id, sop.Id);
        GEN_TestFactory.createAdditionalEmail(ih.Id);
        
        Test.startTest();
        try{
            SELFADMIN_NotificationEmailBatch b = new SELFADMIN_NotificationEmailBatch(metadatas[0]);
            system.scheduleBatch(b, metadatas[0].DeveloperName, 0, 1);
            List<sObject> authors = New List<sObject>();
            SELFADMIN_Notification_Handler.getEmailMap(authors);
        } catch (Exception e) {
            System.assert (false, 'should not reach here. ' + e.getMessage());
        }
        Test.stopTest();

        List<SahkopostiIlmoitus__c> sis = [SELECT Id FROM SahkopostiIlmoitus__c];
//        System.assert (sis.size() > 0, 'Could not find any SahkopostiIlmoitus__c records after running scheduler.');
    }

    @isTest
    static void notificationFetchSchedulerTest() {
        System.Debug('Running notificationFetchSchedulerTest');
        Test.setMock(HttpCalloutMock.class, new MockResponseNotificationsList());
        Test.startTest();
        try{
            SELFADMIN_Notification_Scheduler schNotifications = new SELFADMIN_Notification_Scheduler();
            schNotifications.execute(null);
        } catch (Exception e) {
            System.assert (false, 'should not reach here. ' + e.getMessage());
        }
        Test.stopTest();
    }

    public class MockResponseNotificationsList implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HTTPResponse res = new HTTPResponse();

            if(req.getMethod() == 'POST') {
                res.setBody(SELFADMIN_Testdata.MOCK_SUCCESS_GET_IHS_BY_TIME_WINDOW);
                res.setStatusCode(200);
            } else if(req.getMethod() == 'GET') {
                res.setBody(SELFADMIN_Testdata.MOCK_SUCCESS_GET_IHS_BY_TIME_WINDOW);
                res.setStatusCode(200);
            }
            return res;
        }
    }

    public class MockResponseUpdateNotificationStatus implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HTTPResponse res = new HTTPResponse();

            if(req.getMethod() == 'PUT') {
                res.setStatusCode(204);
            }
            return res;
        }
    }

    public class MockResponseUpdateEmailSend implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HTTPResponse res = new HTTPResponse();

            if(req.getMethod() == 'PUT') {
                res.setStatusCode(204);
            }
            return res;
        }
    }

}