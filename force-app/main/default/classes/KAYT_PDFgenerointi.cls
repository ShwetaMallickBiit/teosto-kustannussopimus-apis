/*
Description
            Class generates pdf:s and inserts them to attachment
*/
global with sharing class KAYT_PDFgenerointi {

    @InvocableMethod(label = 'Flow Generate PDF')
    global static List<flowParams> pdfLahetys_flow(List<flowParams> inputParams) {
        
        inputParams[0].attachmentId = generatePDF(inputParams[0].flowVFPage, inputParams[0].sopimusId, inputParams[0].flowPDFNimi, inputParams[0].flowKieli);    
        return inputParams;
    }

    global class flowParams{
        @InvocableVariable(required=true)
        global String sopimusId;

        @InvocableVariable(required=true)
        global String flowPDFNimi;

        @InvocableVariable(required=true)
        global String flowVFPage;

        @InvocableVariable(required=true)
        global String flowKieli;

        @InvocableVariable()
        global String runType;

        @InvocableVariable
        global String attachmentId;
    }
    
    public static String updatePrices(string sopimusId, string kieli, List<Tuoteosto__c> tuoteostotInput) {

        List<Tuoteosto__c> tuoteostot = new List<Tuoteosto__c>();
        try {
            if(tuoteostotInput.isEmpty()) {
                tuoteostot = [SELECT Id, Tuote__r.Id, Tuotetunnus__c, x_Hintahakupvm__c 
                              FROM Tuoteosto__c 
                              WHERE Sopimustieto__c = :sopimusId AND Tuote__r.Hintalaskuri_aktiivinen__c = TRUE];
            } else {
                tuoteostot = tuoteostotInput;
            }
            
            List<List<Tuoteostohintarivi__c>> insRivit = new List<List<Tuoteostohintarivi__c>>();
            for (Tuoteosto__c tuoteosto : tuoteostot) {
                List<Tuoteostohintarivi__c> rivit;
                    rivit = KAYT_Hintalaskuri.KAYT_HinnanAsettaminenTuoteostolle(kieli, tuoteosto.Id, tuoteosto.Tuote__r.Id, tuoteosto.Tuotetunnus__c, tuoteosto.x_Hintahakupvm__c);
                    insRivit.add(rivit);
            }
            
            // Insert of hintarivit to avoid dml error
            List<Tuoteostohintarivi__c> rivitAll = new List<Tuoteostohintarivi__c>();
            List<String> hintariviId = new List<String>();
            for(integer i = 0; i < insRivit.size(); i++) {
                for(Tuoteostohintarivi__c row : insRivit[i]) {
                    rivitAll.add(row);
                    hintariviId.add(row.x_External_Id__c);
                }
            }
            upsert rivitAll x_External_Id__c;
            return 'SUCCESS';

      } catch(Exception e) {
            System.debug('updatePrices: '+e.getMessage());
            Loki__c errorLog = new Loki__c(Objekti__c = 'Sopimustieto__c / Tuote__c', Toiminnallisuus__c = 'KAYT_PDFgenerointi, Update Prices', Tyyppi__c = 'Virhe', Virheviesti__c = e.getMessage()); insert errorLog; return 'ERROR';
       }
       
    }
     public static String generatePDF(string vfPage, string sopimusId, string pdfNimi, string kieli) {   
        try {
            PageReference pdf;
            IF(vfPage == 'KAYT_SopimusvahvistusPDF') {
                pdf = Page.KAYT_SopimusvahvistusPDF;
            }
            IF(vfPage == 'KAYT_MuutosvahvistusPDF') {
                pdf = Page.KAYT_MuutosvahvistusPDF;
            }
            IF(vfPage == 'KAYT_YhteenvetoPDF') {
                pdf = Page.KAYT_YhteenvetoPDF;
            }
            pdf.getParameters().put('id',sopimusId);
            Attachment pdfAttachment = new Attachment();
            pdfAttachment.Name = pdfNimi;
            pdfAttachment.ParentId = sopimusId;
            if (Test.isRunningTest()) 
                pdfAttachment.Body = Blob.toPdf('asdfg');
            else pdfAttachment.Body = pdf.getContent();

            insert pdfAttachment;   
            return pdfAttachment.Id;
      } 
      catch(Exception e) {
            System.debug(e.getMessage());
            Loki__c errorLog = new Loki__c(Objekti__c = 'Sopimustieto__c', Toiminnallisuus__c = 'KAYT_Batch_PDFgenerointi, Generoi PDF', Tyyppi__c = 'Virhe', Virheviesti__c = e.getMessage()); insert errorLog; return 'ERROR';
       }

    }   
    
}