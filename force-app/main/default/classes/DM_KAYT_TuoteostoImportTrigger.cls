global class DM_KAYT_TuoteostoImportTrigger implements Database.stateful, Schedulable, Database.AllowsCallouts, Database.Batchable<sObject>{
    
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        String soql = 'SELECT Id, onDatamigraatio__c, Batch_Status__c FROM DM_Tasokas_tuoteosto__c WHERE onDatamigraatio__c = FALSE';
        return Database.getQueryLocator(soql);
    }

    public void execute(Database.BatchableContext bc, List<DM_Tasokas_tuoteosto__c> records) {

        for (DM_Tasokas_tuoteosto__c record : records) {
            try {
                /* TRIGGER BATCH */
                DM_Tasokas_tuoteosto__c tuoteosto = new DM_Tasokas_tuoteosto__c();
                tuoteosto.Id = record.Id;
                tuoteosto.onDatamigraatio__c = TRUE;
                tuoteosto.Batch_Status__c = 'SUCCESS';
                update tuoteosto;
                /* UPDATE SUCCESS */
                DM_Tasokas_tuoteosto__c tuoteosto_success = new DM_Tasokas_tuoteosto__c();
                tuoteosto_success.Id = record.Id;
                tuoteosto_success.Batch_Status__c = 'SUCCESS';
            }
            catch(Exception e) {
                System.debug(e.getMessage());

                /* UPDATE ERROR */
                DM_Tasokas_tuoteosto__c tuoteosto_error = new DM_Tasokas_tuoteosto__c(); tuoteosto_error.Id = record.Id; tuoteosto_error.Batch_Status__c = 'ERROR'; update tuoteosto_error;
            }

        }
    }
    public void finish(Database.BatchableContext bc) {
        
    }
    public void execute(SchedulableContext sc){
        Database.executeBatch(this,1);
    }
}