@isTest
public class KAYT_TapahtumakansiohakuCallTest {

    @testSetup static void setup() {    
        ExtranetAPI__c params = new ExtranetAPI__c(name='params',Value__c='eyJraWQiOiJmODBhMjY5YzRlM2Y0NDFmOTFjZTFiYmIxMWExZmM3NiIsInR5cCI6IkpXVCIsImFsZyI6IkhTMjU2In0.eyJ1c2VybnVtYmVyIjo2NjcsInVzZXJ0eXBlIjoiWVAiLCJleHAiOjQ2NTg0NDQ0MTZ9.boPtogjUGHychCcDvt4mQhy_ZqxAJec8PS_KppzfhsI');
        insert params;
    }

    public static List<KAYT_TapahtumakansiohakuCall.FolderRESTCall> initializeParamsMonthly() {

        List<KAYT_TapahtumakansiohakuCall.FolderRESTCall> params = new List<KAYT_TapahtumakansiohakuCall.FolderRESTCall>();
        KAYT_TapahtumakansiohakuCall.FolderRESTCall param = new KAYT_TapahtumakansiohakuCall.FolderRESTCall();
        param.requestType = 'monthly';
        param.orderId = '815AF23C7AB3D964C22580DD00255CA9';
        params.add(param);
        return params;
    }    

    public static List<KAYT_TapahtumakansiohakuCall.FolderRESTCall> initializeParamsYearly() {

        List<KAYT_TapahtumakansiohakuCall.FolderRESTCall> params = new List<KAYT_TapahtumakansiohakuCall.FolderRESTCall>();
        KAYT_TapahtumakansiohakuCall.FolderRESTCall param = new KAYT_TapahtumakansiohakuCall.FolderRESTCall();
        param.requestType = 'yearly';
        param.orderId = '815AF23C7AB3D964C22580DD00255CA9';
        params.add(param);
        return params;
    }    
    
    public static testMethod void TapahtumakansiohakuCallTest() {
        
        Test.startTest();  
        Test.setMock(HttpCalloutMock.class, new KAYT_RestCallMock('Monthly_Pass'));
        List<KAYT_TapahtumakansiohakuCall.FolderRESTCall> evtCall = KAYT_TapahtumakansiohakuCall.GetFolderMonthly(initializeParamsMonthly());
        Test.stopTest();
        System.assertEquals(evtCall[0].Status, 'SUCCESS');
        System.assertNOTEquals(evtCall[0].minEndDate, null);
    }
    
    public static testMethod void TapahtumakansiohakuCallEmptyTest() {
        
        Test.startTest();  
        Test.setMock(HttpCalloutMock.class, new KAYT_RestCallMock('Monthly_Empty'));
        List<KAYT_TapahtumakansiohakuCall.FolderRESTCall> evtCall = KAYT_TapahtumakansiohakuCall.GetFolderMonthly(initializeParamsMonthly());
        Test.stopTest();
        System.assertEquals(evtCall[0].Status, 'SUCCESS');
        System.assertNOTEquals(evtCall[0].minEndDate, null);
    }
    
        public static testMethod void TapahtumakansiohakuCallFailTest() {
        List<String> orderIds = new String[]{'815AF23C7AB3D964C22580DD00255CA9'};
        Test.startTest();  
        Test.setMock(HttpCalloutMock.class, new KAYT_RestCallMock('Monthly_Fail'));
        List<KAYT_TapahtumakansiohakuCall.FolderRESTCall> evtCall = KAYT_TapahtumakansiohakuCall.GetFolderMonthly(initializeParamsMonthly());
        Test.stopTest();
        System.assertEquals(evtCall[0].Status, 'ERROR');
        System.assertEquals(evtCall[0].minEndDate, null);
    }

    public static testMethod void TapahtumakansiohakuCallYearlyTest() {
        
        Test.startTest();  
        Test.setMock(HttpCalloutMock.class, new KAYT_RestCallMock('Monthly_Empty'));
        List<KAYT_TapahtumakansiohakuCall.FolderRESTCall> evtCall = KAYT_TapahtumakansiohakuCall.GetFolderYearly(initializeParamsYearly());
        Test.stopTest();
        System.assertEquals(evtCall[0].Status, 'SUCCESS');
        System.assertNOTEquals(evtCall[0].minEndDate, null);
    }
    
    public class KAYT_RestCallMock implements HttpCalloutMock{
        
        public String type;
        public KAYT_RestCallMock(final String type){
                 this.type = type;
        }
        
        public HttpResponse respond(HTTPRequest req)
        {
            HttpResponse res = new HttpResponse();
            res.setStatusCode(200);        
            res.setHeader('Content-Type', 'application/json');
            
            res.setStatusCode(200); 
            string body1 = '[{"lomakeno":"", "vuosi":"2017", "kk":"11", "vahvistustila":"VAH", "laskutustila":"PAL"}]';
            string body2 = '[]';
            string body3 = '[{}]';
            string body4 = '[{"lomakeno":27127,"vuosi":0,"laskutustila":"","laskurivit":[{"id":467563,"vuosi":2018,"laskutustila":"LAS","netto":313.77},{"id":467564,"vuosi":2018,"laskutustila":"LAS","netto":-9.41},{"id":467565,"vuosi":2018,"laskutustila":"LAS","netto":324.82},{"id":467566,"vuosi":2018,"laskutustila":"LAS","netto":-9.74},{"id":467567,"vuosi":2018,"laskutustila":"LAS","netto":287.6},{"id":467568,"vuosi":2018,"laskutustila":"LAS","netto":-8.63},{"id":467569,"vuosi":2018,"laskutustila":"LAS","netto":286.73},{"id":467570,"vuosi":2018,"laskutustila":"LAS","netto":-8.6}]}]';
            string body = type=='Monthly_Pass' ? body1 : (type == 'Monthly_Empty' ? body2 : body3);
            res.setBody(body);
              
            return res;
        }    
    }
    
}