/*
Description
            Class makes request (REST) to receive self admin notification (itsehallinnointi-ilmoitus)
            Class is called from Lightning Component (e.g. when user navigates to record)
*/

global with sharing class SopimustietoTriggerController {

    Sopimustieto__c rec {get; set;}
    String recId {get; set;}
    
    public SopimustietoTriggerController(ApexPages.StandardController stdCtrl) {
        this.rec = (Sopimustieto__c)stdCtrl.getRecord(); 
        recId = rec.Id;
    }    
    public PageReference getData() {        
        PageReference pg = new PageReference('/'+this.rec.Id);
        pg.setRedirect(true); 
        return pg;
    }
    global static List<Sopimustieto__c> getSopimustietoData(Id idVal) {
        return [SELECT Id, Asiakas_tekija__r.Asiakasnumero_Tepa__c, RecordType.DeveloperName FROM Sopimustieto__c 
                WHERE Id = :idVal
                LIMIT 1];
    }
    @AuraEnabled
    global static Boolean updateRecord(String idVal) {
        try{
            String customerType;
            List<Sopimustieto__c> sops = getSopimustietoData(idVal);
            if(sops[0].RecordType.DeveloperName == 'Tekijan_asiakassopimus') customerType = 'TE';
            if(sops[0].RecordType.DeveloperName == 'Kustantajan_asiakassopimus') customerType = 'KU';
            
            if(customerType == 'TE' || customerType == 'KU')
                SELFADMIN_Notification_Controller.getAdministrationNotificationByCustomerNumber(integer.valueOf(sops[0].Asiakas_tekija__r.Asiakasnumero_Tepa__c), customerType);
            
            return true;
        } catch(Exception e) {
            Loki__c errorLog = new Loki__c();
            errorLog.Objekti__c = 'Sopimustieto__c';
            errorLog.Toiminnallisuus__c = 'SopimustietoTriggerController';
            errorLog.Tyyppi__c = 'Virhe';
            errorLog.Virheviesti__c = e.getMessage();
            insert errorLog;
            return false;
        }
    }

}