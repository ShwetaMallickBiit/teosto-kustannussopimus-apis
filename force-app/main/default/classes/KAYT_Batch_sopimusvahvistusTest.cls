@isTest(seeAllData=true)
private class KAYT_Batch_sopimusvahvistusTest {

    static testMethod void getContractData() {

       // create account
       Account acc = new Account(
           Name = 'TestAccount', Kieli__c = 'Suomi', Asiakasryhma__c = 'Käyttäjä', Luokittelu__c ='Avain',
           Ohita_Y_tunnus_Hetu_validointi__c = true, ShippingStreet = 'Testikatu 10', ShippingCity = 'Helsinki',
           ShippingPostalCode = '00100');
       insert acc;

       // get record type id    
       Id RecordTypeIdST = Schema.SObjectType.Sopimustieto__c.getRecordTypeInfosByName().get('Musiikinkäyttösopimus, sopimusvahvistus').getRecordTypeId(); 

       // create contact
       Contact con = new Contact(
           FirstName = 'Etunimi', LastName = 'Sukunimi');
       insert con;

       // create Acc_con__c
       AccountContactRelation_c__c acc_con = new AccountContactRelation_c__c(
           AccountId_c__c = acc.Id, ContactId_c__c = con.Id,Name = 'Testinimi', Sidoksen_email_c__c = 'testi.testinen@biit.fi');
       insert acc_con;

       // create Sopimustieto record
       Sopimustieto__c st_mv = new Sopimustieto__c(
           recordtypeid=RecordTypeIdST, Asiakas_tekija__c = acc.Id, OnEri_kuin_postitusosoite__c = true,
           Laskutusosoite_Katu__c = 'Testikatu 1', Laskutusosoite_Kaupunki__c = 'Helsinki', Laskutusosoite_Postinumero__c = '00100',
           x_onTuoteosto__c = true, Vastuuhenkilo__c = acc_con.Id, Allekirjoittaja__c = acc_con.Id, Tila__c = 'Valmis lähetettäväksi',
           Alkupvm__c = Date.newInstance(2018, 01, 01), Sopimuksen_allekirjoituspvm__c = Date.newInstance(2018, 01, 01), 
           Tuoteoston_muokkauspvm__c = DateTime.newInstance(2018, 01, 04, 0, 0, 0),
           x_Muutosvahvistus_pdf_Id_Kayt__c = '12345678'
           );
       insert st_mv;

       //  Run test
       Test.startTest();
       /* this nulls SOQL limits after seed data insert */
       System.assertEquals('1', '1'); 

       // create tuote
       Tuote__c tuoteData = new Tuote__c(
           Name = 'Testituote', Tuotetunnus__c = 'PORKKII', Alkupvm__c = Date.newInstance(2018, 01, 01),
           Tila__c = 'Myynnissä');
       insert tuoteData;

       // create Tuoteosto
       Tuoteosto__c conData = new Tuoteosto__c(Sopimustieto__c = st_mv.Id, Asiakas__c = acc.Id, Tuote__c = tuoteData.Id);
       insert conData;

       // create Tuoteostorivi
       Tuoteostorivi__c tRivi = new Tuoteostorivi__c(Tuoteosto__c = conData.Id, Arvo__c = 1, Parametri__c = 'LUPAKPL');
       insert tRivi;

       Test.setMock(HttpCalloutMock.class, new KAYT_HintalaskuriMock());
       KAYT_Batch_sopimusvahvistusPDFgenerointi sopVahvGen = new KAYT_Batch_sopimusvahvistusPDFgenerointi();
       ID batchprocessid1 = Database.executeBatch(sopVahvGen);

       Test.stopTest();
    }
}