public with sharing class KAYT_SopimusFetcher {
   @AuraEnabled
    public static List<Sopimuksen_yhteyshenkilo__c> getContracts (String contactId) {
        List<Sopimuksen_yhteyshenkilo__c> contractList = [SELECT Id, Name, x_Sopimusnro__c,
         Sopimustieto__r.Sopimustyyppi__c,
         Sopimustieto__r.Alkupvm__c, Sopimustieto__r.Loppupvm__c, Yhteyshenkilo_Id__c
         FROM Sopimuksen_yhteyshenkilo__c WHERE Yhteyshenkilo_Id__c = :contactId];
        return contractList;
    }
}