// Tatjana Podvisocka; 17/10/2017
@isTest
public class KAYT_TapahtumahakuCallTest {
	
	 public static testMethod void TapahtumahakuCallTest() {
	 	
	 	List<String> orderIds = new String[]{'815AF23C7AB3D964C22580DD00255CA9'};	
	 	 	
	 	ExtranetAPI__c params = new ExtranetAPI__c(name='params', endpointURL__c ='https://extranet.teosto.fi/soratest/',Value__c='eyJraWQiOiJmODBhMjY5YzRlM2Y0NDFmOTFjZTFiYmIxMWExZmM3NiIsInR5cCI6IkpXVCIsImFsZyI6IkhTMjU2In0.eyJ1c2VybnVtYmVyIjo2NjcsInVzZXJ0eXBlIjoiWVAiLCJleHAiOjQ2NTg0NDQ0MTZ9.boPtogjUGHychCcDvt4mQhy_ZqxAJec8PS_KppzfhsI');
	 	insert params;
	 	
	 	Test.startTest();  
	 	  	 	
		Test.setMock(HttpCalloutMock.class, new KAYT_TapahtumahakuCallMock('Pass'));
		
		Test.stopTest();
		
		List<KAYT_TapahtumahakuCall.EventRESTCall> evtCall = KAYT_TapahtumahakuCall.GetEventEndDate(orderIds);
		
		System.assertEquals(evtCall[0].Status, 'SUCCESS');
		System.assertNOTEquals(evtCall[0].EventEndDate, NULL);
		
	 }
	 
	 public static testMethod void TapahtumahakuCallEmptyTest() {
	 	
	 	List<String> orderIds = new String[]{'815AF23C7AB3D964C22580DD00255CA9'};	
	 	 	
	 	ExtranetAPI__c params = new ExtranetAPI__c(name='params', endpointURL__c ='https://extranet.teosto.fi/soratest/',Value__c='eyJraWQiOiJmODBhMjY5YzRlM2Y0NDFmOTFjZTFiYmIxMWExZmM3NiIsInR5cCI6IkpXVCIsImFsZyI6IkhTMjU2In0.eyJ1c2VybnVtYmVyIjo2NjcsInVzZXJ0eXBlIjoiWVAiLCJleHAiOjQ2NTg0NDQ0MTZ9.boPtogjUGHychCcDvt4mQhy_ZqxAJec8PS_KppzfhsI');
	 	insert params;
	 	
	 	Test.startTest();  
	 	  	 	
		Test.setMock(HttpCalloutMock.class, new KAYT_TapahtumahakuCallMock('Empty'));
		
		Test.stopTest();
		
		List<KAYT_TapahtumahakuCall.EventRESTCall> evtCall = KAYT_TapahtumahakuCall.GetEventEndDate(orderIds);
		
		System.assertEquals(evtCall[0].Status, 'SUCCESS');
		System.assertEquals(evtCall[0].EventEndDate, Date.newInstance(1900, 1, 1));
		
	 }
	 
	 	public static testMethod void TapahtumahakuCallFailedTest() {
	 		
	 		List<String> orderIds = new String[]{'815AF23C7AB3D964C22580DD00255CA9'};	
	 	
	 		Test.startTest();  
	 	  	 	
			Test.setMock(HttpCalloutMock.class, new KAYT_TapahtumahakuCallMock('Pass'));
		
			Test.stopTest();
		
			List<KAYT_TapahtumahakuCall.EventRESTCall> evtCall = KAYT_TapahtumahakuCall.GetEventEndDate(orderIds);
			
			System.assertNOTEquals(evtCall[0].errMsg, null);
			
			System.assertEquals(evtCall[0].Status, 'ERROR');
	 } 
}