/*
Description
            Class calls to fetch invoice and invoice related information from Laskutin
*/
global class KAYT_TapahtumahakuCall {

    @InvocableMethod
    public static List<EventRESTCall> GetEventEndDate(List<String> orderId) 
    {
        EventRESTCall output = new EventRESTCall();
        List<EventRESTCall> outputs = new List<EventRESTCall>();
        String Status = 'ERROR';
        try{
            ExtranetAPI__c params = ExtranetAPI__c.getValues('params');
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(params.endpointURL__c + 'events?licenseid='+orderId[0]+'&limit=1&orderby=loppupvm&order=desc');
            string header = 'Bearer ' +params.Value__c;
            request.setHeader('Authorization', header);
            request.setMethod('GET');
            HttpResponse response = http.send(request);
        
            if (response.getStatusCode() == 200) 
            {
                List<EventJSON> results = (List<EventJSON>) JSON.deserialize(response.getBody(),List<EventJSON>.class);
                if(results.size() == 0) {
                    output.EventEndDate = Date.newInstance(1900, 1, 1);
                    Status = 'SUCCESS';
                }
                else {                
                    for (EventJSON res: results)
                    {
                        output.EventEndDate = res.loppupvm;
                        Status = 'SUCCESS';
                    }
               }   
            }
            if (response.getStatusCode() == 404) 
            {
                    output.EventEndDate = Date.newInstance(1900, 1, 1);
                    Status = 'SUCCESS';
            }
        }catch (Exception e) 
        {
            output.errMsg = e.getMessage();
        }
        finally
        {       
            output.Status = Status;
            outputs.add(output);
        }

        system.debug(outputs);
        
        
        return outputs; 
    }
    
    public class EventJSON
    {
        public String tilnro;
        public Date alkupvm;
        public Date loppupvm;
        public String tilnimi;
        public String paatilnro;
        public String asiakasnro;
        public String kayttajanro;
        public String lomakeno;
        public String tuote; 
        public String tuoteostoid;
        public String tiltyyppi;
        public String luonne;
        public String lahde;
    }

    public class EventRESTCall 
    {
        @InvocableVariable
        public Date EventEndDate;
    
        @InvocableVariable(required=true)
        public String Status;
        
        @InvocableVariable
        public String errMsg;
    }
}