global class DM_KAYT_EsityspaikkasidosImportTrigger implements Database.stateful, Schedulable, Database.AllowsCallouts, Database.Batchable<sObject>{
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        String soql = 'SELECT Id, onDatamigraatio__c, Batch_Status__c FROM DM_Tasokas_esityspaikkasidos__c WHERE onDatamigraatio__c = FALSE';
        return Database.getQueryLocator(soql);
    }

    public void execute(Database.BatchableContext bc, List<DM_Tasokas_esityspaikkasidos__c> records) {

        for (DM_Tasokas_esityspaikkasidos__c record : records) {
            try {
                /* TRIGGER BATCH */
                DM_Tasokas_esityspaikkasidos__c esityspaikkasidos = new DM_Tasokas_esityspaikkasidos__c();
                esityspaikkasidos.Id = record.Id;
                esityspaikkasidos.onDatamigraatio__c = TRUE;
                esityspaikkasidos.Batch_Status__c = 'SUCCESS';
                update esityspaikkasidos;
                /* UPDATE SUCCESS */
                DM_Tasokas_esityspaikkasidos__c esityspaikkasidos_success = new DM_Tasokas_esityspaikkasidos__c();
                esityspaikkasidos_success.Id = record.Id;
                esityspaikkasidos_success.Batch_Status__c = 'SUCCESS';
            }
            catch(Exception e) {
                System.debug(e.getMessage());

                /* UPDATE ERROR */
                DM_Tasokas_esityspaikkasidos__c esityspaikkasidos_error = new DM_Tasokas_esityspaikkasidos__c(); esityspaikkasidos_error.Id = record.Id; esityspaikkasidos_error.Batch_Status__c = 'ERROR'; update esityspaikkasidos_error;
            }

        }
    }
    public void finish(Database.BatchableContext bc) {
        
    }
    public void execute(SchedulableContext sc){
        Database.executeBatch(this,1);
    }
}