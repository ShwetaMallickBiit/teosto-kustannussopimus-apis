global with sharing class KAYT_emailLahetys{

    @InvocableMethod(label = 'Flow Email')
    global static List<emailParams> emailLahetys_flow(List<emailParams> inputParams) {
        lahetaEmail(inputParams[0].sopimusId, inputParams[0].flowTemplate, inputParams[0].flowKieli, inputParams[0].flowAttachmentId, inputParams[0].toContactId, inputParams[0].toEmail, FALSE);
        return null;
    }

  global class emailParams{
    @InvocableVariable(required=true)
    global String sopimusId;

    @InvocableVariable(required=true)
    global String flowTemplate;

    @InvocableVariable(required=true)
    global String toContactId;

    @InvocableVariable(required=true)
    global String toEmail;

    @InvocableVariable(required=true)
    global String flowAttachmentId;

    @InvocableVariable(required=true)
    global String flowKieli;
    
    }
    
    public static string lahetaEmail(string sopimusId, string templateName, string kieli, string attachmentId, string toEmail, string contactId, boolean updateSendDate) {
        List<string> worked = new List<string>();

        try {

            /*query email template id*/
            templateName = templateName + '_' + kieli;
            List<emailTemplate> emailTemplateId = [select id, name from EmailTemplate where developername = :templateName];
            String templateId = emailTemplateId[0].Id;

            /*query sender email address*/
            List<Arvolista__c> orgWideEmailAddresses = [SELECT Arvo_2_Teksti__c FROM Arvolista__c WHERE Ryhma__c = 'AUTOMATIC_EMAIL_SENDER_IPR_OWNER' LIMIT 1];
            String orgWideEmailAddress = orgWideEmailAddresses[0].Arvo_2_Teksti__c;
            
            /*query attachment record*/
            Attachment att = (Attachment) Database.query('SELECT Id, Name, Body, CreatedDate FROM Attachment WHERE Id = :attachmentId');

            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setTemplateId(templateId);
            email.setTargetObjectId(contactId);
            email.setWhatId(sopimusId);
            IF(att != null) {
                Messaging.EmailFileAttachment emailAtt =new Messaging.EmailFileAttachment();
                emailAtt.setFileName(att.name);
                emailAtt.setBody(att.body);
                email.setFileAttachments(new Messaging.EmailFileAttachment[] {emailAtt});
            }
            email.setOrgWideEmailAddressId(orgWideEmailAddress);
            email.setToAddresses(New List<String>{toEmail});
            email.setTreatTargetObjectAsRecipient(False);

            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
            
            worked.add(sopimusId);
            return 'SUCCESS';

      } catch(Exception e) {
            System.debug(e.getMessage());
            Loki__c errorLog = new Loki__c(Objekti__c = 'Sopimustieto__c', Toiminnallisuus__c = 'KAYT_Batch_PDFlahetys', Tyyppi__c = 'Virhe', Virheviesti__c = e.getMessage());
            insert errorLog;
            return 'ERROR';
       }

    }
}