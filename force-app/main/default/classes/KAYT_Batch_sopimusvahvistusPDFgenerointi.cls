/*
Description
            Class generates sopimusvahvistus pdf documents to attachments of Sopimustieto__c record
*/
global class KAYT_Batch_sopimusvahvistusPDFgenerointi implements Database.stateful, Schedulable, Database.AllowsCallouts, Database.Batchable<sObject>{
    
    private List <Sopimustieto__c> stLst;
    public Database.QueryLocator start(Database.BatchableContext bc) {
    
        String soql;
        if(Test.isRunningTest()) {
            soql = 'SELECT Id, x_Sopimusvahvistus_pdf_Nimi_Kayt__c, Asiakas_tekija__r.Kieli__c FROM Sopimustieto__c WHERE x_Sopimusvahvistus_generointiajossa_Kayt__c = TRUE AND x_Muutosvahvistus_pdf_Id_Kayt__c = \'12345678\' LIMIT 1';
        }
        else {
            soql = 'SELECT Id, x_Sopimusvahvistus_pdf_Nimi_Kayt__c, Asiakas_tekija__r.Kieli__c FROM Sopimustieto__c WHERE x_Sopimusvahvistus_generointiajossa_Kayt__c = TRUE LIMIT 100';
        }
        return Database.getQueryLocator(soql);
    }

    public void execute(Database.BatchableContext bc, List<Sopimustieto__c> records) {

        List<Tuoteosto__c> tuoteostot = new List<Tuoteosto__c>();
        for (Sopimustieto__c record : records) {
                /*generate pdf as attachment to sopimus*/
                string updPricesStatus = KAYT_PDFgenerointi.updatePrices(record.Id, record.Asiakas_tekija__r.Kieli__c, tuoteostot);
        }
    }

    public void finish(Database.BatchableContext bc) {
        
        // create PDF attachment; Called in finish to avoid DML conflict with asynchronous call
        string attachmentId;
        List<Sopimustieto__c> recs = New List<Sopimustieto__c>();
        if(Test.isRunningTest()) {
            recs = [SELECT Id, x_Sopimusvahvistus_pdf_Nimi_Kayt__c, Asiakas_tekija__r.Kieli__c FROM Sopimustieto__c WHERE x_Sopimusvahvistus_generointiajossa_Kayt__c = true AND x_Muutosvahvistus_pdf_Id_Kayt__c = '12345678' LIMIT 1];
        } else {
            recs = [SELECT Id, x_Sopimusvahvistus_pdf_Nimi_Kayt__c, Asiakas_tekija__r.Kieli__c FROM Sopimustieto__c WHERE x_Sopimusvahvistus_generointiajossa_Kayt__c = true LIMIT 100];
        }
        
        for (Sopimustieto__c record : recs) {
            try {
                /*generate pdf as attachment to sopimus*/
                attachmentId = KAYT_PDFgenerointi.generatePDF('KAYT_SopimusvahvistusPDF', record.Id, record.x_Sopimusvahvistus_pdf_Nimi_Kayt__c, record.Asiakas_tekija__r.Kieli__c);
                
                /*update sopimustieto*/
                Sopimustieto__c sopimustieto = new Sopimustieto__c();
                sopimustieto.Id = record.Id;
                if(attachmentId <> 'ERROR') {
                    sopimustieto.Sopimusvahvistus_generointipaiva__c = Date.Today();
                    sopimustieto.Sopimusvahvistus_generointipaiva_dt__c = Datetime.Now();
                }
                sopimustieto.x_Sopimusvahvistus_pdf_Id_Kayt__c = attachmentId;
                sopimustieto.x_Ei_automaatioita__c = Datetime.Now();
                update sopimustieto;
            } 
            catch(Exception e) {
                System.debug(e.getMessage());
                Loki__c errorLog = new Loki__c(Objekti__c = 'Sopimustieto__c', Toiminnallisuus__c = 'KAYT_Batch_sopimusvahvistusPDFgenerointi, KAYT_Batch_PDFgenerointi', Tyyppi__c = 'Virhe', Virheviesti__c = record.Id + ' ' + e.getMessage());
                insert errorLog;
            }
        }

        // käynnistetään tarvittaessa sopimusvahvistuslähetys
        ExtranetAPI__c params = ExtranetAPI__c.getValues('params');
        if(params.Generointi_ja_lahetys_sopimusvahvistus__c) {
            database.executeBatch(new KAYT_Batch_sopimusvahvistusLahetys());
        }
                
    }
    public void execute(SchedulableContext sc){
        Database.executeBatch(this,1);
    }
}