global with sharing class GEN_TapausAutoresponse{
 
    @InvocableMethod(label = 'Flow Tapaus Autoreply')
    global static List<emailParams> emailLahetys_flow(List<emailParams> inputParams) {
        lahetaEmail(inputParams[0].tapausId, inputParams[0].emailTemplate, inputParams[0].kieli, inputParams[0].toContactId, inputParams[0].toEmail, inputParams[0].orgWideEmail);
        return null;
    }

  global class emailParams{
    @InvocableVariable(required=true)
    global String tapausId;

    @InvocableVariable(required=true)
    global String emailTemplate;

    @InvocableVariable(required=true)
    global String kieli;

    @InvocableVariable(required=false)
    global String toContactId;

    @InvocableVariable(required=true)
    global String toEmail;

    @InvocableVariable(required=true)
    global String orgWideEmail;
    
    }
    
    public static string lahetaEmail(string tapausId, string emailTemplate, string kieli, string toContactId, string toEmail, string orgWideEmail) {

        try {
            /*query email template id*/
            emailTemplate = emailTemplate + '_' + kieli;
            List<emailTemplate> emailTemplateId = [select id, name from EmailTemplate where developername = :emailTemplate];
            String templateId = emailTemplateId[0].Id;

            /* initialize params and send email */
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setTemplateId(templateId);
            if(!string.isBlank(toContactId)) email.setTargetObjectId(toContactId);
            email.setWhatId(tapausId);
            email.setOrgWideEmailAddressId(orgWideEmail);
            email.setToAddresses(New List<String>{toEmail});
            email.setTreatTargetObjectAsRecipient(False);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
            
            return 'SUCCESS';

      } catch(Exception e) {
            System.debug(e.getMessage());
            Loki__c errorLog = new Loki__c(Objekti__c = 'Sopimustieto__c', Toiminnallisuus__c = 'KAYT_Batch_PDFlahetys', Tyyppi__c = 'Virhe', Virheviesti__c = e.getMessage());
            insert errorLog; return 'ERROR';
       }

    }
}