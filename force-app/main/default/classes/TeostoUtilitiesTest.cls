@isTest(seeAllData=false)
private class TeostoUtilitiesTest {
    @isTest
    public static void getAccountPrimaryEmailTest() {
        Account a = GEN_TestFactory.createAccountTekija();
        Contact con = GEN_TestFactory.createContact(a.Id, 'Test', 'Testinen');
        IPI_numero__c ipi = GEN_TestFactory.createIpiNumero(a.Id, TRUE);
        Id oweaId = '0D29E00000000AV';
        String yhteyshenkiloId = [SELECT Id FROM AccountContactRelation_c__c WHERE AccountId_c__c = :a.Id AND ContactId_c__c = :con.Id LIMIT 1][0].Id;
        Sopimustieto__c st = GEN_TestFactory.createSopimusTekija2(a.Id, yhteyshenkiloId, FALSE);
        st.Tila__c = 'Valmis lähetettäväksi';
        insert st;
        List<Account> accs = New List<Account>();
        accs.add(a);
        Test.startTest();
            TeostoUtilities.getAccountPrimaryEmail(accs, 'OIKOM');
        Test.stopTest();
    }
    @isTest
    public static void getStringSetOfSObjectListTest() {
        Account a = GEN_TestFactory.createAccountTekija();
        List<Account> accs = New List<Account>();
        accs.add(a);
        TeostoUtilities.getStringSetOfSObjectList(accs, 'Name');
    }
    @isTest
    public static void getMapOfSObjectListTest() {
        Account a = GEN_TestFactory.createAccountTekija();
        List<Account> accs = New List<Account>();
        accs.add(a);
        TeostoUtilities.getMapOfSObjectList(accs, 'Name');  
    }
}