global class DM_KAYT_SidosImportTrigger implements Database.stateful, Schedulable, Database.AllowsCallouts, Database.Batchable<sObject>{
    
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        String soql = 'SELECT Id, onDatamigraatio__c, Batch_Status__c FROM DM_Tasokas_sidos__c WHERE onDatamigraatio__c = FALSE';
        return Database.getQueryLocator(soql);
    }

    public void execute(Database.BatchableContext bc, List<DM_Tasokas_sidos__c> records) {

        for (DM_Tasokas_sidos__c record : records) {
            try {
                /* TRIGGER BATCH */
                DM_Tasokas_sidos__c sidos = new DM_Tasokas_sidos__c();
                sidos.Id = record.Id;
                sidos.onDatamigraatio__c = TRUE;
                sidos.Batch_Status__c = 'SUCCESS';
                update sidos;
                /* UPDATE SUCCESS */
                DM_Tasokas_sidos__c sidos_success = new DM_Tasokas_sidos__c();
                sidos_success.Id = record.Id;
                sidos_success.Batch_Status__c = 'SUCCESS';
            }
            catch(Exception e) {
                System.debug(e.getMessage());

                /* UPDATE ERROR */
                DM_Tasokas_sidos__c sidos_error = new DM_Tasokas_sidos__c(); sidos_error.Id = record.Id; sidos_error.Batch_Status__c = 'ERROR'; update sidos_error;
            }

        }
    }
    public void finish(Database.BatchableContext bc) {
        
    }
    public void execute(SchedulableContext sc){
        Database.executeBatch(this,1);
    }
}