@isTest
public class GEN_TestFactory {
    public static Account createAccount(){
       Account acc = new Account(
           Name = 'TestAccount', Kieli__c = 'Suomi', Asiakasryhma__c = 'Käyttäjä', Luokittelu__c ='Avain',
           Ohita_Y_tunnus_Hetu_validointi__c = true, ShippingStreet = 'Testikatu 10', ShippingCity = 'Helsinki',
           ShippingPostalCode = '00100');
       insert acc;
       return acc;
    }

    public static Account createAccountTekija(){
       Account acc = new Account(
           Name = 'TestAccount', Kieli__c = 'Suomi', Asiakasryhma__c = 'Tekijä', Luokittelu__c ='Avain',
           Ohita_Y_tunnus_Hetu_validointi__c = true, Ominaisuus__c = 'Sanoittaja',
           ShippingStreet = 'Testikatu 10', ShippingCity = 'Helsinki', ShippingPostalCode = '00100',
           BillingStreet = 'Testikatu 10', BillingCity = 'Helsinki', BillingPostalCode = '00100'
           );
       insert acc;
       return acc;
    }
    
    public static Sopimustieto__c createSopimusTekija(String asiakasId) {
        Sopimustieto__c sop = new Sopimustieto__c(
            Asiakas_tekija__c = asiakasId, Tila__c = 'Sopimus lähetetty', Alkupvm__c = date.Today() - 100
            );
        insert sop;
        return sop;
    }

    public static Sopimustieto__c createSopimusTekija2(String asiakasId, String yhteyshenkilosuhdeId, Boolean autoInsert) {
        Sopimustieto__c sop = new Sopimustieto__c(
            Asiakas_tekija__c = asiakasId, Tila__c = 'Sopimus lähetetty', Alkupvm__c = date.Today() - 100, Vastuuhenkilo__c = yhteyshenkilosuhdeId, IPI_Numero__c = '39861256'
            );
        if(autoInsert) insert sop;
        return sop;
    }

    public static IPI_Numero__c createIpiNumero(String asiakasId, Boolean autoInsert) {
        IPI_numero__c ipi = new IPI_numero__c();
        ipi.Name = 'Firstname LastName';
        ipi.Etunimi__c = 'Test Firstname';
        ipi.Sukunimi__c = 'Test Lastname';
        ipi.IPI_numero__c = 123456;
        ipi.Ominaisuus__c = 'Säveltäjä';
        ipi.Tyyppi__c = 'Patronyymi';
        ipi.Asiakas__c = asiakasId;
        if(autoInsert) insert ipi;
        return ipi;
    }

    public static Attachment createAttachment(String parentId, Boolean autoInsert) {
        Attachment att = new Attachment();
        att.Name = 'UnitTestFileAttachment';
        att.Body = Blob.valueOf('Unit Test file binary attachment body');
        att.ParentId = parentId;
        if(autoInsert) insert att;
        return att;
    }

    public static Itsehallinnointi__c createItsehallinnointi(String asiakasId, String sopimusId) {
        Itsehallinnointi__c ih = new Itsehallinnointi__c(Asiakas__c = asiakasId, Sopimustieto__c = sopimusId, Name = 'Ih name', Status__c = 'Waiting for processing', NotificationId__c = 76588, Tunniste__c = '12345');
        insert ih;
        return ih;
    }
    
    public static AdditionalEmail__c createAdditionalEmail(String itsehallinointiId) {
        AdditionalEmail__c ae = new AdditionalEmail__c(
            Email__c = 'testi@biit.fi', Name = 'Test', Itsehallinnointi__c = itsehallinointiId);
        insert ae;
        return ae;
    }

    public static Contact createContact(Id accountId, String fName, String lName){
       Contact con = new Contact(AccountId = accountId, FirstName = fName, LastName = lName, Email = fName + lName + '@biit.fi');
       insert con;
       return con;
    }
    
    public static void generateAccountWithContacts() {
        Account acc = createAccount();
        Contact con = createContact(acc.Id, 'Etunimi', 'Sukunimi');
    }
    
    public static Tuote__c createTuote(String name, String tuotetunnus, Boolean doInsert) {
        Tuote__c t = New Tuote__c();
        t.name = name;
        t.Alkupvm__c = date.Today() - 500;
        t.Tila__c = 'Myynnissä';
        t.Tuotetunnus__c = tuotetunnus;
        if(doInsert) insert t;
        return t;
    }
    
    public static Tuotteen_hintarivi__c createTuotteenHintarivi(String tuotteenHinnastoId, Boolean doInsert) {
        Tuotteen_hintarivi__c th = New Tuotteen_hintarivi__c();
        th.Arvo__c = 10;
        th.Tuotteen_hinnasto__c = tuotteenHinnastoId;
        if(doInsert) insert th;
        return th;
    }
    
    public static Tuotteen_hinnasto__c createTuotteenHinnasto(String tuoteId, Boolean doInsert) {
        Tuotteen_hinnasto__c th = New Tuotteen_hinnasto__c();
        th.Tuote__c = tuoteId;
        th.Alkupvm__c = date.Today() - 500;
        if(doInsert) insert th;
        return th;
    }
}