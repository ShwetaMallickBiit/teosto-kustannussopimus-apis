/*
Description
            Class fetches prices from Hintalaskuri (external system)
*/
global with sharing class KAYT_Hintalaskuri {

    Tuoteosto__c rec {get; set;}
    String recId {get; set;}
    KAYT_HintahakuJSON jsonHH = new KAYT_HintahakuJSON();
    
    public KAYT_Hintalaskuri(ApexPages.StandardController stdCtrl) 
    {
        this.rec = (Tuoteosto__c)stdCtrl.getRecord(); 
        recId = rec.Id;
        jsonHH.id = null;
        jsonHH.code = null;
        jsonHH.name = null;
        jsonHH.startDate = null;
        jsonHH.endDate = null;
        jsonHH.message = null;
    }
    
    public PageReference getData() {      
        validateUpdate(this.rec.Id);
        PageReference pg = new PageReference('/'+this.rec.Id);
        pg.setRedirect(true); 
        return pg;
    }
    
    @RemoteAction
    global static void updateHintarivit(String idVal) {
        validateUpdate(idVal);
    }
    
    @AuraEnabled
    global static Boolean updateRecord(String idVal) {
        validateUpdate(idVal);
        return true;
    }

    @InvocableMethod(label = 'Flow Calculate Price')
    global static List<flowParams> pdfLahetys_flow(List<flowParams> inputParams) {

    try {
        /*assign the input (order) parameters to price calculate method format*/    
        List<OrderParameter> orderParams = new List<OrderParameter>();

        orderParams.add(new OrderParameter(inputParams[0].param1, inputParams[0].param1Value));

        IF(inputParams[0].param2 <> '' && inputParams[0].param2 <> null) {
            orderParams.add(new OrderParameter(inputParams[0].param2, inputParams[0].param2Value));
        }
        IF(inputParams[0].param3 <> '' && inputParams[0].param3 <> null) {
            orderParams.add(new OrderParameter(inputParams[0].param3, inputParams[0].param3Value));
        }

        IF(inputParams[0].param4 <> '' && inputParams[0].param4 <> null) {
            orderParams.add(new OrderParameter(inputParams[0].param4, inputParams[0].param4Value));
        }   

        IF(inputParams[0].param5 <> '' && inputParams[0].param5 <> null) {
            orderParams.add(new OrderParameter(inputParams[0].param5, inputParams[0].param5Value));
        }   

        IF(inputParams[0].param11 <> '' && inputParams[0].param11 <> null) {
            orderParams.add(new OrderParameter(inputParams[0].param11, inputParams[0].param11Value));
        }   

        IF(inputParams[0].param12 <> '' && inputParams[0].param12 <> null) {
            orderParams.add(new OrderParameter(inputParams[0].param12, inputParams[0].param12Value));
        }   

        IF(inputParams[0].param13 <> '' && inputParams[0].param13 <> null) {
            orderParams.add(new OrderParameter(inputParams[0].param13, inputParams[0].param13Value));
        }   

        /*calculate prices*/        
        List<PriceList> prices = KAYT_HintahakuCall(inputParams[0].productId, orderParams, 'Suomi', inputParams[0].priceDate);

        integer N = prices.size();

        IF(N >= 1) {
            inputParams[0].priceRow1 = prices[0].title + ' ' + prices[0].value + ' ' + prices[0].unit + ' ' + prices[0].reason;}
        IF(N >= 2) {
            inputParams[0].priceRow2 = prices[1].title + ' ' + prices[1].value + ' ' + prices[1].unit + ' ' + prices[1].reason;}
        IF(N >= 3) {
            inputParams[0].priceRow3 = prices[2].title + ' ' + prices[2].value + ' ' + prices[2].unit + ' ' + prices[2].reason;}
        IF(N >= 4) {
            inputParams[0].priceRow4 = prices[3].title + ' ' + prices[3].value + ' ' + prices[3].unit + ' ' + prices[3].reason;}
        IF(N >= 5) {
            inputParams[0].priceRow5 = prices[4].title + ' ' + prices[4].value + ' ' + prices[4].unit + ' ' + prices[4].reason;}

        return inputParams;
    }
    catch(Exception e) {
            inputParams[0].status = 'ERROR';
            inputParams[0].errorMsg = e.getMessage();
            return inputParams;
    }
    }

    global class flowParams{
        @InvocableVariable(required=true)
        global String productId;        

        @InvocableVariable(required=true)
        global date priceDate;

        @InvocableVariable
        global String param1;

        @InvocableVariable
        global String param2;

        @InvocableVariable
        global String param3;

        @InvocableVariable
        global String param4;

        @InvocableVariable
        global String param5;

        @InvocableVariable
        global String param11;

        @InvocableVariable
        global String param12;

        @InvocableVariable
        global String param13;

        @InvocableVariable
        global integer param1Value;

        @InvocableVariable
        global integer param2Value;

        @InvocableVariable
        global integer param3Value;

        @InvocableVariable
        global integer param4Value;

        @InvocableVariable
        global integer param5Value;

        @InvocableVariable
        global integer param11Value;

        @InvocableVariable
        global integer param12Value;

        @InvocableVariable
        global integer param13Value;

        @InvocableVariable
        global string priceRow1;

        @InvocableVariable
        global string priceRow2;

        @InvocableVariable
        global string priceRow3;        

        @InvocableVariable
        global string priceRow4;        

        @InvocableVariable
        global string priceRow5;        

        @InvocableVariable
        global string status;        

        @InvocableVariable
        global string errorMsg;        

    }
    
    private static void validateUpdate(String idVal) { 
        Tuoteosto__c rec = new Tuoteosto__c();       
        try {
            rec = [SELECT Id, x_Hintalaskuri_aktiivinen__c, Sopimustieto__r.Asiakas_tekija__r.Kieli__c, Integraatio_Tila_Hintakysely__c, Integraatio_Virheviesti_Hintakysely__c, x_Ei_automaatioita__c, Tuote__r.Id, Tuotetunnus__c, x_Hintahakupvm__c FROM Tuoteosto__c WHERE Id = :idVal LIMIT 1];
            if(rec.x_Hintalaskuri_aktiivinen__c) 
            {
                List<Tuoteostohintarivi__c> rivit = KAYT_HinnanAsettaminenTuoteostolle(rec.Sopimustieto__r.Asiakas_tekija__r.Kieli__c, idVal, rec.Tuote__r.Id, rec.Tuotetunnus__c, rec.x_Hintahakupvm__c);
                if(rivit.isEmpty()) {
                    rec.Integraatio_Tila_Hintakysely__c = 'NOPRICEROWS';
                }
                else {
                    upsert rivit x_External_Id__c;
                    rec.Integraatio_Tila_Hintakysely__c = 'SUCCESS';
                }
            rec.x_Ei_automaatioita__c = Datetime.Now();
            update rec;
            } 
       }catch(KAYT_Exception e) 
       {
            rec.Integraatio_Tila_Hintakysely__c = 'ERROR';
            rec.Integraatio_Virheviesti_Hintakysely__c = e.getMessage();
            update rec;
       }
    }
    
    global class OrderParameter {
        public String parameterCode {get; set;}
        public Decimal parameterValue {get; set;}
        public OrderParameter(String code, Decimal value) {
            this.parameterCode = code;
            this.parameterValue = value;
        }
    }
    
    global class PriceList {
        public String parameterCode {get; set;}
        public Double parameterValue {get; set;}
        public String id {get; set;}
        public String name {get; set;}
        public String account {get; set;}
        public String type {get; set;}
        public String reason {get; set;}
        public String title {get; set;}
        public String unit {get; set;}
        public String value {get; set;}
        public PriceList() {
            
        }
    }
    
    static public List<Tuoteostohintarivi__c> KAYT_HinnanAsettaminenTuoteostolle(String kieli, String orderId, String tuoteId, String tuotetunnus, Date hintahakupvm) {
        List<Tuoteostorivi__c> paramRecs = [SELECT Parametri__c, Arvo__c FROM Tuoteostorivi__c WHERE Tuoteosto__c = :orderID];
        List<OrderParameter> params = new List<OrderParameter>();
        for(Tuoteostorivi__c p : paramRecs) params.add(new OrderParameter(p.Parametri__c, p.Arvo__c));
        List<PriceList> prices = KAYT_HintahakuCall(tuotetunnus, params, kieli, hintahakupvm);
        List<Tuoteostohintarivi__c> rivit = new List<Tuoteostohintarivi__c>();
        for(PriceList p : prices) {
            Tuoteostohintarivi__c r = new Tuoteostohintarivi__c();
            r.Arvo__c = Decimal.valueOf(p.value);
            r.Kieli__c = kieli;
            r.Nimi__c = p.name;
            r.Seli__c = p.id;
            r.Selite__c = p.reason;
            r.Tili__c = p.account;
            r.Tuoteosto__c = orderID;
            r.Tyyppi__c = p.type;
            r.Yksikko__c = p.unit;
            r.Otsikko__c = p.title;
            r.x_External_Id__c = orderID + tuoteId + p.id + kieli;
            rivit.add(r);
        }
        return rivit;
    }

    static public List<PriceList> KAYT_HintahakuCall(String productId, List<OrderParameter> orderParameters, String kieli, Date priceDate) {
        List<PriceList> pList = new List<PriceList>();
        string priceDateString = DateTime.newInstance(priceDate.year(),priceDate.month(),priceDate.day()).format('YYYY-MM-dd');
        string languageString = '';

        try {            

            // language parameter defined to request
            if(kieli == 'Ruotsi') {
                languageString = '&kielikoodi=sv';
            }
            else if(kieli == 'Englanti') {
                languageString = '&kielikoodi=en';
            }

            ExtranetAPI__c params = ExtranetAPI__c.getValues('params');
    
            Http http1=new Http();
            HttpRequest req1=new HttpRequest();
            String queryParams = '?';
            for(OrderParameter oP : orderParameters) {
                if(queryParams.length()>1) queryParams += '&';
                queryParams += EncodingUtil.urlEncode(oP.parameterCode, 'UTF-8') + '=' + oP.parameterValue;
            }
            if(queryParams.length() == 1) queryParams = '';
//            req1.setendpoint(params.endpointURL__c+params.priceRequestURL__c+productID+'/prices'+'/'
//                             +priceDateString+queryParams+languageString); //inactivated due to new sora-api of Teosto
            req1.setendpoint(
                    params.endpointURL__c+params.priceRequestURL__c+EncodingUtil.urlEncode(productID, 'UTF-8')+'/prices'+'/'
                    +priceDateString+queryParams+languageString
            );

            req1.Setheader('Authorization','Bearer '+ params.Value__c);
            req1.Setheader('Content-Type', 'application/json');
            req1.setmethod('GET');
            HttpResponse res1;
            res1 = http1.send(req1);
            String str=res1.getbody();
            System.debug(str);
            KAYT_HintahakuJSON row = (KAYT_HintahakuJSON) JSON.deserialize(str, KAYT_HintahakuJSON.class);
            for(KAYT_HintahakuJSON.PriceRow r : row.priceRows) {
                PriceList pL = new PriceList();
                pL.id = r.id;
                pL.name = r.name;
                pL.account = r.account;
                pL.type = r.type;
                pL.reason = r.reason;
                pL.title = r.title;
                pL.unit = r.unit;
                pL.value = r.value;
                pList.add(pL);
            }
        } catch(Exception e) {
        }
        return pList;
    }
}