global class KAYT_TapahtumakansiohakuCall {
    
    @InvocableMethod
    public static List<FolderRESTCall> GetFolder(List<FolderRESTCall> inputParams) 
    {
        if(inputparams[0].requestType == 'yearly') {
            return GetFolderYearly(inputParams);
        } else {
            return GetFolderMonthly(inputParams);
        }
        return null;
    }

    public static List<FolderRESTCall> GetFolderMonthly(List<FolderRESTCall> inputParams)
    {
        FolderRESTCall output = new FolderRESTCall();
        List<FolderRESTCall> outputs = new List<FolderRESTCall>();
        String Status = 'ERROR';
        Integer lastDayOfMonth;

        /* define date lists to gather dates from integration response */
        List<Date> dateList = new List<Date>();
        List<Date> dateList_mins = new List<Date>();
        
        try{
            /* assign absolute minimum to date, last day of two years ago */
            dateList.add(Date.newInstance(Date.Today().Year() - 2, 12, 31));


            /* fetch integration parameters and execute integration call */
            ExtranetAPI__c params = ExtranetAPI__c.getValues('params');
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(params.endpointURL__c + 'laskutin/monthly/'+inputParams[0].orderId+'');
            string header = 'Bearer ' +params.Value__c;
            request.setHeader('Authorization', header);
            request.setMethod('GET');
            HttpResponse response = http.send(request);
            system.debug(response.getBody());
        
            /* status 200 is success code */
            if (response.getStatusCode() == 200) 
            {
                List<FolderJSON> results = (List<FolderJSON>) JSON.deserialize(response.getBody(),List<FolderJSON>.class);
                if(results.size() == 0) {
                    output.minEndDate = Date.newInstance(1900, 1, 1);
                    Status = 'SUCCESS';
                }
                else {
                    for (FolderJSON res: results)
                    {
                        /* calculate last day of the returned year and month */
                        lastDayofMonth = date.daysInMonth(res.vuosi, res.kk);
    
                        /* assign date related to laskutustila */
                        if(res.laskutustila != '' && res.laskutustila != ' ' && res.laskutustila != 'EIL')
                            dateList.add(Date.newInstance(res.vuosi, res.kk, lastDayOfMonth));
    
                        /* assign date related to vahvistustila*/
                        if(res.vahvistustila == 'VAH')
                            dateList.add(Date.newInstance(res.vuosi, res.kk, lastDayOfMonth));
                            
                        /* find minimum of all returned dates*/
                        dateList_mins.add(Date.newInstance(res.vuosi, res.kk, 1) - 1);
                    }
                        /* add minimum for min-list to date list by first sorting the list and getting first value */
                        dateList_mins.sort();
                        dateList.add(dateList_mins.get(0));
                        /* fetch maximum from the list by first sorting the list and getting highest value and return that date as minimum end date */
                        dateList.sort();
                        output.minEndDate = dateList.get(dateList.size() - 1);
                        Status = 'SUCCESS';
               }   
            }
            /* status 404 is success because invoice folders are not created yet, which is success scenario */
            if (response.getStatusCode() == 404) {
                output.minEndDate = Date.newInstance(1900, 1, 1);
                Status = 'SUCCESS';
            }

        } catch (Exception e) 
        {
            output.errMsg = e.getMessage();
        }
        finally
        {       
            output.Status = Status;
            outputs.add(output);
        }
         
        return outputs; 
    }

    public static List<FolderRESTCall> GetFolderYearly(List<FolderRESTCall> inputParams) {
        FolderRESTCall output = new FolderRESTCall();
        List<FolderRESTCall> outputs = new List<FolderRESTCall>();
        String Status = 'ERROR';
        Decimal nettoVuosi = 0;
        Integer maxVuosi = 1900;
        output.minEndDate = Date.newInstance(Date.Today().Year() - 2, 12, 31);
        output.isCancellable = true;

        try{
            /* fetch integration parameters and execute integration call */
            ExtranetAPI__c params = ExtranetAPI__c.getValues('params');
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(params.endpointURL__c + 'laskutin/yearly/'+inputParams[0].orderId+'');
            string header = 'Bearer ' +params.Value__c;
            request.setHeader('Authorization', header);
            request.setMethod('GET');
            HttpResponse response = http.send(request);
            system.debug(response.getBody());
        
            if (response.getStatusCode() == 200) 
            {
                List<FolderJSONYearly> results = (List<FolderJSONYearly>) JSON.deserialize(response.getBody(),List<FolderJSONYearly>.class);
                if(results.size() == 0) {
                    Status = 'SUCCESS';
                }
                else {                
                    for (FolderJSONYearly res: results)
                    {
                        if(res.vuosi > maxVuosi) {
                            for(yearlyRows rows : res.laskurivit) {
                                if(rows.laskutustila != 'OHI') nettoVuosi += rows.netto;
                            }
                        }
                        if(nettoVuosi != 0) {
                            system.debug('vuosi: ' + res.vuosi + ' ' + nettovuosi);
                            maxVuosi = res.vuosi;
                            output.isCancellable = false;
                            nettoVuosi = 0;
                        }
                    }
                    Status = 'SUCCESS';
                    if(output.minEndDate < Date.newInstance(maxVuosi, 12, 31))
                        output.minEndDate = Date.newInstance(maxVuosi, 12, 31);
                }   
            }
        }
        catch (Exception e) {
            output.errMsg = e.getMessage();
        }
        finally
        {       
            output.Status = Status;
            outputs.add(output);
        }
    
        return outputs; 
    }
    
    public class FolderJSON
    {
        public String lomakeno;
        public Integer vuosi;
        public Integer kk;
        public String vahvistustila;
        public String laskutustila;
    }

    public class FolderJSONYearly
    {
        public Integer lomakeno;
        public Integer vuosi;
        public String laskutustila;
        public List<yearlyRows> laskurivit;
    }
    public class yearlyRows
    {
        public Integer id;
        public Integer vuosi;
        public String laskutustila;
        public Decimal netto;
    }
    
    public class FolderRESTCall 
    {
        @InvocableVariable
        public Date minEndDate;
    
        @InvocableVariable
        public String Status;

        @InvocableVariable
        public String orderId;

        @InvocableVariable
        public String requestType;

        @InvocableVariable
        public Boolean isCancellable;
        
        @InvocableVariable
        public String errMsg;
    }
}