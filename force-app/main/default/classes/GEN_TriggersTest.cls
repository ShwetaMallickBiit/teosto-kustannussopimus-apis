/*
Description: 
            This Apex class is test class for following Apex Triggers: 
                1) GENValidatointi_EiPoistamista (Esityspaikka__c)
                2) KAYT_ValidationVapaaEsityspaikka (Sopimustieto__c)
*/

@isTest(seeAllData=true)
private class GEN_TriggersTest {

    static Account acc;
    static Account acc2;
    static Contact con;
    static Contact con2;
    static AccountContactRelation acc_con_standard;
    static AccountContactRelation_c__c acc_con;

    /* this method creates seed data for all test methods */
    public static void luoData() {

       acc = new Account(
           Name = 'TestAccount', Kieli__c = 'Suomi', Asiakasryhma__c = 'Käyttäjä', Luokittelu__c ='Avain',
           Ohita_Y_tunnus_Hetu_validointi__c = true, ShippingStreet = 'Testikatu 10', ShippingCity = 'Helsinki',
           ShippingPostalCode = '00100');
       insert acc;

       acc2 = new Account(
           Name = 'TestAccount', Kieli__c = 'Suomi', Asiakasryhma__c = 'Käyttäjä', Luokittelu__c ='Avain',
           Ohita_Y_tunnus_Hetu_validointi__c = true, ShippingStreet = 'Testikatu 10', ShippingCity = 'Helsinki',
           ShippingPostalCode = '00100');
       insert acc2;

       con = new Contact(
           AccountId = acc.Id, FirstName = 'Etunimi', LastName = 'Sukunimi');
       insert con;

       con2 = new Contact(
           AccountId = acc2.id, FirstName = 'Etunimi2', LastName = 'Sukunimi2');
       insert con2;

       acc_con_standard = new AccountContactRelation(
           AccountId = acc.Id, ContactId = con2.Id);
       insert acc_con_standard;

       acc_con = new AccountContactRelation_c__c(
           AccountId_c__c = acc.Id, ContactId_c__c = con.Id,Name = 'Testinimi', Sidoksen_email_c__c = 'testi.testinen@biit.fi');
       insert acc_con;

    }

    static testMethod void GENValidatointi_EiPoistamistaTest() {

        Test.startTest();
        luoData();
        try {
            Delete (acc_con_standard);
        } catch(DMLexception e) {
            system.assert(e.getMessage().contains('Sinulla ei ole oikeutta poistaa suhdetietuetta. Ota tarvittaessa yhteyttä pääkäyttäjään.'),
                                                  'Testi ei onnistunut (genValidatointi_EiPoistamistaTest)');
        }
        Test.stopTest();

    }

    static testMethod void KAYT_ValidationVapaaEsityspaikkaTest() {

        /* creates seed data */
        luoData();

        Test.startTest();
        
        /* this nulls SOQL limits after seed data insert */
        System.assertEquals('1', '1'); 
        // create Sopimustieto record
       Id RecordTypeIdST = Schema.SObjectType.Sopimustieto__c.getRecordTypeInfosByName().get('Musiikinkäyttösopimus, sopimusvahvistus').getRecordTypeId(); 

        Sopimustieto__c sop = new Sopimustieto__c(
            recordtypeid=RecordTypeIdST, Asiakas_tekija__c = acc.Id, OnEri_kuin_postitusosoite__c = true,
            Laskutusosoite_Katu__c = 'Testikatu 1', Laskutusosoite_Kaupunki__c = 'Helsinki', Laskutusosoite_Postinumero__c = '00100',
            x_onTuoteosto__c = true, Vastuuhenkilo__c = acc_con.Id, Allekirjoittaja__c = acc_con.Id, Tila__c = 'Avoin',
            Alkupvm__c = Date.newInstance(2018, 01, 01), Sopimuksen_allekirjoituspvm__c = Date.newInstance(2018, 01, 01));
        insert sop;

        // create Esityspaikka record
        Esityspaikka__c ep = new Esityspaikka__c(
            Name = 'Testi EP', Lahiosoite__c = 'Testikatu 1', Postinumero__c = '00100', Postitoimipaikka__c = 'Helsinki', Yleisokapasiteetti__c = 1000
        );
        insert ep;
        
        sop.x_Esityspaikka__c = ep.Id;
        try {
            update sop;
        } catch(DMLexception e) {
            throw e;
        }

        Test.stopTest();

    }

    static testMethod void KAYT_ValidationVapaaEsityspaikkaFailureTest() {

        string errorMsg;

        /* creates seed data */
        luoData();

        Test.startTest();
        
        /* this nulls SOQL limits after seed data insert */
        System.assertEquals('1', '1'); 
        // create Sopimustieto record
       Id RecordTypeIdST = Schema.SObjectType.Sopimustieto__c.getRecordTypeInfosByName().get('Musiikinkäyttösopimus, sopimusvahvistus').getRecordTypeId(); 

        Sopimustieto__c sop = new Sopimustieto__c(
            recordtypeid=RecordTypeIdST, Asiakas_tekija__c = acc.Id, OnEri_kuin_postitusosoite__c = true,
            Laskutusosoite_Katu__c = 'Testikatu 1', Laskutusosoite_Kaupunki__c = 'Helsinki', Laskutusosoite_Postinumero__c = '00100',
            x_onTuoteosto__c = true, Vastuuhenkilo__c = acc_con.Id, Allekirjoittaja__c = acc_con.Id, Tila__c = 'Avoin',
            Alkupvm__c = Date.newInstance(2018, 01, 01), Sopimuksen_allekirjoituspvm__c = Date.newInstance(2018, 01, 01));
        insert sop;

        // create Esityspaikka record
        Esityspaikka__c ep = new Esityspaikka__c(
            Name = 'Testi EP', Lahiosoite__c = 'Testikatu 1', Postinumero__c = '00100', Postitoimipaikka__c = 'Helsinki', Yleisokapasiteetti__c = 1000
        );
        insert ep;

        // create Esityspaikkahistoria record
        Esityspaikkahistoria__c eph = new Esityspaikkahistoria__c(
            Asiakas__c = acc.Id, Sopimustieto__c = sop.Id, Esityspaikka__c = ep.Id, Alkupvm__c = Date.newInstance(2018, 01, 01)
        );
        insert eph;
        
        sop.x_Esityspaikka__c = ep.Id;
        try {
            update sop;
        } catch(system.DMLexception e) {
            errorMsg = e.getMessage();
            system.assert(errorMsg.contains('Esityspaikka on sidottu asiakkaalle'), 'Testi ei onnistunut (KAYT_ValidationVapaaEsityspaikkaFailureTest)');  
        }

        Test.stopTest();

    }

}