@isTest
global class Teosto_httpMockCallout implements HttpCalloutMock {
global String responseHeader {get; set;}
global String responseJSON {get; set;}
global Integer responseCode {get; set;}

public Teosto_httpMockCallout(String s, Integer i) {
	this.responseJSON = s;
	this.responseCode = i;
}

public Teosto_httpMockCallout(String h, String s, Integer i) {
	this.responseHeader = h;
	this.responseJSON = s;
	this.responseCode = i;
}

global HTTPResponse respond(HTTPRequest req) {

	// Create a fake response
	HttpResponse res = new HttpResponse();
	if(this.responseHeader != null) {
		res.setHeader('Location', this.responseHeader);
	}
	res.setHeader('Content-Type', 'application/json');
	res.setBody(this.responseJSON);
	res.setStatusCode(this.responseCode);
	return res;
}

}