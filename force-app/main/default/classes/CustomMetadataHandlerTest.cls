@isTest
public with sharing class CustomMetadataHandlerTest {

    @isTest
    static void getItsehallinnoinninTilaLabelAndKeyTest() {
        String label = CustomMetadataHandler.getItsehallinnoinninTilaLabel('OH');
        System.assertEquals('Waiting for processing', label, 'Itsehallinnoinnin tila label fetched from them custom metadata types was not expected (Waiting for processing), but ' + label);
        String key = CustomMetadataHandler.getItsehallinnoinninTilaKey(label);
        System.assertEquals('OH', key, 'Itsehallinnoinnin tila key fetched from them custom metadata types was not expected (OH), but ' + label);
    }

    @isTest
    static void getItsehallinnoinninTilaLabelAndKeyFailTest() {
        String label = CustomMetadataHandler.getItsehallinnoinninTilaLabel('fdsfsd');
        System.assertEquals('', label, 'Itsehallinnoinnin tila label fetched from them custom metadata types was not expected (empty string), but ' + label);
        String key = CustomMetadataHandler.getItsehallinnoinninTilaKey('fsdfs');
        System.assertEquals('', key, 'Itsehallinnoinnin tila key fetched from them custom metadata types was not expected (empty string), but ' + label);
    }


    @isTest
    static void getItsehallinnoinninTyyppiLabelAndKeyTest() {
        String label = CustomMetadataHandler.getItsehallinnoinninTyyppiLabel('LI');
        System.assertEquals('Live event', label, 'Itsehallinnoinnin tyyppi label fetched from them custom metadata types was not expected (Live event), but ' + label);
        String key = CustomMetadataHandler.getItsehallinnoinninTyyppiKey(label);
        System.assertEquals('LI', key, 'Itsehallinnoinnin tyyppi key fetched from them custom metadata types was not expected (LI), but ' + label);
    }

    @isTest
    static void getItsehallinnoinninTyyppiLabelAndKeyFailTest() {
        String label = CustomMetadataHandler.getItsehallinnoinninTyyppiLabel('fdsfsd');
        System.assertEquals('', label, 'Itsehallinnoinnin tyyppi label fetched from them custom metadata types was not expected (empty string), but ' + label);
        String key = CustomMetadataHandler.getItsehallinnoinninTyyppiKey('fsdfs');
        System.assertEquals('', key, 'Itsehallinnoinnin tyyppi key fetched from them custom metadata types was not expected (empty string), but ' + label);
    }



    @isTest
    static void getItsehallinnoinninTeoksenTilaLabelAndKeyTest() {
        String label = CustomMetadataHandler.getItsehallinnoinninTeoksenTilaLabel('OH');
        System.assertEquals('Waiting for acceptance', label, 'Itsehallinnoinnin teoksen tila label fetched from them custom metadata types was not expected (Waiting for processing), but ' + label);
        String key = CustomMetadataHandler.getItsehallinnoinninTeoksenTilaKey(label);
        System.assertEquals('OH', key, 'Itsehallinnoinnin teoksen tila key fetched from them custom metadata types was not expected (OH), but ' + label);
    }

    @isTest
    static void getItsehallinnoinninTeoksenTilaLabelAndKeyFailTest() {
        String label = CustomMetadataHandler.getItsehallinnoinninTeoksenTilaLabel('fdsfsd');
        System.assertEquals('', label, 'Itsehallinnoinnin teoksen tila label fetched from them custom metadata types was not expected (empty string), but ' + label);
        String key = CustomMetadataHandler.getItsehallinnoinninTeoksenTilaKey('fsdfs');
        System.assertEquals('', key, 'Itsehallinnoinnin teoksen tila key fetched from them custom metadata types was not expected (empty string), but ' + label);
    }

    @isTest
    static void getItsehallinnoinninTekijanRooliLabelAndKeyTest() {
        String label = CustomMetadataHandler.getItsehallinnoinninTekijanRooliLabel('HY');
        System.assertEquals('Author, Composer or Editor of the work whose acceptance is required', label, 'Itsehallinnoinnin tekijän rooli label fetched from them custom metadata types was not expected (Author, Composer or Editor of the work whose acceptance is required), but ' + label);
        String key = CustomMetadataHandler.getItsehallinnoinninTekijanRooliKey(label);
        System.assertEquals('HY', key, 'Itsehallinnoinnin tekijän rooli key fetched from them custom metadata types was not expected (HY), but ' + label);
    }

    @isTest
    static void getItsehallinnoinninTekijanRooliLabelAndKeyFailTest() {
        String label = CustomMetadataHandler.getItsehallinnoinninTekijanRooliLabel('fdsfsd');
        System.assertEquals('', label, 'Itsehallinnoinnin tekijän rooli label fetched from them custom metadata types was not expected (empty string), but ' + label);
        String key = CustomMetadataHandler.getItsehallinnoinninTekijanRooliKey('fsdfs');
        System.assertEquals('', key, 'Itsehallinnoinnin tekijän rooli key fetched from them custom metadata types was not expected (empty string), but ' + label);
    }

    @isTest
    static void getItsehallinnoinninTekijanPeruutusTilaLabelAndKeyTest() {
        String label = CustomMetadataHandler.getItsehallinnoinninTekijanPeruutusTilaLabel('HL');
        System.assertEquals('Cancellation denied', label, 'Itsehallinnoinnin tekijän peruutustila label fetched from them custom metadata types was not expected (Cancellation denied), but ' + label);
        String key = CustomMetadataHandler.getItsehallinnoinninTekijanPeruutusTilaKey(label);
        System.assertEquals('HL', key, 'Itsehallinnoinnin tekijän peruutustila key fetched from them custom metadata types was not expected (HL), but ' + label);
    }

    @isTest
    static void getItsehallinnoinninTekijanPeruutusTilaLabelAndKeyFailTest() {
        String label = CustomMetadataHandler.getItsehallinnoinninTekijanPeruutusTilaLabel('fdsfsd');
        System.assertEquals('', label, 'Itsehallinnoinnin tekijän peruutustila label fetched from them custom metadata types was not expected (empty string), but ' + label);
        String key = CustomMetadataHandler.getItsehallinnoinninTekijanPeruutusTilaKey('fsdfs');
        System.assertEquals('', key, 'Itsehallinnoinnin tekijän peruutustila key fetched from them custom metadata types was not expected (empty string), but ' + label);
    }

    @isTest
    static void getItsehallinnoinninTyonPeruutusTilaLabelAndKeyTest() {
        String label = CustomMetadataHandler.getItsehallinnoinninTyonPeruutusTilaLabel('HV');
        System.assertEquals('Accepted', label, 'Itsehallinnoinnin työn peruutustila label fetched from them custom metadata types was not expected (Accepted), but ' + label);
        String key = CustomMetadataHandler.getItsehallinnoinninTyonPeruutusTilaKey(label);
        System.assertEquals('HV', key, 'Itsehallinnoinnin työn peruutustila key fetched from them custom metadata types was not expected (HV), but ' + label);
    }

    @isTest
    static void getItsehallinnoinninTyonPeruutusTilaLabelAndKeyFailTest() {
        String label = CustomMetadataHandler.getItsehallinnoinninTyonPeruutusTilaLabel('fdsfsd');
        System.assertEquals('', label, 'Itsehallinnoinnin työn peruutustila label fetched from them custom metadata types was not expected (empty string), but ' + label);
        String key = CustomMetadataHandler.getItsehallinnoinninTyonPeruutusTilaKey('fsdfs');
        System.assertEquals('', key, 'Itsehallinnoinnin työn peruutustila key fetched from them custom metadata types was not expected (empty string), but ' + label);
    }

    @isTest
    static void getHyvaksynnanTilaLabelAndKeyTest() {
        String label = CustomMetadataHandler.getHyvaksynnanTilaLabel('HV');
        System.assertEquals('Accepted', label, 'Hyväksynnän tila label fetched from them custom metadata types was not expected (Accepted), but ' + label);
        String key = CustomMetadataHandler.getHyvaksynnanTilaKey(label);
        System.assertEquals('HV', key, 'Hyväksynnän tila key fetched from them custom metadata types was not expected (HV), but ' + label);
    }

    @isTest
    static void getHyvaksynnanTilaLabelAndKeyFailTest() {
        String label = CustomMetadataHandler.getHyvaksynnanTilaLabel('fdsfsd');
        System.assertEquals('', label, 'Hyväksynnän tila label fetched from them custom metadata types was not expected (empty string), but ' + label);
        String key = CustomMetadataHandler.getHyvaksynnanTilaKey('fsdfs');
        System.assertEquals('', key, 'Hyväksynnän tila key fetched from them custom metadata types was not expected (empty string), but ' + label);
    }

    @isTest
    static void getItsehallinnoinninPeruutuksenTilaLabelAndKeyTest() {
        String label = CustomMetadataHandler.getItsehallinnoinninPeruutuksenTilaLabel('OH');
        System.assertEquals('Termination started, waiting for acceptance', label, 'Itsehallinnoinnin peruutuksen tila label fetched from them custom metadata types was not expected (Termination started, waiting for acceptance), but ' + label);
        String key = CustomMetadataHandler.getItsehallinnoinninPeruutuksenTilaKey(label);
        System.assertEquals('OH', key, 'Itsehallinnoinnin peruutuksen tila key fetched from them custom metadata types was not expected (OH), but ' + label);
    }

    @isTest
    static void getItsehallinnoinninPeruutuksenTilaLabelAndKeyFailTest() {
        String label = CustomMetadataHandler.getItsehallinnoinninPeruutuksenTilaLabel('fdsfsd');
        System.assertEquals('', label, 'Itsehallinnoinnin peruutuksen tila label fetched from them custom metadata types was not expected (empty string), but ' + label);
        String key = CustomMetadataHandler.getItsehallinnoinninPeruutuksenTilaKey('fsdfs');
        System.assertEquals('', key, 'Itsehallinnoinnin peruutuksen tila key fetched from them custom metadata types was not expected (empty string), but ' + label);
    }

}