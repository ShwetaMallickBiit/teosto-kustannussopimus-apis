public class KAYT_SopimusvahvistusPDF_controller {
    
    private final ApexPages.StandardController stdController;
    private final Sopimustieto__c contract;
    public List<Tuoteostorivi__c> contractDetails{get;set;}
    public List<Tuoteosto__c> contractDataLst{get;set;}
    public List<Tuoteostohintarivi__c> contractPrices{get;set;}
    public List<Arvolista__c> labelLst{get;set;}
    public Arvolista__c labels{get;set;}
    public final string kieli{get;set;}
    private Id accId;
    public List<StaticResource> docs;
    public String content{get; set;}
    
    public KAYT_SopimusvahvistusPDF_controller (ApexPages.StandardController stc) {
         
        this.stdController = stc;
        this.contract = (Sopimustieto__c) stc.getRecord();

        // Get the account Id 
        List<Sopimustieto__c> stLst =[SELECT Id, Asiakas_tekija__c from Sopimustieto__c WHERE Id =:contract.Id];  
        accId = stLst[0].Asiakas_tekija__c;      
        kieli = [SELECT Kieli__c FROM Account WHERE Id = :accId].Kieli__c; 

        //Get labels
        labelLst = [SELECT Arvo_1_RichText__c, Arvo_2_RichText__c, Arvo_3_RichText__c, Arvo_4_RichText__c, Arvo_5_RichText__c, 
                    Arvo_6_RichText__c, Arvo_7_RichText__c, Arvo_8_RichText__c, Arvo_9_RichText__c, Arvo_10_RichText__c, 
                    Arvo_11_RichText__c,Arvo_12_RichText__c, Arvo_13_RichText__c, Arvo_14_RichText__c, Arvo_15_RichText__c, 
                    Arvo_16_RichText__c, Arvo_17_RichText__c, Arvo_18_RichText__c, Arvo_19_RichText__c, Arvo_20_RichText__c, 
                    Arvo_21_RichText__c, Arvo_22_RichText__c, Arvo_23_RichText__c, Arvo_24_RichText__c, Arvo_25_RichText__c, 
                    Arvo_26_RichText__c, Arvo_27_RichText__c, Arvo_28_RichText__c , Arvo_29_RichText__c,Arvo_30_RichText__c,
                    Arvo_31_RichText__c, Arvo_32_RichText__c, Arvo_33_RichText__c, Arvo_34_RichText__c
                    FROM Arvolista__c WHERE Ryhma__c = 'Sopimusvahvistusteksti' AND Arvo_1_teksti__c =:kieli];
        if( !labelLst.isEmpty() )
            labels = labelLst[0];       
        
        // get contract data
        contractDataLst = [SELECT Esityspaikkasidos__r.Esityspaikka__r.Name, Esityspaikkasidos__r.Name, Esityspaikkasidos__r.Nimi__c, Esityspaikkasidos__r.Esityspaikka__r.Lahiosoite__c, 
                        Esityspaikkasidos__r.Esityspaikka__r.Postinumero__c, Esityspaikkasidos__r.Esityspaikka__r.Postitoimipaikka__c,
                        x_Esityspaikkanro__c, Esityspaikkasidos__r.Alkupvm__c, Tuote__r.Erityisehdot__c, Alkupvm_laskutus__c, 
                        Tuote__r.Erityisehdot_eng__c, Tuote__r.Erityisehdot_ruotsi__c, Tuote__r.Tuoteselite__c, 
                        Tuote__r.Tuoteselite_eng__c, Tuote__r.Tuoteselite_swe__c, x_Tuotenimi_sv__c, x_Voimassaolo_sv__c, Ei_voimassa__c,
                        Tarkenne__c, x_Ei_voimassa_sv__c, x_Ei_voimassa_sv_eng__c, x_Ei_voimassa_sv_swe__c, Sopimustieto__r.Asiakas_tekija__r.Kieli__c
                        FROM Tuoteosto__c WHERE Sopimustieto__c =: contract.Id AND x_Tulosta_sopimusvahvistukselle__c = TRUE 
                        ORDER BY x_Jarjestys__c];
        List<Id> contractIds = new List<Id>();
        for(Tuoteosto__c con : contractDataLst )
            contractIds.add(con.Id);
        
        // get contract details
        contractDetails = [SELECT Id, Parametri__c, x_Jarjestys__c, Tuoteosto__c, x_Otsikko_sv__c, x_Parametri_arvo_yksikko_sv__c 
                          FROM Tuoteostorivi__c WHERE Tuoteosto__c IN : contractIds ORDER BY x_Jarjestys__c];
        
        // get contract prices
        contractPrices = [SELECT Id, Tuoteosto__c, x_Hintarivi_sv__c FROM Tuoteostohintarivi__c WHERE Tuoteosto__c IN : contractIds AND 
                          Kieli__c = :kieli ORDER BY x_Jarjestys_sv__c];
     }
}