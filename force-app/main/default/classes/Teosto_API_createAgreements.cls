public with sharing class Teosto_API_createAgreements {
    static Map<String,Arvolista__c> arvolistaMap;

    public static List<String> getIncomingAccountIds(List<KAYT_API_SalesforceRequest.inboundAgreement> paramAgreements) {
        List<String> ids = new List<String>();
        for(KAYT_API_SalesforceRequest.inboundAgreement i : paramAgreements) { 
            if(i.tekijanro!= null) {
                ids.add(i.tekijanro);
            }
        }
        return ids;
    }
    
    public static Map<String, Account> getAccountMap(List<String> ids) {
        Map<String,Account> accountMap = new Map<String,Account>();
        List<Account> accounts = [SELECT Id, Asiakasnumero_Tepa__c FROM Account WHERE Asiakasnumero_Tepa__c IN :ids];
        for(Account a : accounts) {
            accountMap.put(a.Asiakasnumero_Tepa__c, a);
        }
        return accountMap;   
    }

    public static Map<String, Arvolista__c> getArvolistaMap() {
        system.debug(arvolistaMap);
        if(arvolistaMap == null) {
            arvolistaMap = new Map<String,Arvolista__c>();
            List<Arvolista__c> arvolistat = [SELECT Id, Arvo_3_teksti__c FROM Arvolista__c];
            for(Arvolista__c a : arvolistat) {
                arvolistaMap.put(a.Arvo_3_teksti__c, a);
            }
        }
        return arvolistaMap;   
    }

    public static Sopimustieto__c mapAgreement(KAYT_API_SalesforceRequest.inboundAgreement i, Map<String, Account> accountMap, String externalId) {
        
        Id recordTypeId = 
        Schema.SObjectType.Sopimustieto__c.getRecordTypeInfosByName().get('Kustannussopimus').getRecordTypeId();

        Sopimustieto__c s = new Sopimustieto__c();
        s.RecordTypeId = recordTypeId;
        if(i.id != ''){
            s.Id = i.id;
        }
        s.Tila__c = i.tila;
        s.Muistiinpanot__c = i.muistiinpanot;
        if(i.sopimusNumero != null){
            s.Sopimusnumero__c = String.valueOf(i.sopimusNumero);
        }
        s.Sopimuksen_nimi__c = i.sopimuksenNimi;
        s.AsiakasNumero__c = i.tekijanro;
        s.Alkupvm__c = i.alkupvm;
        s.Loppupvm__c = i.loppupvm;
        if(i.id == ''){
            if(accountMap.containsKey(i.tekijanro)){
                s.Asiakas_tekija__c = accountMap.get(i.tekijanro).Id;
            }
        }
        if(i.ipinro != null){
            s.IPI_Numero__c = String.valueOf(i.ipinro);
            s.IPInumero__c = getIPINumeroId(String.valueOf(i.ipinro));
        }
        s.ExternalId__c = externalId;
        return s;
    }
    
    public static Sopimuksen_territorioryhma__c mapRegions(String tekijanro, String alikustantaja, String agreeExternalId) {
        Id recordTypeId = 
        Schema.SObjectType.Sopimuksen_territorioryhma__c.getRecordTypeInfosByName().get('Kustannussopimus').getRecordTypeId();

        String prefix = System.now().format('yyyy-MM-dd');
        prefix += '-'+ System.now().getTime();
        prefix += '-' + System.now().millisecond();
        Integer j = 0;

        String externalId = prefix+'-'+String.valueOf(++j)+'region';
        Sopimuksen_territorioryhma__c r = new Sopimuksen_territorioryhma__c();
        r.RecordTypeId = recordTypeId;
        r.ExternalId__c = externalId;
        r.Alikustantaja__c = alikustantaja;
        r.Sopimustieto__r = new Sopimustieto__c(ExternalId__c = agreeExternalId);
        return r;
    }

    public static Sopimuksen_territorio__c mapTerritories(String territory, Sopimustieto__c s, Sopimuksen_territorioryhma__c g, Boolean allowed) {
        Map<String,Arvolista__c> arvolistaMap = getArvolistaMap();

        Id recordTypeId = 
        Schema.SObjectType.Sopimuksen_territorio__c.getRecordTypeInfosByName().get('Kustannussopimus').getRecordTypeId();

        Sopimuksen_territorio__c t = new Sopimuksen_territorio__c();
        t.RecordTypeId = recordTypeId;
        if(arvolistaMap.containsKey(territory)){
            t.Territorio__c = arvolistaMap.get(territory).Id;
        }
        t.Sopimustieto__r = new Sopimustieto__c(ExternalId__c = s.ExternalId__c);
        t.Laajuudessa__c = allowed;
        t.Sopimuksen_territorioryhma__r = new Sopimuksen_territorioryhma__c(ExternalId__c = g.ExternalId__c);
        
        return t;
    }

    public static Sopimuksen_oikeusluokka__c mapShares(String type, Double sharePercent, Sopimustieto__c s, Sopimuksen_territorioryhma__c g) {

        Id recordTypeId = 
        Schema.SObjectType.Sopimuksen_oikeusluokka__c.getRecordTypeInfosByName().get('Kustannussopimus').getRecordTypeId();

        Sopimuksen_oikeusluokka__c share = new Sopimuksen_oikeusluokka__c();
        share.RecordTypeId = recordTypeId;
        share.Tyyppi__c = type;
        share.Osuus__c = sharePercent;
        share.Sopimustieto__r = new Sopimustieto__c(ExternalId__c = s.ExternalId__c);
        share.Sopimuksen_territorioryhma__r = new Sopimuksen_territorioryhma__c(ExternalId__c = g.ExternalId__c);
        
        return share;
    }

    public static Id getIPINumeroId(String ipiNum){
        Id ipiNumeroId;
        List<IPI_numero__c> ipiList = [SELECT Id FROM IPI_numero__c WHERE IPI_numero_tekstina__c =:ipiNum LIMIT 1];
        for(IPI_numero__c i : ipiList){
            ipiNumeroId = i.Id;
        }
        return ipiNumeroId;
    }
    
    public static List<Sopimustieto__c> getAgreements(List<KAYT_API_SalesforceRequest.inboundAgreement> paramCases, Map<String, Account> accountMap) {
        List<Sopimustieto__c> incomingAgreements = new List<Sopimustieto__c>();  
        String prefix = System.now().format('yyyy-MM-dd');
        prefix += '-'+ System.now().getTime();
        prefix += '-' + System.now().millisecond();
        Integer j = 0;
        for(KAYT_API_SalesforceRequest.inboundAgreement i : paramCases) { 
            String externalId = prefix+'-'+String.valueOf(++j);
            Sopimustieto__c s = mapAgreement(i, accountMap, externalId);
            incomingAgreements.add(s);    
        }
        return incomingAgreements;
    }


    public static List<Sopimuksen_territorioryhma__c> getRegions(List<KAYT_API_SalesforceRequest.inboundAgreement> paramCases, List<Sopimustieto__c> incomingAgreements) {
        List<Sopimuksen_territorioryhma__c> incomingRegions = new List<Sopimuksen_territorioryhma__c>();  
        Integer j = 0;
        for(KAYT_API_SalesforceRequest.inboundAgreement i : paramCases) { 
            Sopimustieto__c s = incomingAgreements[j];
            if(i.sopimusalueet != null){
                for(KAYT_API_SalesforceRequest.regions r : i.sopimusalueet){
                    Sopimuksen_territorioryhma__c region = mapRegions(i.tekijanro, r.alikustantaja, s.ExternalId__c);
                    incomingRegions.add(region);   
                } 
            }
            j++;
        }
        return incomingRegions;
    }

    public static List<Sopimuksen_territorio__c> getTerritories(List<KAYT_API_SalesforceRequest.inboundAgreement> paramCases, List<Sopimuksen_territorioryhma__c> incomingRegions, List<Sopimustieto__c> incomingAgreements) {
        List<Sopimuksen_territorio__c> incomingTerritories = new List<Sopimuksen_territorio__c>();  
        Integer a = 0;
        Integer b = 0;
        for(KAYT_API_SalesforceRequest.inboundAgreement i : paramCases) { 
            Sopimustieto__c agreement = incomingAgreements[a];
            if(i.sopimusalueet != null){
                for(KAYT_API_SalesforceRequest.regions r : i.sopimusalueet){
                    Sopimuksen_territorioryhma__c region = incomingRegions[b];
                    if(r.territoriot != null){
                        for(String str : r.territoriot){
                            Sopimuksen_territorio__c t = mapTerritories(str, agreement, region, true);
                            incomingTerritories.add(t);
                        }
                    }
                    if(r.poissuljetutTerritoriot != null){
                        for(String str : r.poissuljetutTerritoriot){
                            Sopimuksen_territorio__c t = mapTerritories(str, agreement, region, false);
                            incomingTerritories.add(t);
                        }
                    }
                    b++;
                } 
            }
            a++;
        }
        return incomingTerritories;
    }

    public static List<Sopimuksen_oikeusluokka__c> getShares(List<KAYT_API_SalesforceRequest.inboundAgreement> paramCases, List<Sopimuksen_territorioryhma__c> incomingRegions, List<Sopimustieto__c> incomingAgreements) {
        List<Sopimuksen_oikeusluokka__c> incomingShares = new List<Sopimuksen_oikeusluokka__c>();  
        Integer a = 0;
        Integer b = 0;
        for(KAYT_API_SalesforceRequest.inboundAgreement i : paramCases) { 
            Sopimustieto__c agreement = incomingAgreements[a];
            if(i.sopimusalueet != null){
                for(KAYT_API_SalesforceRequest.regions r : i.sopimusalueet){
                    Sopimuksen_territorioryhma__c region = incomingRegions[b];
                    if(r.osuudet != null){
                        Sopimuksen_oikeusluokka__c s = new Sopimuksen_oikeusluokka__c();
                        if(r.osuudet.esitys != null){
                            s = mapShares('perfSharePercentage', r.osuudet.esitys, agreement, region);
                            incomingShares.add(s);
                        }
                        if(r.osuudet.tallennus != null){
                            s = mapShares('mechSharePercentage', r.osuudet.tallennus, agreement, region);
                            incomingShares.add(s);
                        }
                        if(r.osuudet.synkronointi != null){
                            s = mapShares('syncSharePercentage', r.osuudet.synkronointi, agreement, region);
                            incomingShares.add(s);
                        }
                    }
                    b++;
                } 
            }
            a++;
        }
        return incomingShares;
    }

    public static RestResponse generateResponse(List<Sopimustieto__c> incomingAgreements, String statusCode, String statusMessage){
        RestResponse res = RestContext.response; 
        KAYT_API_SalesforceResponse.agreementResponse returnAgreementResponse = new KAYT_API_SalesforceResponse.agreementResponse();
        List<KAYT_API_SalesforceResponse.createdAgreements> agreements = new List<KAYT_API_SalesforceResponse.createdAgreements>();
        for(Sopimustieto__c s : incomingAgreements) {
            KAYT_API_SalesforceResponse.createdAgreements c = new KAYT_API_SalesforceResponse.createdAgreements();
            c.Id = s.Id;
            c.sopimusId = s.Sopimus_ID__c;
            agreements.add(c);
        }
        returnAgreementResponse.statusCode = statusCode;
        returnAgreementResponse.statusMessage = statusMessage;
        returnAgreementResponse.sopimukset = agreements;
        res.addHeader('Content-Type', 'application/json');
        res.responseBody = Blob.valueOf(JSON.serialize(returnAgreementResponse));
        return res;
    }

    public static RestResponse createAgreement(List<KAYT_API_SalesforceRequest.inboundAgreement> paramCases) {
        String statusCode = '200';
        String statusMessage = 'Success';
        System.debug('requestMessage:-incoming  paramCases :- '+ paramCases);   
        List<String> ids_asiakas = getIncomingAccountIds(paramCases);
        Map<String,Account> accountMap = getAccountMap(ids_asiakas);
        List<Sopimustieto__c> incomingAgreements = getAgreements(paramCases, accountMap);
        if(incomingAgreements == null || incomingAgreements.size() == 0) { 
            statusCode = '204';
            statusMessage = 'Empty agreement list';
        }

        List<KAYT_API_SalesforceRequest.inboundAgreement> newAgreements = new List<KAYT_API_SalesforceRequest.inboundAgreement>();
        List<KAYT_API_SalesforceRequest.inboundAgreement> updateAgreements = new List<KAYT_API_SalesforceRequest.inboundAgreement>();
        for(KAYT_API_SalesforceRequest.inboundAgreement i : paramCases){
            if(i.id == '' || i.id == null){
                newAgreements.add(i);
            }else{
                updateAgreements.add(i);
            }
        }

        List<Sopimustieto__c> incomingUpdateAgreements = getAgreements(updateAgreements, accountMap);
        List<Sopimustieto__c> incomingNewAgreements = getAgreements(newAgreements, accountMap);
        List<Sopimuksen_territorioryhma__c> incomingRegions = getRegions(newAgreements, incomingNewAgreements);
        List<Sopimuksen_territorio__c> incomingTerritories = getTerritories(newAgreements, incomingRegions, incomingNewAgreements);
        List<Sopimuksen_oikeusluokka__c> incomingShares = getShares(newAgreements, incomingRegions, incomingNewAgreements);
        
        update incomingUpdateAgreements;
        insert incomingNewAgreements;
        insert incomingRegions;
        insert incomingTerritories;
        insert incomingShares;

        List<Sopimustieto__c> allAgreements = new List<Sopimustieto__c>();
        allAgreements.addAll(incomingNewAgreements);
        allAgreements.addAll(incomingUpdateAgreements);

        allAgreements = [SELECT Id, Name, Sopimusnumero__c, Sopimus_ID__c FROM Sopimustieto__c WHERE Id IN :allAgreements];
        return generateResponse(allAgreements, statusCode, statusMessage);
    }
}
