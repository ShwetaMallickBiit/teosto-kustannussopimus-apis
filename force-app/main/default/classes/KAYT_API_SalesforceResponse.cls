public class KAYT_API_SalesforceResponse { 
    
    public class APISalesforceResponse {
        public SalesforceResponse SalesforceResponse{get;set;}
    }
    public class SalesforceResponse{
       public Asiakkaat Asiakkaat {get;set;}
    }
    public class Asiakkaat{
       public List<asiakas> asiakas {get;set;}  
    }
 
    public class asiakas {
        public String tekijanro {get;set;}
        public String id {get;set;}
        public String nimi {get;set;}
        public String etunimi {get;set;}
        public string email {get;set;}
        public string status {get;set;}
        public string description {get;set;}
        public List<jasenyys> jasenyydet {get;set;}
        public List<ipinumero> ipinumerot {get;set;}
        public List<agreement> sopimukset {get;set;}
    }

    public class jasenyys {
        public String tekijanro {get;set;}
        public String tekijaid {get;set;}
        public String yhdistysnro {get;set;}
        public String yhdistysid {get;set;}
        public String yhdistysnimi {get;set;}
        public String jasenmaksuperitaan {get;set;}
        public String alkupvm {get;set;}
        public String loppupvm {get;set;}
        public String id {get;set;}
    }
 
    public class ipinumero {
        public String name {get;set;}
        public String tekijanro {get;set;}
        public String tekijaid {get;set;}
        public String aliasnro {get;set;}
        public String etunimi {get;set;}
        public String sukunimi {get;set;}
        public String ipinro {get;set;}
        public String ominaisuus {get;set;}
        public String selite {get;set;}
        public String tyyppi {get;set;}
        public String id {get;set;}
    }

    public class agreement {
        public String id {get;set;}
        public String tila {get;set;}
        public String sopimusId {get;set;}
        public Integer sopimusNumero {get;set;}
        public String sopimuksenNimi {get;set;}
        public String tekijanro {get;set;} 
        public Integer ipinro {get;set;}
        public Date alkupvm {get;set;}
        public Date loppupvm {get;set;}
        public String muistiinpanot {get;set;}
        public List<regions> sopimusalueet {get;set;}
    }

    public class regions{
        public String alikustantaja {get;set;}
        public List<String> territoriot {get;set;}
		public List<String> poissuljetutTerritoriot {get;set;}
		public shares osuudet {get;set;}
    }
    
    public class shares {
		public Double esitys {get;set;}
		public Double tallennus {get;set;}
		public Double synkronointi {get;set;}
	}

    public class caseResponse{
        public String statusCode{get;set;}
        public String statusMessage{get;set;}
        public List<createdcases> tapaukset{get;set;}      
    }

    public class createdcases{
        public String tapausnro{get;set;}    
        public String tapausid{get;set;}  
    }

    public class agreementResponse{
        public String statusCode{get;set;}
        public String statusMessage{get;set;}
        public List<createdAgreements> sopimukset{get;set;}      
    }

    public class createdAgreements{
        public String sopimusId{get;set;}    
        public String Id{get;set;}  
    }
}