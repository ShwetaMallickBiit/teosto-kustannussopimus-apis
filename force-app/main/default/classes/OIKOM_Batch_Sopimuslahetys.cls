public class OIKOM_Batch_Sopimuslahetys implements Schedulable, Database.Batchable<sObject> { 
    public Database.QueryLocator start(Database.BatchableContext bc) {
        String soql = 
            'SELECT Id, Asiakas_tekija__r.Kieli__c, Vastuuhenkilo__c, Vastuuhenkilo__r.ContactId_c__c, Sopimuksen_vastuuhenkilo_email__c, ' + 
            '(SELECT Id, ContentDocument.LatestPublishedVersion.VersionData, ContentDocument.LatestPublishedVersion.Title, ContentDocument.LatestPublishedVersion.FileExtension FROM ContentDocumentLinks) ' + 
            'FROM Sopimustieto__c WHERE Sopimusvahvistus_lahetysajossa_Tekija__c = TRUE'; 
        if (Test.isRunningTest()) soql+=' ORDER BY Id DESC LIMIT 1'; return Database.getQueryLocator(soql); 
    } 
    public void execute(Database.BatchableContext bc, List<Sopimustieto__c> records) { 
        for (Sopimustieto__c record : records) { 
            Boolean limitsAllowSending = true; 
            try { 
                Messaging.reserveSingleEmailCapacity(1001);
            } catch(Exception e) { 
                if(!Test.isRunningTest()) limitsAllowSending = false;
            } 
            if(limitsAllowSending) { 
            try { 
                String recordId = record.Id;
                List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.EmailFileAttachment>();
                List<Attachment> attLst = (List<Attachment>) Database.query('SELECT Id,Name,Body,CreatedDate FROM Attachment WHERE parentid = :recordId ORDER BY CreatedDate DESC'); 
                for ( Attachment att : attLst )
                { 
                    Messaging.EmailFileAttachment mailAtt = new Messaging.EmailFileAttachment(); 
                    mailAtt.setFileName(att.name); 
                    mailAtt.setBody(att.body); 
                    fileAttachments.add(mailAtt);
                }       
                if(record.ContentDocumentLinks != null) for(ContentDocumentLink cd : record.ContentDocumentLinks)
                { 
                    Messaging.EmailFileAttachment mailAtt = new Messaging.EmailFileAttachment(); 
                    mailAtt.setFileName(cd.ContentDocument.LatestPublishedVersion.Title+'.'+cd.ContentDocument.LatestPublishedVersion.FileExtension); 
                    mailAtt.setBody(cd.ContentDocument.LatestPublishedVersion.VersionData); 
                    fileAttachments.add(mailAtt);
                }       
                Messaging.SingleEmailMessage maili = new Messaging.SingleEmailMessage(); 
                String templateName = 'OIKOM_Sopimusvahvistus_saatekirje_FIN'; 
                if(record.Asiakas_tekija__r.Kieli__c== 'Englanti') templateName = 'OIKOM_Sopimusvahvistus_saatekirje_ENG'; 
                else if(record.Asiakas_tekija__r.Kieli__c== 'Ruotsi') templateName = 'OIKOM_Sopimusvahvistus_saatekirje_SWE'; 
                List<EmailTemplate> et = [select id, name from EmailTemplate where developername = :templateName];
                if( !et.isEmpty() )
                { 
                    Id templateId = et[0].id; 
                    maili.setTemplateId(templateId); 
                    maili.setTargetObjectId(record.Vastuuhenkilo__r.ContactId_c__c);
                    maili.setTreatTargetObjectAsRecipient(False);
                    maili.setToAddresses(New List<String>{record.Sopimuksen_vastuuhenkilo_email__c});
                    maili.setWhatId(record.Id);
                    maili.setFileAttachments(fileAttachments); 
                } 
                //RETRIEVE EMAIL ADDRESS FROM Arvolista__c
                List<Arvolista__c> arvolistaRecords = [SELECT Arvo_1_Teksti__c FROM Arvolista__c WHERE Ryhma__c = 'AUTOMATIC_EMAIL_SENDER_IPR_OWNER' LIMIT 1];
                if( !arvolistaRecords.isEmpty() )
                    maili.setOrgWideEmailAddressId(arvolistaRecords[0].Arvo_1_Teksti__c); 
                system.debug('Arvolista'+arvolistaRecords[0].Arvo_1_Teksti__c);
                
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {maili}); 
                record.Sopimusvahvistus_lahetyspaiva__c=Date.today();
                record.Tila__c = 'Sopimus lähetetty';
                update record;
            }catch(Exception e) { 
                System.debug(e.getMessage()); 
                Loki__c errorLog  = new Loki__c(Objekti__c = 'Sopimustieto__c', Toiminnallisuus__c = 'OIKOM_Batch_Sopimuslahetys', Tyyppi__c = 'Virhe', Virheviesti__c =e.getMessage());
                insert errorLog;
            } 
            } 
        } 
    } 
    public void finish(Database.BatchableContext bc) {
    
    } 
    public void execute(SchedulableContext sc){
        Database.executeBatch(this,1);
    } 
}