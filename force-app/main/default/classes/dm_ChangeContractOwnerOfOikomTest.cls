@isTest
private class dm_ChangeContractOwnerOfOikomTest {

    static testMethod void OIKOM_Batch_SopimuslahetysTest() 
    {
        Account a = GEN_TestFactory.createAccountTekija();
        Contact con = GEN_TestFactory.createContact(a.Id, 'Test', 'Testinen');
        IPI_numero__c ipi = GEN_TestFactory.createIpiNumero(a.Id, TRUE);
        Id oweaId = '0D29E00000000AV';
        String yhteyshenkiloId = [SELECT Id FROM AccountContactRelation_c__c WHERE AccountId_c__c = :a.Id AND ContactId_c__c = :con.Id LIMIT 1][0].Id;
        Sopimustieto__c st = GEN_TestFactory.createSopimusTekija2(a.Id, yhteyshenkiloId, FALSE);
        st.Tila__c = 'Valmis lähetettäväksi';
        st.Yhteyshenkilo__c = con.Id;
        insert st;
        Test.startTest();
            dm_ChangeContractOwnerOfOikom batch= new dm_ChangeContractOwnerOfOikom();
            ID batchprocessid = Database.executeBatch(batch);
        Test.stopTest();
    }
}