public class KAYT_VahvistusPDF_hintalaskenta {

    private final ApexPages.StandardController stdController;
    private final Sopimustieto__c contract;
    private Id asiakasId;
    private string kieli;
    private string recordTypeName;
    private string recordTypeId;
    private string VFPageName;

    public KAYT_VahvistusPDF_hintalaskenta (ApexPages.StandardController stc) {

        this.stdController = stc;
        this.contract = (Sopimustieto__c) stc.getRecord();
        String VFPageName = apexpages.currentpage().getparameters().get('pageName');

    }
    public pageReference fetchPricerows() {

        try{
            // query customer language and record type information
            List<Sopimustieto__c> sopLst = [SELECT Id, Asiakas_tekija__c, RecordTypeId FROM Sopimustieto__c WHERE Id =:contract.Id];  
            asiakasId = sopLst[0].Asiakas_tekija__c;
            kieli = [SELECT Kieli__c FROM Account WHERE Id = :asiakasId].Kieli__c; 

            VFPageName = 'KAYT_SopimusvahvistusPDF';
            VFPageName = apexpages.currentpage().getparameters().get('pageName');

            List<Tuoteosto__c> tuoteostot = [SELECT Id, Tuote__r.Id, Tuotetunnus__c, x_Hintahakupvm__c 
                                             FROM Tuoteosto__c 
                                             WHERE Sopimustieto__c = :contract.Id AND Tuote__r.Hintalaskuri_aktiivinen__c = TRUE];
            // Invoice price generation class
            KAYT_PDFgenerointi.updatePrices(contract.Id, kieli, tuoteostot);

            // Redirect to visualforce page (pdf)
            PageReference vahvistusPage = new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/apex/' + VFPageName + '?Id=' + contract.Id);
            vahvistusPage.setRedirect(true);
            return vahvistusPage;
         }
        catch(Exception e) {
            throw e;
        }
    }
}