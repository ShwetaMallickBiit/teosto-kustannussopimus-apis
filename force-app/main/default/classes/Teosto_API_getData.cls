public with sharing class Teosto_API_getData {

    public static Set<String> getJasenyysCustomerNumbers(KAYT_API_SalesforceRequest requestMessage) {
        Set<String> jasenParams = new Set<String>();
        if(requestMessage.SalesforceRequest.getJasenyys!=null) {
            for(String jasen: requestMessage.SalesforceRequest.getJasenyys.tekijanro){
                jasenParams.add(jasen);
            }
        }
        return jasenParams;
    }
    
    public static Set<String> getIPICustomerNumbers(KAYT_API_SalesforceRequest requestMessage) {
        Set<String> ipiParams = new Set<String>();
        if(requestMessage.SalesforceRequest.getIPINumero!=null) {
             for(String ipi: requestMessage.SalesforceRequest.getIPINumero.tekijanro){
                ipiParams.add(ipi);
            }
        }
        return ipiParams;
    }

    public static Set<String> getAgreementCustomerNumbers(KAYT_API_SalesforceRequest requestMessage) {
        Set<String> agreementParams = new Set<String>();
        if(requestMessage.SalesforceRequest.getSopimukset!=null) {
            for(String agreement: requestMessage.SalesforceRequest.getSopimukset.tekijanro){
                agreementParams.add(agreement);
            }
        }
        return agreementParams;
    }

    public static List<Jasenyys__c> getJasenyysRecords(Set<String> jasenParams) {
        List<Jasenyys__c> jasenyys = New List<Jasenyys__c>();
        if(!jasenParams.isEmpty()) {
            jasenyys = 
                [
                    SELECT x_tekijanro__c, x_Yhdistysnumero__c,Yhdistys__c,
                           Yhdistys__r.Name, Jasenmaksu_peritaan__c,Alkupvm__c,Loppupvm__c,Asiakas__r.Asiakasnumero_Tepa__c
                    FROM Jasenyys__c where Asiakas__r.Asiakasnumero_Tepa__c in:jasenParams
                ];
            
              Set<String> foundIds = new Set<String>();
                for(Jasenyys__c j : jasenyys) {
                    foundIds.add(j.Asiakas__r.Asiakasnumero_Tepa__c);
                }
            system.debug('this is jasenparems; ' + jasenParams);
                for (String s : jasenParams) {
                    if (foundIds.contains(s)) {
                       system.debug('these ids are valid ' + foundIds);
                    } else {
                    }
                }
        }
        return jasenyys;
    }

    public static List<IPI_Numero__c> getIPIRecords(Set<String> IPIParams) {
        List<IPI_numero__c> ipi = New List<IPI_Numero__c>();
        if(!ipiParams.isEmpty()) {
            ipi = 
                [
                    SELECT Id, Name, Asiakas__r.Asiakasnumero_Tepa__c, Asiakas__c, x_tekijanro__c, Etunimi__c,
                           Sukunimi__c, IPI_numero_tekstina__c, Ominaisuus__c,x_Aliasnumero_Tepa__c,
                           Selite__c, Tyyppi__c FROM IPI_numero__c
                     WHERE Asiakas__r.Asiakasnumero_Tepa__c in :ipiParams
                ];
            
            Set<String> foundIds = new Set<String>();
            for(IPI_numero__c i : ipi) {
                foundIds.add(i.Asiakas__r.Asiakasnumero_Tepa__c);
            }
            for (String s : ipiParams) {
                if (foundIds.contains(s)) {
                   system.debug('these ids are valid ' + foundIds);
                } else {
                }
            }
        }
        return ipi;
    }

    public static List<Sopimustieto__c> getAgreementRecords(Set<String> agreementParams) {
        List<Sopimustieto__c> agreement = New List<Sopimustieto__c>();
        Id recordTypeId = 
        Schema.SObjectType.Sopimustieto__c.getRecordTypeInfosByName().get('Kustannussopimus').getRecordTypeId();

        if(!agreementParams.isEmpty()) {
            agreement = 
                [
                    SELECT Id, Name, Sopimus_ID__c, Asiakas_tekija__r.Asiakasnumero_Tepa__c, Asiakas_tekija__c, 
                           Sopimuksen_nimi__c, Tila__c, AsiakasNumero__c, IPI_Numero__c, Alkupvm__c, 
                           Loppupvm__c, Muistiinpanot__c, Tyyppi__c, Sopimusnumero__c, 
                           Asiakasnumero_Tepa__c, IPINumeroFormula__c 
                    FROM Sopimustieto__c
                    WHERE RecordTypeId =: recordTypeId
                ];
            
            Set<String> foundIds = new Set<String>();
            for(Sopimustieto__c s : agreement) {
                foundIds.add(s.Asiakas_tekija__r.Asiakasnumero_Tepa__c);
            }
            for (String s : agreementParams) {
                if (foundIds.contains(s)) {
                   system.debug('these ids are valid ' + foundIds);
                } else {
                }
            }
       }
        return agreement;
    }

    public static List<Account> getAsiakkaat(Set<String> allParams) {
        return [
                SELECT Asiakasnumero_Tepa__c, Id, Name, Email__c, Etunimi__c, RecordType.DeveloperName
                FROM Account 
                WHERE Asiakasnumero_Tepa__c in :allParams
               ];
    }
    
    public static String getEmail(Account acc) {
        List<Account> accs = New List<Account>();
        accs.add(acc);
        Map<Id, AccountContactRelation> accrs = TeostoUtilities.getAccountPrimaryEmail(accs, 'OIKOM');
        system.debug('HERE: ' + accrs + ' | ' + accrs.values());
        if(accrs.values().isEmpty() || accrs.values()[0] == null) return '';
        return accrs.values()[0].x_Sahkoposti__c;
    }
    
    public static KAYT_API_SalesforceResponse.asiakas setAsiakasResponse(Account asia, List<AccountContactRelation> acrs) {
        KAYT_API_SalesforceResponse.asiakas a = new KAYT_API_SalesforceResponse.asiakas();
        a.tekijanro = asia.Asiakasnumero_Tepa__c;
        a.id = asia.Id;
        a.nimi = asia.Name;
        a.etunimi = asia.Etunimi__c;
        a.email = getEmail(asia);
        a.status = 'SUCCESS';
        return a;
    }

    public static List<KAYT_API_SalesforceResponse.jasenyys> setJasenyys(Account asia, List<Jasenyys__c> jasenyys) {
        List<KAYT_API_SalesforceResponse.jasenyys> jasenet = new List<KAYT_API_SalesforceResponse.jasenyys>();
        if(jasenyys!=null) {
            for (Jasenyys__c jasen : jasenyys) {
                if(jasen.asiakas__r.Asiakasnumero_Tepa__c==asia.Asiakasnumero_Tepa__c){
                    KAYT_API_SalesforceResponse.jasenyys j  = new KAYT_API_SalesforceResponse.jasenyys();
                    j.tekijanro = jasen.x_tekijanro__c;
                    j.tekijaid = jasen.Asiakas__r.Id;
                    j.yhdistysnro = jasen.x_Yhdistysnumero__c;
                    j.yhdistysid = jasen.Yhdistys__c;
                    j.id = jasen.Id;
                    j.yhdistysnimi = jasen.Yhdistys__r.Name;
                    j.jasenmaksuperitaan = String.valueOf(jasen.Jasenmaksu_peritaan__c);
                    j.alkupvm = String.valueOf(jasen.Alkupvm__c);
                    j.loppupvm = String.valueOf(jasen.Loppupvm__c);
                    jasenet.add(j);    
                }
            }
        }
        return jasenet;
    }

    public static List<KAYT_API_SalesforceResponse.ipinumero> setIPI(Account asia, List<IPI_Numero__c> ipi) {
        List<KAYT_API_SalesforceResponse.ipinumero> ipinumerot = new List<KAYT_API_SalesforceResponse.ipinumero>();
        if(ipi!=null) {
            for (IPI_numero__c ipinum : ipi) {
            if(ipinum.asiakas__r.Asiakasnumero_Tepa__c==asia.Asiakasnumero_Tepa__c){
                    KAYT_API_SalesforceResponse.ipinumero i  = new KAYT_API_SalesforceResponse.ipinumero();
                    i.name = ipinum.Name;
                    i.tekijanro = ipinum.Asiakas__r.Asiakasnumero_Tepa__c;
                    i.tekijaid = ipinum.Asiakas__c;
                    i.aliasnro = String.valueOf(ipinum.x_Aliasnumero_Tepa__c);
                    i.etunimi = ipinum.Etunimi__c;
                    i.sukunimi = ipinum.Sukunimi__c;
                    if(!string.isBlank(ipinum.IPI_numero_tekstina__c))
                        i.ipinro = ipinum.IPI_numero_tekstina__c.replace(' ', '');
                    else 
                        i.ipinro = ipinum.IPI_numero_tekstina__c;

                    i.ominaisuus = ipinum.Ominaisuus__c;
                    i.selite = ipinum.Selite__c;
                    i.tyyppi = ipinum.Tyyppi__c;
                    i.id = ipinum.Id; 
                    ipinumerot.add(i);     
                }
            }
        }
        return ipinumerot;
    }

    public static List<KAYT_API_SalesforceResponse.agreement> setSopimus(Account asia, List<Sopimustieto__c> agreement) {
        List<KAYT_API_SalesforceResponse.agreement> agreements = new List<KAYT_API_SalesforceResponse.agreement>();
        system.debug('setSopimus, List: ' + agreement);
        if(agreement!=null) {
            for (Sopimustieto__c s : agreement) {
                if(s.Asiakas_tekija__r.Asiakasnumero_Tepa__c == asia.Asiakasnumero_Tepa__c){
                    KAYT_API_SalesforceResponse.agreement a  = new KAYT_API_SalesforceResponse.agreement();
                    a.tila = s.Tila__c;
                    if(s.Sopimusnumero__c != null){
                        a.sopimusNumero = Integer.valueOf(s.Sopimusnumero__c);
                    }
                    a.id = s.Id;
                    if(s.Sopimus_ID__c != null){
                        a.sopimusId = String.valueOf(s.Sopimus_ID__c);
                    }
                    a.sopimuksenNimi = s.Sopimuksen_nimi__c;
                    if(s.AsiakasNumero_Tepa__c != null){
                        a.tekijanro = String.valueOf(s.AsiakasNumero_Tepa__c);
                    }
                    if(s.IPINumeroFormula__c != null){
                        a.ipinro = Integer.valueOf(s.IPINumeroFormula__c);
                    }
                    a.alkupvm = s.Alkupvm__c;
                    a.loppupvm = s.Loppupvm__c;
                    a.muistiinpanot = s.Muistiinpanot__c;
                    a.sopimusalueet = setRegions(getRegions(s));
                    agreements.add(a);    
                }
            }
        }
        system.debug('setSopimus, JSON: ' + agreement);
        return agreements;
    }

    public static List<Sopimuksen_territorioryhma__c> getRegions(Sopimustieto__c a){
        List<Sopimuksen_territorioryhma__c> regions = new List<Sopimuksen_territorioryhma__c>();
            regions = 
                [
                    SELECT Id, Name, Alikustantaja__c,
                    (SELECT Id, Name, TerritorioKoodi__c, Laajuudessa__c FROM Sopimuksen_territoriot_new__r),
                    (SELECT Id, Name, Oikeusluokka_nimi__c, Tyyppi__c, Osuus__c FROM Sopimuksen_oikeusluokat_new__r) 
                    FROM Sopimuksen_territorioryhma__c
                    WHERE Sopimustieto__c =:a.Id
                ];
        system.debug(regions);
        return regions;
    }
    
    public static List<KAYT_API_SalesforceResponse.regions> setRegions(List<Sopimuksen_territorioryhma__c> regions){
        List<KAYT_API_SalesforceResponse.regions> regionGroup = new List<KAYT_API_SalesforceResponse.regions>();
        if(regionGroup!=null) {
            for (Sopimuksen_territorioryhma__c t : regions) {
                KAYT_API_SalesforceResponse.regions r  = new KAYT_API_SalesforceResponse.regions();
                r.alikustantaja = t.Alikustantaja__c;
                List<String> includedTerritories = new List<String>();
                List<String> excludedTerritories = new List<String>();
                for(Sopimuksen_territorio__c c : t.Sopimuksen_territoriot_new__r){
                    if(c.Laajuudessa__c){
                        includedTerritories.add(c.TerritorioKoodi__c);
                    }else{
                        excludedTerritories.add(c.TerritorioKoodi__c);
                    }
                }
                
                String serializedTerritories = JSON.serializePretty(includedTerritories);
                List<String> territories = new List<String> {serializedTerritories};
                r.territoriot = territories;

                serializedTerritories = JSON.serializePretty(excludedTerritories);
                territories = new List<String> {serializedTerritories};
                r.poissuljetutTerritoriot = territories;

                KAYT_API_SalesforceResponse.shares s  = new KAYT_API_SalesforceResponse.shares();
                for(Sopimuksen_oikeusluokka__c l : t.Sopimuksen_oikeusluokat_new__r){
                    system.debug(l.Osuus__c);
                    if(l.Tyyppi__c == 'perfSharePercentage' && l.Osuus__c != null){
                        s.esitys = l.Osuus__c;
                    }
                    if(l.Tyyppi__c == 'mechSharePercentage' && l.Osuus__c != null){
                        s.tallennus = l.Osuus__c;
                    }
                    if(l.Tyyppi__c == 'syncSharePercentage' && l.Osuus__c != null){
                        s.synkronointi = l.Osuus__c;
                    }
                }  
                r.osuudet = s;
                regionGroup.add(r);
            }
        }
        system.debug(regionGroup);
        return regionGroup;
    }

    public static List<AccountContactRelation> getAccountContactRelations(List<Account> accs) {
        Set<Id> ids = New Set<Id>();
        for(Account acc : accs) {
            ids.add(acc.Id);
        }
        return [SELECT Id, AccountId, Roles, x_sahkoposti__c, x_Aktiivinen__c FROM AccountContactRelation WHERE AccountId IN :ids];
    }

    public static KAYT_API_SalesforceResponse.SalesforceResponse getResponse (List<Account> asiakas, List<Jasenyys__c> jasenyys, List<IPI_numero__c> ipi,  Set<String> invalidParams, List<Sopimustieto__c> agreeList) {
    
        List<KAYT_API_SalesforceResponse.asiakas> asiakkaat = new List<KAYT_API_SalesforceResponse.asiakas>();
        List<KAYT_API_SalesforceResponse.jasenyys> jasenet = new List<KAYT_API_SalesforceResponse.jasenyys>();
        List<KAYT_API_SalesforceResponse.ipinumero> ipinumerot = new List<KAYT_API_SalesforceResponse.ipinumero>();
        List<KAYT_API_SalesforceResponse.agreement> agreements = new List<KAYT_API_SalesforceResponse.agreement>();
        List<AccountContactRelation> acrs = getAccountContactRelations(asiakas);

        KAYT_API_SalesforceResponse.Asiakkaat returnAsiakkaat = new KAYT_API_SalesforceResponse.Asiakkaat();

    
        for(Account asia : asiakas) {
            KAYT_API_SalesforceResponse.asiakas a = setAsiakasResponse(asia, acrs);
            jasenet = setJasenyys(asia, jasenyys);
            a.jasenyydet = jasenet;
            ipinumerot = setIPI(asia, ipi);
            a.ipinumerot = ipinumerot;
            agreements = setSopimus(asia, agreeList);
            a.sopimukset = agreements;
            asiakkaat.add(a);
        }
        KAYT_API_SalesforceResponse.SalesforceResponse responseBody = new KAYT_API_SalesforceResponse.SalesforceResponse();
        returnAsiakkaat.asiakas = asiakkaat;
        responseBody.Asiakkaat = returnAsiakkaat;
        
        system.debug('responseBody: ' + responseBody);

        return responseBody;
    }
    
    public static RestResponse getData(KAYT_API_SalesforceRequest requestMessage) {

        Set<String> allParams = new Set<String>();
        Set<String> invalidParams = new Set<String>();
        RestResponse res = RestContext.response;       

        /* get customer numbers from the message */    
        Set<String> jasenParams = getJasenyysCustomerNumbers(requestMessage);
        allParams.addAll(jasenParams);
        Set<String> ipiParams = getIPICustomerNumbers(requestMessage);
        allParams.addAll(ipiParams);
        Set<String> agreeParams = getAgreementCustomerNumbers(requestMessage);
        allParams.addAll(agreeParams);

        /* get Salesforce records based on message customer numbers */
        List<Jasenyys__c> jasenyys = getJasenyysRecords(jasenParams);
        List<IPI_numero__c> ipi = getIPIRecords(ipiParams);
        List<Sopimustieto__c> agreement = getAgreementRecords(agreeParams);
        List<Account> asiakas = getAsiakkaat(allParams); 

        /* create response message based on Salesforce records */
        KAYT_API_SalesforceResponse.APISalesforceResponse response = new KAYT_API_SalesforceResponse.APISalesforceResponse();

        response.SalesforceResponse = getResponse(asiakas, jasenyys, ipi,invalidParams, agreement);
        system.debug('response: '+response);
        String jasenyysResponseToJSON = JSON.serialize(response);
        system.debug('Response data in json ' + jasenyysResponseToJSON);

        res.addHeader('Content-Type', 'application/json');
        res.responseBody = Blob.valueOf(JSON.serialize(response));
        res.statusCode = 200;
            
        return res;
    }

public class cException extends exception {}

}