public with sharing class CustomMetadataHandler {

    public static String getItsehallinnoinninTilaLabel(String key) {
        try {
            ItsehallinnoinninTila__mdt iht = [SELECT value__c FROM ItsehallinnoinninTila__mdt WHERE key__c=:key LIMIT 1];
            return iht.value__c;
        } catch (Exception e) {
            System.Debug('Failed to get Itsehallinnoinnin tila label by key ' + key);
            return '';
        }
    }

    public static String getItsehallinnoinninTilaKey(String label) {
        try {
            ItsehallinnoinninTila__mdt iht = [SELECT key__c FROM ItsehallinnoinninTila__mdt WHERE value__c=:label LIMIT 1];
            return iht.key__c;
        } catch (Exception e) {
            System.Debug('Failed to getItsehallinnoinnin tila key by label ' + label);
            return '';
        }
    }

    public static String getItsehallinnoinninTyyppiLabel(String key) {
        try {
            Itsehallinnoinnin_Tyyppi__mdt iht = [SELECT value__c FROM Itsehallinnoinnin_Tyyppi__mdt WHERE key__c=:key LIMIT 1];
            return iht.value__c;
        } catch (Exception e) {
            System.Debug('Failed to get Itsehallinnoinnin tyyppi label by key ' + key);
            return '';
        }
    }

    public static String getItsehallinnoinninTyyppiKey(String label) {
        try {
            Itsehallinnoinnin_Tyyppi__mdt iht = [SELECT key__c FROM Itsehallinnoinnin_Tyyppi__mdt WHERE value__c=:label LIMIT 1];
            return iht.key__c;
        } catch (Exception e) {
            System.Debug('Failed to getItsehallinnoinnin tyyppi key by label ' + label);
            return '';
        }
    }

    public static String getItsehallinnoinninTeoksenTilaLabel(String key) {
        try {
            ItsehallinnoinninTeoksenTila__mdt iht = [SELECT value__c FROM ItsehallinnoinninTeoksenTila__mdt WHERE key__c=:key LIMIT 1];
            return iht.value__c;
        } catch (Exception e) {
            System.Debug('Failed to get Itsehallinnoinnin teoksen tila label by key ' + key);
            return '';
        }
    }

    public static String getItsehallinnoinninTeoksenTilaKey(String label) {
        try {
            ItsehallinnoinninTeoksenTila__mdt iht = [SELECT key__c FROM ItsehallinnoinninTeoksenTila__mdt WHERE value__c=:label LIMIT 1];
            return iht.key__c;
        } catch (Exception e) {
            System.Debug('Failed to getItsehallinnoinnin teoksen tila key by label ' + label);
            return '';
        }
    }

    public static String getItsehallinnoinninTekijanRooliLabel(String key) {
        try {
            Itsehallinnoinnin_Teoksen_Tekij_n__mdt iht = [SELECT value__c FROM Itsehallinnoinnin_Teoksen_Tekij_n__mdt WHERE key__c=:key LIMIT 1];
            return iht.value__c;
        } catch (Exception e) {
            System.Debug('Failed to get Itsehallinnoinnin tekijän rooli label by key ' + key);
            return '';
        }
    }

    public static String getItsehallinnoinninTekijanRooliKey(String label) {
        try {
            Itsehallinnoinnin_Teoksen_Tekij_n__mdt iht = [SELECT key__c FROM Itsehallinnoinnin_Teoksen_Tekij_n__mdt WHERE value__c=:label LIMIT 1];
            return iht.key__c;
        } catch (Exception e) {
            System.Debug('Failed to getItsehallinnoinnin tekijän rooli key by label ' + label);
            return '';
        }
    }

    public static String getItsehallinnoinninTekijanPeruutusTilaLabel(String key) {
        try {
            Itsehallinnoinnin_Tekijan_Peruutustila__mdt iht = [SELECT value__c FROM Itsehallinnoinnin_Tekijan_Peruutustila__mdt WHERE key__c=:key LIMIT 1];
            return iht.value__c;
        } catch (Exception e) {
            System.Debug('Failed to get Itsehallinnoinnin tekijän peruutustila label by key ' + key);
            return '';
        }
    }

    public static String getItsehallinnoinninTekijanPeruutusTilaKey(String label) {
        try {
            Itsehallinnoinnin_Tekijan_Peruutustila__mdt iht = [SELECT key__c FROM Itsehallinnoinnin_Tekijan_Peruutustila__mdt WHERE value__c=:label LIMIT 1];
            return iht.key__c;
        } catch (Exception e) {
            System.Debug('Failed to getItsehallinnoinnin tekijän peruutustila key by label ' + label);
            return '';
        }
    }

    public static String getItsehallinnoinninTyonPeruutusTilaLabel(String key) {
        try {
            Itsehallinnoinnin_Tyon_Peruutustila__mdt iht = [SELECT value__c FROM Itsehallinnoinnin_Tyon_Peruutustila__mdt WHERE key__c=:key LIMIT 1];
            return iht.value__c;
        } catch (Exception e) {
            System.Debug('Failed to get Itsehallinnoinnin työn peruutustila label by key ' + key);
            return '';
        }
    }

    public static String getItsehallinnoinninTyonPeruutusTilaKey(String label) {
        try {
            Itsehallinnoinnin_Tyon_Peruutustila__mdt iht = [SELECT key__c FROM Itsehallinnoinnin_Tyon_Peruutustila__mdt WHERE value__c=:label LIMIT 1];
            return iht.key__c;
        } catch (Exception e) {
            System.Debug('Failed to getItsehallinnoinnin työn peruutustila key by label ' + label);
            return '';
        }
    }

    public static String getHyvaksynnanTilaLabel(String key) {
        try {
            Hyvaksynnan_Tila__mdt iht = [SELECT value__c FROM Hyvaksynnan_Tila__mdt WHERE key__c=:key LIMIT 1];
            return iht.value__c;
        } catch (Exception e) {
            System.Debug('Failed to get hyväksynnän tila label by key ' + key);
            return '';
        }
    }

    public static String getHyvaksynnanTilaKey(String label) {
        try {
            Hyvaksynnan_Tila__mdt iht = [SELECT key__c FROM Hyvaksynnan_Tila__mdt WHERE value__c=:label LIMIT 1];
            return iht.key__c;
        } catch (Exception e) {
            System.Debug('Failed to get hyväksynnän tila key by label ' + label);
            return '';
        }
    }


    public static String getItsehallinnoinninPeruutuksenTilaLabel(String key) {
        try {
            Itsehallinnoinnin_Peruutuksen_Tila__mdt iht = [SELECT value__c FROM Itsehallinnoinnin_Peruutuksen_Tila__mdt WHERE key__c=:key LIMIT 1];
            return iht.value__c;
        } catch (Exception e) {
            System.Debug('Failed to get Itsehallinnoinnin Peruutuksen tila label by key ' + key);
            return '';
        }
    }

    public static String getItsehallinnoinninPeruutuksenTilaKey(String label) {
        try {
            Itsehallinnoinnin_Peruutuksen_Tila__mdt iht = [SELECT key__c FROM Itsehallinnoinnin_Peruutuksen_Tila__mdt WHERE value__c=:label LIMIT 1];
            return iht.key__c;
        } catch (Exception e) {
            System.Debug('Failed to getItsehallinnoinnin Peruutuksen tila key by label ' + label);
            return '';
        }
    }



}