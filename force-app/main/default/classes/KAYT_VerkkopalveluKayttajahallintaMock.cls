/*
Description
            This class is integration mock class for test class KAYT_VerkkopalveluKayttajahallintaTest
 */
 
 @isTest
public class KAYT_VerkkopalveluKayttajahallintaMock implements HttpCalloutMock{
    
 public HttpResponse respond(HTTPRequest req)
    {
        HttpResponse res = new HttpResponse();
        res.setStatusCode(200);        
        res.setHeader('Content-Type', 'application/json');
        if(req.getBody().contains('statusfailed'))    
        {
            res.setStatusCode(400); 
        }
        else if(req.getBody().contains('webCustomerId'))
        {
            res.setStatusCode(201);        
            res.setBody('{"webCustomerId":{"customerType":"AS","customerNumber":1122334455},"status":"active","name":"Mikko Muukka","ISO639code":"fi",'+
            '"dataPrivileges":[{"webCustomerId":{"customerType":"AS","customerNumber":11223344},"privilegeLevel":"UPD","privilegeClass":"TAPAHTUMA"},'+
            '{"webCustomerId":{"customerType":"MA","customerNumber":100010176},"privilegeLevel":"UPD","privilegeClass":"MAOIKEUS"}],'+
            '"credentials":{"username":"mikko.muukka","password":"jdhsdfbkwc"},"applicationPrivileges":[{"privilegeClass":"TAPAHTUMA"}]}');
/*
            res.setBody('{"webCustomerId":{"customerType":"AS","customerNumber":"1122334455"},"status":"active","name":"Mikko Muukka","ISO639code":"fi",'+
            '"dataPrivileges":[{"webCustomerId":{"customerType":"AS","customerNumber":"11223344"},"privilegeLevel":"UPD","privilegeClass":"TAPAHTUMA"},'+
            '{"webCustomerId":{"customerType":"MA","customerNumber":"100010176"},"privilegeLevel":"UPD","privilegeClass":"MAOIKEUS"}],'+
            '"credentials":{"username":"mikko.muukka","password":"jdhsdfbkwc"},"applicationPrivileges":[{"privilegeClass":"TAPAHTUMA"}]}');
*/
        }
        else
        {
            res.setStatusCode(204);
        }
                             
        return res;
    }   
    
}