public class KAYT_API_SalesforceRequest {
    public SalesforceRequest SalesforceRequest;
    
    public class SalesforceRequest {
        public getJasenyys getJasenyys {get;set;}
        public getIPINumero getIPINumero {get;set;}
        public getSopimukset getSopimukset {get;set;}
        public Cases setTapaus {get;set;}
        public Agreements setSopimus {get;set;}                
    }    
    public class getJasenyys {
        public List<String> tekijanro {get;set;}
    }
    public class getIPINumero {
        public List<String> tekijanro {get;set;}
    }
    public class getSopimukset {
        public List<String> tekijanro {get;set;}
    }
    public class Cases{
        public List <inboundCase> tapaukset{get;set;}
    }
    public class inboundCase {
        public String aihe{get;set;}
        public String kuvaus{get;set;}
        public String tyyppi{get;set;}
        public String alatyyppi1{get;set;}
        public String alatyyppi2{get;set;}
        public String lahde{get;set;} 
        public String tekijanro{get;set;}         
        public String sopimusnro{get;set;}         
    }
    public class Agreements{
        public List<inboundAgreement> sopimukset{get;set;}
    }
    public class inboundAgreement{
        public String id {get;set;}
        public String tila {get;set;}
        public String sopimusId {get;set;}
        public Integer sopimusNumero {get;set;}
        public String sopimuksenNimi {get;set;}
        public String tekijanro {get;set;}
        public Integer ipinro {get;set;}
        public Date alkupvm {get;set;}
        public Date loppupvm {get;set;}
        public String muistiinpanot {get;set;}
        public List<regions> sopimusalueet {get;set;}
    }
    public class regions{
        public String alikustantaja {get;set;}
        public List<String> territoriot {get;set;}
		public List<String> poissuljetutTerritoriot {get;set;}
		public shares osuudet {get;set;}
    }
    public class shares {
		public Double esitys {get;set;}
		public Double tallennus {get;set;}
		public Double synkronointi {get;set;}
	}
}