/**
 */
@isTest
public class OIKOM_VerotiedotCallMock implements HttpCalloutMock{
    
 public HttpResponse respond(HTTPRequest req)
    {
        HttpResponse res = new HttpResponse();
        res.setStatusCode(200);        
        res.setHeader('Content-Type', 'application/json');
        
        res.setBody('[{"abrutto":"1588.76", "avero":"480.5", "lbrutto":"28", "pbrutto":"1888.98", "pvero":"18","alkpvm":"",'+
        '"paatpvm":"","rowid":"666","asnro":"12345","lvero":"21","veroton":"1", "verotieto":"T", "maakoodi":"FI"}]');
                              
        return res;
    }   
    
}