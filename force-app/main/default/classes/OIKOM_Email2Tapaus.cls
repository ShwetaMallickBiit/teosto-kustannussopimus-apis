/*
    Description
                This Apex Class creates Tapaus__c record from inbound email. The class is used in Salesforce Email Services (inbound)
*/


global class OIKOM_Email2Tapaus implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.Inboundenvelope envelope) {

        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        string debugText = '11'; // this is used to debug issue with 'Attempt to de-reference a null object'
        string emailBodyText;
        string emailBodyHtml;
        boolean isNew = false;
        
        if(string.isblank(email.plainTextBody) || email.plainTextBody == null || email.plainTextBody == '') {
            emailBodyText = '';
        }
        else {
            emailBodyText = email.plainTextBody;
        }
        if(string.isblank(email.htmlBody) || email.htmlBody == null || email.htmlBody == '' || email.htmlBody == 'null') {
            emailBodyHtml = '';
        }
        else {
            emailBodyHtml = email.htmlBody;
        }

        try {
            string ticketId;
                   
            // Find existing tapaus records ("tickets") by comparing the subject of incoming email with existing tapaus records
            string emailsubject = email.subject.right(21);
            List<Tapaus__c> ticketLst = (List<Tapaus__c>)Database.query('SELECT Id, Viimeisin_viesti__c, x_Inbound_Id__c, Sulkemispvm__c  FROM Tapaus__c WHERE x_Inbound_Id__c =:emailsubject  LIMIT 1');
            Tapaus__c ticket = new Tapaus__c();

            // If ticket (Tapaus) already exists, new Tapaus record is not created
            // if ticket is already closed creates a new entry with link to closed one.
            if( !ticketLst.isEmpty() && ticketLst[0].Sulkemispvm__c == null  )
            {
                ticket = ticketLst[0];
                ticket.Viimeisin_viesti__c = emailBodyText.left(99999);
                ticket.Viimeisin_viesti_html__c = emailBodyHtml.left(99999);

                /* many 255-long text fields are updated because of Salesforce issues in email sending lightning acction:
                   lightning action cannot parse text correctly to different rows if email body is fetched from long text fields */
                ticket.x_Viimeisin_viesti_1__c = emailBodyHtml.left(255);
                ticket.x_Viimeisin_viesti_2__c = emailBodyHtml.mid(255, 255);
                ticket.x_Viimeisin_viesti_3__c = emailBodyHtml.mid(510, 255);
                ticket.x_Viimeisin_viesti_4__c = emailBodyHtml.mid(765, 255);
                ticket.x_Viimeisin_viesti_5__c = emailBodyHtml.mid(1020, 255);
                ticket.x_Viimeisin_viesti_6__c = emailBodyHtml.mid(1275, 255);
                ticket.x_Viimeisin_viesti_7__c = emailBodyHtml.mid(1530, 255);
                ticket.x_Viimeisin_viesti_8__c = emailBodyHtml.mid(1785, 255);
                ticket.x_Viimeisin_viesti_9__c = emailBodyHtml.mid(2040, 255);
                ticket.x_Viimeisin_viesti_10__c = emailBodyHtml.mid(2295, 255);
                ticket.x_Viimeisin_viesti_11__c = emailBodyHtml.mid(2550, 255);
                ticket.x_Viimeisin_viesti_12__c = emailBodyHtml.mid(2805, 255);
                ticket.x_Viimeisin_viesti_13__c = emailBodyHtml.mid(3060, 255);
                ticket.x_Viimeisin_viesti_14__c = emailBodyHtml.mid(3315, 255);
                ticket.x_Viimeisin_viesti_15__c = emailBodyHtml.mid(3570, 255);
                ticket.x_Viimeisin_viesti_16__c = emailBodyHtml.mid(3825, 255);
                ticket.x_Viimeisin_viesti_17__c = emailBodyHtml.mid(4080, 255);
                ticket.x_Viimeisin_viesti_18__c = emailBodyHtml.mid(4335, 255);
                ticket.x_Viimeisin_viesti_19__c = emailBodyHtml.mid(4590, 255);
                ticket.x_Viimeisin_viesti_20__c = emailBodyHtml.mid(4845, 255);
                /* three dots is added if text is cut */
                if(emailBodyHtml.length() > 5099) {
                    ticket.x_Viimeisin_viesti_20__c = emailBodyHtml.mid(4845, 250) + '...';
                }
                ticket.Viestipvm__c = datetime.now();
                ticket.Lukemattomia_viesteja__c = true;
                ticketId = ticketLst[0].Id;
                update ticket;               
            }
            // Otherwise create a new record and in case referenced Tapaus was closed add the link to that.
            else
            {
                ticket.Aihe__c = email.subject.left(255);
                ticket.Kuvaus_html__c = emailBodyHtml.left(99999);
                if(string.isblank(emailBodyHtml) || emailBodyHtml == '') {
                    ticket.kuvaus_html__c = emailBodyText.left(99999);
                } else {
                    ticket.kuvaus_html__c = emailBodyHtml.left(99999);
                }
                ticket.Kuvaus__c = emailBodyText.left(99999);
                ticket.Kuvaus_text__c = emailBodyText.left(99999);
                ticket.Viimeisin_viesti__c = emailBodyText.left(99999);
                ticket.Viimeisin_viesti_html__c = emailBodyHtml.left(99999);

                /* many 255-long text fields are updated because of Salesforce issues in email sending lightning acction:
                   lightning action cannot parse text correctly to different rows if email body is fetched from long text fields */
                ticket.x_Viimeisin_viesti_1__c = emailBodyHtml.left(255);
                ticket.x_Viimeisin_viesti_2__c = emailBodyHtml.mid(255, 255);
                ticket.x_Viimeisin_viesti_3__c = emailBodyHtml.mid(510, 255);
                ticket.x_Viimeisin_viesti_4__c = emailBodyHtml.mid(765, 255);
                ticket.x_Viimeisin_viesti_5__c = emailBodyHtml.mid(1020, 255);
                ticket.x_Viimeisin_viesti_6__c = emailBodyHtml.mid(1275, 255);
                ticket.x_Viimeisin_viesti_7__c = emailBodyHtml.mid(1530, 255);
                ticket.x_Viimeisin_viesti_8__c = emailBodyHtml.mid(1785, 255);
                ticket.x_Viimeisin_viesti_9__c = emailBodyHtml.mid(2040, 255);
                ticket.x_Viimeisin_viesti_10__c = emailBodyHtml.mid(2295, 255);
                ticket.x_Viimeisin_viesti_11__c = emailBodyHtml.mid(2550, 255);
                ticket.x_Viimeisin_viesti_12__c = emailBodyHtml.mid(2805, 255);
                ticket.x_Viimeisin_viesti_13__c = emailBodyHtml.mid(3060, 255);
                ticket.x_Viimeisin_viesti_14__c = emailBodyHtml.mid(3315, 255);
                ticket.x_Viimeisin_viesti_15__c = emailBodyHtml.mid(3570, 255);
                ticket.x_Viimeisin_viesti_16__c = emailBodyHtml.mid(3825, 255);
                ticket.x_Viimeisin_viesti_17__c = emailBodyHtml.mid(4080, 255);
                ticket.x_Viimeisin_viesti_18__c = emailBodyHtml.mid(4335, 255);
                ticket.x_Viimeisin_viesti_19__c = emailBodyHtml.mid(4590, 255);
                ticket.x_Viimeisin_viesti_20__c = emailBodyHtml.mid(4845, 255);
                /* three dots is added if text is cut */
                if(emailBodyHtml.length() > 5099) {
                    ticket.x_Viimeisin_viesti_20__c = emailBodyHtml.mid(4845, 250) + '...';
                }
                
                ticket.Viestipvm__c = datetime.now();
                ticket.Lukemattomia_viesteja__c = true;
                ticket.x_Email_Service_Address__c = envelope.toAddress;
                ticket.Lahettaja__c = email.fromAddress;
                // validate if Tapaus is closed
                if( !ticketLst.isEmpty() && ticketLst[0].Sulkemispvm__c != null  ){
                    ticket.Liittyva_tapaus__c = ticketLst[0].Id;
                }
                ticket.Tila__c = 'Avoin';
                insert ticket;
                ticketId = ticket.Id;
                isNew = true;
            }
            // Create a new Viesti__c record (links to tapaus)
            Viesti__c viesti = new Viesti__c();
            viesti.Aihe__c = email.subject.left(255);
            viesti.Kuvaus__c = emailBodyHtml.left(99999);
            viesti.Lahettaja_text__c = email.fromAddress;

            viesti.Tapaus__c = ticketId;
            string ccAddress = '';
            string toAddress = '';

            if(email.toAddresses != null && !email.toAddresses.isEmpty() && email.toAddresses.size() > 0)
            {
                for( string toaddr : email.toAddresses)
                {
                    toAddress = toAddress != null ? toAddress + ';' + toaddr : toaddr;
                }
                viesti.Vastaanottaja_text__c = toAddress.left(255);
            }

            if( email.ccAddresses != null && !email.ccAddresses.isEmpty() && email.ccAddresses.size() > 0)
            {
                for( string ccaddr : email.ccAddresses)
                {
                    ccAddress = ccAddress != null ? ccAddress + ';' + ccaddr : ccaddr;
                }
                viesti.CC__c = ccAddress.left(255);
            }
            insert viesti;

            // Create new EmailMessage related to tapaus to make inbound email visible in activity history
            EmailMessage emailMsgCreation = new EmailMessage();
            emailMsgCreation.RelatedToId = ticketId;
            emailMsgCreation.FromAddress = email.fromAddress;
            emailMsgCreation.FromName = email.fromName;
//            emailMsgCreation.CcAddress= ccAddress.left(255);
//            emailMsgCreation.ToAddress = toAddress.left(255);
            emailMsgCreation.CcAddress= ccAddress;
            emailMsgCreation.ToAddress = toAddress;
            emailMsgCreation.Subject = email.subject.left(255);
            emailMsgCreation.TextBody = emailBodyText.left(32000);
            emailMsgCreation.HtmlBody = emailBodyHtml.left(32000);
            emailMsgCreation.MessageDate = system.now();
            emailMsgCreation.Incoming = TRUE;
            emailMsgCreation.Status = '1';
            insert emailMsgCreation;

            // update WhoId to activity based on Contact on Tapaus__c record
            string activityId = '';
            string contactId = '';
            List<Tapaus__c> tapList = [SELECT Id, Yhteyshenkilo__c FROM Tapaus__c WHERE Id = :ticketId LIMIT 1];
            List<EmailMessage> emailMsgList = [SELECT Id, ActivityId FROM EmailMessage WHERE Id = :emailMsgCreation.Id LIMIT 1];

            if(!emailMsgList.isEmpty()) activityId = emailMsgList[0].ActivityId;
            if(!tapList.isEmpty()) contactId = tapList[0].Yhteyshenkilo__c;
            Task taskRecord = new Task();
            taskRecord.Id = activityId; system.debug('activityid = ' + emailMsgCreation.ActivityId);
            taskRecord.WhoId = contactId;
            if(!string.isblank(activityId) && activityId != '') update taskRecord;

            // Add attachments from inbound email
            List<Attachment> attList = new List<Attachment>();
            if(email != null && email.binaryAttachments != null && email.binaryAttachments.size() > 0) {
                for( Messaging.InboundEmail.BinaryAttachment file : email.binaryAttachments) 
                {
                    Attachment attachment = new Attachment();
                    attachment.Name = file.fileName;
                    attachment.Body = file.body;
                    attachment.ParentId = viesti.Id;
                    attachment.ParentId = emailMsgCreation.Id;
                    attList.add(attachment);
                }
            }

            if(email != null && email.textAttachments != null && email.textAttachments.size() > 0)   
            {
                for (Messaging.Inboundemail.TextAttachment textFile : email.textAttachments) {
                    Attachment attachment = new Attachment();
                    attachment.Name = textFile.fileName;
                    attachment.Body = Blob.valueOf(textFile.body);
                    attachment.ParentId = viesti.Id;
                    attachment.ParentId = emailMsgCreation.Id;
                    attList.add(attachment);
                }
            }

            if(email != null && attList != null && !attList.isEmpty() && attList.size() > 0) {
                insert attList;
            }

            /* update email message id to tapaus record */
            if(isNew == true) {
                ticket.x_Sahkoposti_Id__c = emailMsgCreation.Id;
                update ticket;
            }

            result.success = true;
                
        } catch (Exception e) {
            result.success = false;
            result.message = e.getStackTraceString();
            System.debug(e.getMessage()); 
            Loki__c errorLog  = new Loki__c();
            errorLog.Objekti__c = 'Tapaus__c, Viesti__c';
            errorLog.Toiminnallisuus__c = 'OIKOM_Email2Tapaus';
            errorLog.Tyyppi__c = 'Virhe';
            errorLog.Virheviesti__c = e.getMessage();   
            string moreInfo =  email.subject + ' / ' + email.fromAddress;
            errorLog.Lisatieto_1_Pitkatekstikentta__c = debugText + '/' + moreInfo + '/' + emailBodyText.left(40000) + '/' + emailBodyHtml.left(40000);
            insert errorLog;
        }
        
        return result;
    }
}