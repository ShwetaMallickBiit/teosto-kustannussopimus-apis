@isTest
public with sharing class Teosto_API_createAgreementsTest {
    @isTest
    public static void Teosto_API_createAgreements_Test() {
        String inString1 ='{ "SalesforceRequest": { "setSopimus": { "sopimukset": [ { "id": "", "sopimusId": null , "tila": "Käsittelyssä", "sopimusNumero" : null, "sopimuksenNimi": "Test Shweta Agreement 3", "tekijanro": "100", "ipinro": null, "alkupvm": "2019-01-07", "loppupvm": null, "muistiinpanot": "Kustantajan omat muistiinpanot sopimukseen test 12345", "sopimusalueet":[ { "alikustantaja": "test1", "territoriot":["2SC", "2EU"], "poissuljetutTerritoriot":["2WL", "FI"], "osuudet": { "esitys": 10, "tallennus": 20, "synkronointi": 30 } }, { "alikustantaja": "test2", "territoriot":["2WL", "FI"], "poissuljetutTerritoriot":["2SC"], "osuudet": { "esitys": 40, "tallennus": 50, "synkronointi": 60 } } ] } ] } } }';
        String accountId;
        RestResponse res = new RestResponse();
        Account acc=new Account();
                    acc.name='Account used for Testing class';
                    acc.Etunimi__c = 'Account';
                    acc.Asiakasryhma__c = 'Tekijä';
                    acc.Ominaisuus__c = 'Säveltäjä';
                    acc.Kieli__c =  'Suomi';
                    acc.Email__c = 'testaccount@test.com';
                    acc.Luokittelu__c = 'Perus';
                    acc.Asiakasnumero_Tepa__c = '100';
                    acc.Maksutapa__c = 'Sepa-maksu';
                    acc.Ohita_Y_tunnus_Hetu_validointi__c = true;
        insert acc;
        accountId = acc.id;
        
        system.debug('this is data on account ' + acc);
        

        Test.startTest();
        res = callSetAgreements(inString1); 
        System.debug(res.responseBody.toString());
        // System.assert(true, (KAYT_API_SalesforceResponse)JSON.deserialize(res.responseBody.toString(),KAYT_API_SalesforceResponse.Class));
        System.debug(res.responseBody.toString());
        Test.stopTest();
    }

    public static RestResponse callSetAgreements(String inString){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/restSalesforceAPIService';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(inString);

        RestContext.request = req;
        RestContext.response= res;
        KAYT_API_AWS_RetrieveMicroService.postProcessor();
        return res;
    }
}
