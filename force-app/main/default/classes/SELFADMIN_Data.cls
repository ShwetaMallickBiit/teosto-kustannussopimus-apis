public with sharing class SELFADMIN_Data {
    public class SelfAdministrationNotification {
        public Integer notificationId;
        public String customerType;
        public Integer customerNumber;
        public String notificationType;
        public String status;
        public String terminationStatus;
        public String title;
        public String startDate;
        public String endDate;
        public String additionalInformation;
        public List<SelfAdministrationWork> workList;
        public List<SelfAdministrationWorkInterestedParty> interestedPartyList;
        public EventInformation eventInformation;
        public AVInformation avProduction;
        public List<EmailNotification> emailNotificationList;
    }

    public class SelfAdministrationWork {
        public Integer workKey;
        public String title;
        //public String status;
        //public String statusTimestamp;
        //public String cancellationStatus;
        //public String endDate; // Format: YYYY-MM-DD
    }

    public class EventInformation {
        public Integer eventNumber;
        public Integer venueNumber;
        public EventContactInfo venue;
        public Integer organizerNumber;
        public EventContactInfo organizer;
        public String eventDate; // Format: YYYY-MM-DD
    }

    public class AVInformation {
        public Integer year;
        public String productionType;
        public String spotgateId;
        public String channel;
    }

    public class EmailNotification {
        public String email;
        public Integer order;
    }

    public class SelfAdministrationWorkInterestedParty {
        public Long ipiNameNumber;
        public Integer customerNumber;
        public String name;
        public String firstName;
        public String acceptanceRole;
        public String acceptanceStatus;
        public String emailTimestamp;
        public String cancellationStatus;
    }

    public class EventContactInfo {
        public String name;
        public String address;
        public String postNumber;
        public String postalAddress;
        public String contactPerson;
        public String phone;
        public String email;
    }

    public class NotificationStatus {
        public String status;
    }

    public class NotificationEmailSend {
        public List<Long> authors; // Array containing ipiNameNumbers of the authors
    }

}