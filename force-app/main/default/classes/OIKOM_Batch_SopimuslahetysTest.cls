@isTest
private class OIKOM_Batch_SopimuslahetysTest {

    static testMethod void OIKOM_Batch_SopimuslahetysTest() 
    {
        Account a = GEN_TestFactory.createAccountTekija();
        Contact con = GEN_TestFactory.createContact(a.Id, 'Test', 'Testinen');
        IPI_numero__c ipi = GEN_TestFactory.createIpiNumero(a.Id, TRUE);
        Id oweaId = '0D29E00000000AV';
        String yhteyshenkiloId = [SELECT Id FROM AccountContactRelation_c__c WHERE AccountId_c__c = :a.Id AND ContactId_c__c = :con.Id LIMIT 1][0].Id;
        Sopimustieto__c st = GEN_TestFactory.createSopimusTekija2(a.Id, yhteyshenkiloId, FALSE);
        st.Tila__c = 'Valmis lähetettäväksi';
        insert st;
        Attachment att = GEN_TestFactory.createAttachment(st.Id, TRUE);
        Arvolista__c al = New Arvolista__c(Arvo_1_Teksti__c = oweaId, Ryhma__c = 'AUTOMATIC_EMAIL_SENDER_IPR_OWNER');
        insert al;
        
        Test.startTest();
            OIKOM_Batch_Sopimuslahetys sopimus = new OIKOM_Batch_Sopimuslahetys();
            ID batchprocessid = Database.executeBatch(sopimus);
        Test.stopTest();
    }
}