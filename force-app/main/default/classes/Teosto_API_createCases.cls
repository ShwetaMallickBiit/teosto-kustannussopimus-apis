public with sharing class Teosto_API_createCases {
 /* Created Date : 27.3.2019
    Created By   : Shwetha Telkar
    Description  : Implement Salesforce Rest API integration.This class called by KAYT_API_AWS_RetrieveMicroService.cls 
                   - this class will process incoming cases(Tapaus) & creates Tapaus records ,
                   - checks if external Customer Id if found, if yes Links to Asiakas (Accounts) 
                   - Type , Subject are mandatory fields to be sent by AWS 
                   - create the response message ,set the statuscode & statusMessage
                   - sends Rest Response message back. 
*/
    public static List<String> getIncomingAccountIds(List<KAYT_API_SalesforceRequest.inboundCase> paramCases) {
            List<String> ids = new List<String>();
            for(KAYT_API_SalesforceRequest.inboundCase i : paramCases) { 
                if(i.tekijanro!= null) {
                    ids.add(i.tekijanro);
                    }
            }
            return ids;
    }
    public static List<String> getIncomingSopimusIds(List<KAYT_API_SalesforceRequest.inboundCase> paramCases) {
            List<String> ids = new List<String>();
            for(KAYT_API_SalesforceRequest.inboundCase i : paramCases) { 
                if(i.sopimusnro!= null) {
                    ids.add(i.sopimusnro);
                    }
            }
            return ids;
    }
    public static Map<String, Account> getAccountMap(List<String> ids) {
        Map<String,Account> accountMap = new Map<String,Account>();
        List<Account> accounts = [SELECT Id, Asiakasnumero_Tepa__c FROM Account WHERE Asiakasnumero_Tepa__c IN :ids];
        for(Account a : accounts) {
            accountMap.put(a.Asiakasnumero_Tepa__c, a);
        }
        return accountMap;   
    }
    public static Map<String, Sopimustieto__c> getSopimusMap(List<String> ids) {
        Map<String,Sopimustieto__c> sopimusMap = new Map<String,Sopimustieto__c>();
        List<Sopimustieto__c> sopimukset= [SELECT Id, Name, Asiakas_tekija__c FROM Sopimustieto__c WHERE Name IN :ids];
        for(Sopimustieto__c s : sopimukset) {
            sopimusMap.put(s.Name, s);
        }
        return sopimusMap;   
    }

    public static Tapaus__c mapTapaus(KAYT_API_SalesforceRequest.inboundCase i, Map<String, Account> accountMap, Map<String, Sopimustieto__c> sopimusMap) {
        Tapaus__c t = new Tapaus__c();
        t.Aihe__c = i.aihe;
        t.Kuvaus_html__c = i.kuvaus;
        t.Tyyppi__c = i.tyyppi;
        t.Alatyyppi__c = i.alatyyppi1;
        t.Alatyyppi_2__c = i.alatyyppi2;
        t.Lahde__c = i.lahde;
        t.Tila__c = 'Avoin';
        if(accountMap.containsKey(i.tekijanro))
            t.Asiakas__c = accountMap.get(i.tekijanro).Id;
        if(sopimusMap.containsKey(i.sopimusnro)) {
            t.Sopimustieto__c = sopimusMap.get(i.sopimusnro).Id;
            t.Asiakas__c = sopimusMap.get(i.sopimusnro).Asiakas_tekija__c;
        }

        return t;

    }
    
    public static List<Tapaus__c> getCases(List<KAYT_API_SalesforceRequest.inboundCase> paramCases, Map<String, Account> accountMap, Map<String, Sopimustieto__c> sopimusMap) {
        List<Tapaus__c> incomingCases = new List<Tapaus__c>();  
        for(KAYT_API_SalesforceRequest.inboundCase i : paramCases) { 
            if(i.aihe==null || i.aihe.trim()=='' || i.tyyppi==null || i.tyyppi.trim()=='') { 

            } else {
                Tapaus__c t = mapTapaus(i, accountMap, sopimusMap);
                incomingCases.add(t);    
            }
        }
        return incomingCases;
    }
    public static RestResponse generateResponse(List<Tapaus__c> incomingCases, String statusCode, String statusMessage){
        RestResponse res = RestContext.response; 
        KAYT_API_SalesforceResponse.caseResponse returncaseResponse = new KAYT_API_SalesforceResponse.caseResponse();
        List<KAYT_API_SalesforceResponse.createdCases> cases = new List<KAYT_API_SalesforceResponse.createdCases>();
        for(Tapaus__c t : incomingCases) {
            KAYT_API_SalesforceResponse.createdCases c = new KAYT_API_SalesforceResponse.createdCases();
            c.tapausid = t.Id;
            c.tapausnro = t.Name;
            cases.add(c);
        }
        returncaseResponse.statusCode = statusCode;
        returncaseResponse.statusMessage=statusMessage;
        returncaseResponse.tapaukset = cases;
        res.addHeader('Content-Type', 'application/json');
        res.responseBody=Blob.valueOf(JSON.serialize(returncaseResponse));
        return res;
    }
    public static RestResponse createCase(List<KAYT_API_SalesforceRequest.inboundCase> paramCases) {
        String statusCode = '200';
        String statusMessage = 'Success';
        System.debug('requestMessage:-incoming  paramCases :- '+ paramCases);   
        List<String> ids_asiakas = getIncomingAccountIds(paramCases);
        List<String> ids_sopimus = getIncomingSopimusIds(paramCases);
        Map<String,Account> accountMap = getAccountMap(ids_asiakas);
        Map<String,Sopimustieto__c> sopimusMap = getSopimusMap(ids_sopimus);        
        List<Tapaus__c> incomingCases = getCases(paramCases, accountMap, sopimusMap);
        if(incomingCases==null || incomingCases.size()==0) { 
            statusCode = '204';
            statusMessage = 'Empty case list';
        }
        insert incomingCases;
        incomingCases = [SELECT Id, Name FROM Tapaus__c WHERE Id IN :incomingCases];
        return generateResponse(incomingCases, statusCode, statusMessage);
    }
}