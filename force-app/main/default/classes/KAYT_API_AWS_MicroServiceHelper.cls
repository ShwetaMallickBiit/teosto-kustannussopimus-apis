public with sharing class KAYT_API_AWS_MicroServiceHelper {

    public static KAYT_API_SalesforceRequest deserializeRequestMsg(RestRequest req) {
        System.debug('req.requestBody.toString()' + req.requestBody.toString());
        KAYT_API_SalesforceRequest requestMessage = (KAYT_API_SalesforceRequest)JSON.deserialize(req.requestBody.toString(),KAYT_API_SalesforceRequest.Class);
        system.debug('requestMessage'+requestMessage);    
        return requestMessage;
    }
    public static Boolean createCases(KAYT_API_SalesforceRequest requestMessage) {
        if(requestMessage.SalesforceRequest.setTapaus != null) return true;
        return false;
    }
    public static Boolean createAgreements(KAYT_API_SalesforceRequest requestMessage) {
        if(requestMessage.SalesforceRequest.setSopimus != null) return true;
        return false;
    }
}