@isTest(seeAllData=false)
private class AccountContactRelationTriggerTest{

    static testMethod void testSuccess() {
        GEN_TestFactory.generateAccountWithContacts();
        List<AccountContactRelation> accCon = [SELECT Id, Sidoksen_email__c 
                                               FROM AccountContactRelation
                                               LIMIT 1];
        accCon[0].EndDate  = date.today() + 1;
        update accCon;
    }
}