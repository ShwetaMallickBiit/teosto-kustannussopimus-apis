/*
    Description
                This Apex Class creates Tapaus__c record from inbound email. The class is used in Salesforce Email Services (inbound)
*/
global class GEN_Email2Tapaus implements Messaging.InboundEmailHandler {

    public static Tapaus__c getTapausData(Messaging.InboundEmail email, string emailBodyText, string emailBodyHtml) {

        Integer runningNr = 255;
        Tapaus__c ticket = New Tapaus__c();
        string emailBody;
        ticket.Viimeisin_viesti__c = emailBodyText.left(99999);
        ticket.Viimeisin_viesti_html__c = emailBodyHtml.left(99999);

        if(string.isBlank(emailBodyHtml)) emailBody = emailBodyText;
        else emailBody = emailBodyHtml;
        
        ticket.x_Viimeisin_viesti_1__c = emailBody.left(255);
        ticket.x_Viimeisin_viesti_2__c = emailBody.mid(255, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_3__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_4__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_5__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_6__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_7__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_8__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_9__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_10__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_11__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_12__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_13__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_14__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_15__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_16__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_17__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_18__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_19__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_20__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_21__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_22__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_23__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_24__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_25__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_26__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_27__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_28__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_29__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_30__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_31__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_32__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_33__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_34__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_35__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_36__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_37__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_38__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_39__c = emailBody.mid(runningNr, 255); runningNr += 255;
        ticket.x_Viimeisin_viesti_40__c = emailBody.mid(runningNr, 255);
        /* three dots is added if text is cut */
        if(emailBody.length() > runningNr - 1) {
            ticket.x_Viimeisin_viesti_20__c = emailBody.mid(runningNr, 250) + '...';
        }
        ticket.Viestipvm__c = datetime.now();
        ticket.Lukemattomia_viesteja__c = true;

        return ticket;
    }

    public static string getToAddress(Messaging.InboundEmail email) {

        string toAddress = '';
        if(email.toAddresses != null && !email.toAddresses.isEmpty() && email.toAddresses.size() > 0)
        {
            for( string toaddr : email.toAddresses)
            {
                toAddress = toAddress != null ? toAddress + ';' + toaddr : toaddr;
            }
        }
        return toAddress;
    }

    public static string getCcAddress(Messaging.InboundEmail email) {
    
        string ccAddress = '';
        if( email.ccAddresses != null && !email.ccAddresses.isEmpty() && email.ccAddresses.size() > 0)
        {
            for( string ccaddr : email.ccAddresses)
            {
                ccAddress = ccAddress != null ? ccAddress + ';' + ccaddr : ccaddr;
            }
        }
        return ccAddress;
    }

    public static Viesti__c createViesti(Messaging.InboundEmail email, string ticketId, string emailBodyHtml, string toAddress, string ccAddress) {
        Viesti__c viesti = new Viesti__c();
        viesti.Aihe__c = email.subject.left(255);
        viesti.Kuvaus__c = emailBodyHtml.left(99999);
        viesti.Lahettaja_text__c = email.fromAddress;
        viesti.Tapaus__c = ticketId;
        viesti.Vastaanottaja_text__c = toAddress.left(255);
        viesti.CC__c = ccAddress.left(255);

        insert viesti;
        return viesti;
    }
    public static EmailMessage createEmailMsg(Messaging.InboundEmail email, string ticketId, string emailBodyText, string emailBodyHtml, string toAddress, string ccAddress) {

            EmailMessage emailMsgCreation = new EmailMessage();
            emailMsgCreation.RelatedToId = ticketId;
            emailMsgCreation.FromAddress = email.fromAddress;
            emailMsgCreation.FromName = email.fromName;
            emailMsgCreation.CcAddress= ccAddress;
            emailMsgCreation.ToAddress = toAddress;
            emailMsgCreation.Subject = email.subject.left(255);
            emailMsgCreation.TextBody = emailBodyText.left(32000);
            emailMsgCreation.HtmlBody = emailBodyHtml.left(32000);
            emailMsgCreation.MessageDate = system.now();
            emailMsgCreation.Incoming = TRUE;
            emailMsgCreation.Status = '1';
            insert emailMsgCreation;
            return emailMsgCreation;
    }

    public static void updateWhoId(Messaging.InboundEmail email, string ticketId, EmailMessage emailMsgCreation) {
            string activityId = '';
            string contactId = '';
            List<Tapaus__c> tapList = [SELECT Id, Yhteyshenkilo__c FROM Tapaus__c WHERE Id = :ticketId LIMIT 1];
            List<EmailMessage> emailMsgList = [SELECT Id, ActivityId FROM EmailMessage WHERE Id = :emailMsgCreation.Id LIMIT 1];
            if(!emailMsgList.isEmpty()) activityId = emailMsgList[0].ActivityId;
            if(!tapList.isEmpty()) contactId = tapList[0].Yhteyshenkilo__c;
            Task taskRecord = new Task();
            taskRecord.Id = activityId; system.debug('activityid = ' + emailMsgCreation.ActivityId);
            taskRecord.WhoId = contactId;
            
            if(!string.isblank(activityId) && activityId != '') update taskRecord;

    }

    public static void insertAttachments(Messaging.InboundEmail email, string ticketId, EmailMessage emailMsgCreation, Viesti__c viesti) {
            List<Attachment> attList = new List<Attachment>();
            if(email != null && email.binaryAttachments != null && email.binaryAttachments.size() > 0) {
                for( Messaging.InboundEmail.BinaryAttachment file : email.binaryAttachments) 
                {
                    Attachment attachment = new Attachment();
                    attachment.Name = file.fileName;
                    attachment.Body = file.body;
                    attachment.ParentId = viesti.Id;
                    attachment.ParentId = emailMsgCreation.Id;
                    attList.add(attachment);
                }
            }

            if(email != null && email.textAttachments != null && email.textAttachments.size() > 0)   
            {
                for (Messaging.Inboundemail.TextAttachment textFile : email.textAttachments) {
                    Attachment attachment = new Attachment();
                    attachment.Name = textFile.fileName;
                    attachment.Body = Blob.valueOf(textFile.body);
                    attachment.ParentId = viesti.Id;
                    attachment.ParentId = emailMsgCreation.Id;
                    attList.add(attachment);
                }
            }

            if(email != null && attList != null && !attList.isEmpty() && attList.size() > 0) {
                insert attList;
            }
    }

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.Inboundenvelope envelope) {

        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        string debugText = '11'; // this is used to debug issue with 'Attempt to de-reference a null object'
        string emailBodyText;
        string emailBodyHtml;
        boolean isNew = false;
        
        if(string.isblank(email.plainTextBody) || email.plainTextBody == null || email.plainTextBody == '') {
            emailBodyText = '';
        }
        else {
            emailBodyText = email.plainTextBody;
        }
        if(string.isblank(email.htmlBody) || email.htmlBody == null || email.htmlBody == '' || email.htmlBody == 'null') {
            emailBodyHtml = '';
        }
        else {
            emailBodyHtml = email.htmlBody;
        }

        try {
            string ticketId;                   
            string emailsubject = email.subject.right(21);
            string ccAddress = getCcAddress(email);
            string toAddress = getToAddress(email);

            // Find existing tapaus records ("tickets") by comparing the subject of incoming email with existing tapaus records
            List<Tapaus__c> ticketLst = [SELECT Id, Viimeisin_viesti__c, x_Inbound_Id__c, Sulkemispvm__c  FROM Tapaus__c WHERE x_Inbound_Id__c =:emailsubject  LIMIT 1];
            Tapaus__c ticket = new Tapaus__c();

            // If ticket (Tapaus) already exists, new Tapaus record is not created
            // if ticket is already closed creates a new entry with link to closed one.
            if(!ticketLst.isEmpty() && ticketLst[0].Sulkemispvm__c == null  )
            {
                ticket = ticketLst[0];
                ticket = getTapausData(email, emailBodyText, emailBodyHtml);
                ticketId = ticketLst[0].Id;
                ticket.Id = ticketId;
                update ticket;               
            }
            // Otherwise create a new record and in case referenced Tapaus was closed add the link to that.
            else
            {
                ticket = getTapausData(email, emailBodyText, emailBodyHtml);
                ticket.Aihe__c = email.subject.left(255);
                ticket.Kuvaus_html__c = emailBodyHtml.left(99999);
                if(string.isblank(emailBodyHtml) || emailBodyHtml == '') ticket.kuvaus_html__c = emailBodyText.left(99999);
                else ticket.kuvaus_html__c = emailBodyHtml.left(99999);
                
                ticket.Kuvaus__c = emailBodyText.left(99999);
                ticket.Kuvaus_text__c = emailBodyText.left(99999);
                ticket.Viimeisin_viesti__c = emailBodyText.left(99999);
                ticket.Viimeisin_viesti_html__c = emailBodyHtml.left(99999);              
                ticket.x_Email_Service_Address__c = envelope.toAddress;
                ticket.Lahettaja__c = email.fromAddress;
                // validate if Tapaus is closed
                if( !ticketLst.isEmpty() && ticketLst[0].Sulkemispvm__c != null  ){
                    ticket.Liittyva_tapaus__c = ticketLst[0].Id;
                }
                ticket.Tila__c = 'Avoin';
                insert ticket;
                ticketId = ticket.Id;
                isNew = true;
            }

            // Create a new Viesti__c record
            Viesti__c viesti = createViesti(email, ticketId, emailBodyHtml, toAddress, ccAddress);

            // Create new EmailMessage related to tapaus to make inbound email visible in activity history
            EmailMessage emailMsgCreation = createEmailMsg(email, ticketId, emailBodyText, emailBodyHtml, toAddress, ccAddress);
            if(isNew == true) {
                ticket.x_Sahkoposti_Id__c = emailMsgCreation.Id;
                update ticket;
            }

            // update WhoId to activity based on Contact on Tapaus__c record
            updateWhoId(email, ticketId, emailMsgCreation);

            // insert attachments
            insertAttachments(email, ticketId, emailMsgCreation, viesti);

            result.success = true;
                
        } catch (Exception e) {
            result.success = false;
            result.message = e.getStackTraceString();
            System.debug(e.getMessage()); 
            Loki__c errorLog  = new Loki__c();
            errorLog.Objekti__c = 'Tapaus__c, Viesti__c';
            errorLog.Toiminnallisuus__c = 'OIKOM_Email2Tapaus';
            errorLog.Tyyppi__c = 'Virhe';
            errorLog.Virheviesti__c = e.getMessage();   
            string moreInfo =  email.subject + ' / ' + email.fromAddress;
            errorLog.Lisatieto_1_Pitkatekstikentta__c = debugText + '/' + moreInfo + '/' + emailBodyText.left(40000) + '/' + emailBodyHtml.left(40000);
            insert errorLog;
        }
        
        return result;
    }
}