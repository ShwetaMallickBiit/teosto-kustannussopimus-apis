/*
Description
            Data migration for agreements
*/
global class dm_ChangeContractOwnerOfOikom implements Database.stateful, Schedulable, Database.Batchable<sObject>{
    public Database.QueryLocator start(Database.BatchableContext bc) {
        String soql;
        soql = 'SELECT Id, Asiakas_tekija__c, Yhteyshenkilo__c, Vastuuhenkilo__c, Vastuuhenkilo__r.Id_c__c FROM Sopimustieto__c WHERE (RecordType.DeveloperName = \'Kustantajan_asiakassopimus\' OR RecordType.DeveloperName = \'Tekijan_asiakassopimus\')';
        return Database.getQueryLocator(soql);
    }
    public static Set<Id> getIdsFromSObjectList(List<sObject> recs) {
        Set<Id> ids = New Set<Id>();
        for(sObject rec : recs) {
            ids.add((id)rec.get('Asiakas_tekija__c'));
        }
        return ids;
    }
    public static Map<String, String> accConMapC(List<Sopimustieto__c> records) {
        Map<String, String> accConMap = New Map<String, String>();
        Set<Id> ids = getIdsFromSObjectList(records);
        List<AccountContactRelation_c__c> accCons = [SELECT Id, AccountId_c__c, ContactId_c__c FROM AccountContactRelation_c__c WHERE AccountId_c__c IN :ids];
        for(AccountContactRelation_c__c accCon : accCons) {
            accConMap.put((string)accCon.AccountId_c__c + (string)accCon.ContactId_c__c, (string)accCon.Id);
            system.debug('YYY: ' + (string)accCon.AccountId_c__c + (string)accCon.ContactId_c__c);
            system.debug('ZZZ: ' + (string)accCon.Id);
        }
        return accConMap;
    }
    public void execute(Database.BatchableContext bc, List<Sopimustieto__c> records) {
        Map<String, String> accConMap = accConMapC(records);
        List<Sopimustieto__c> sops = New List<Sopimustieto__c>();
        for(Sopimustieto__c rec : records) {
            Sopimustieto__c sop = New Sopimustieto__c();
            sop.Id = rec.Id;
            system.debug('XXX: ' + (string)sop.Asiakas_tekija__c + (string)sop.Yhteyshenkilo__c);
            sop.Vastuuhenkilo__c = accConMap.get((string)rec.Asiakas_tekija__c + (string)rec.Yhteyshenkilo__c);
            sops.add(sop);
        }
        update sops;
    }    
    public void finish(Database.BatchableContext bc) {
    
    }
    public void execute(SchedulableContext sc){
        Database.executeBatch(this,1);
    }
}