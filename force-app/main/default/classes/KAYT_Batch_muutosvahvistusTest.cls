@isTest(seeAllData=true)
private class KAYT_Batch_muutosvahvistusTest {

    static testMethod void getContractData() {

       // create account
       Account acc = new Account(
           Name = 'TestAccount', Kieli__c = 'Suomi', Asiakasryhma__c = 'Käyttäjä', Luokittelu__c ='Avain',
           Ohita_Y_tunnus_Hetu_validointi__c = true, ShippingStreet = 'Testikatu 10', ShippingCity = 'Helsinki',
           ShippingPostalCode = '00100');
       insert acc;

       // get record type id    
       Id RecordTypeIdST = Schema.SObjectType.Sopimustieto__c.getRecordTypeInfosByName().get('Musiikinkäyttösopimus, sopimusvahvistus').getRecordTypeId(); 

       // create contact
       Contact con = new Contact(
           FirstName = 'Etunimi', LastName = 'Sukunimi');
       insert con;

       // create Acc_con__c
       AccountContactRelation_c__c acc_con = new AccountContactRelation_c__c(
           AccountId_c__c = acc.Id, ContactId_c__c = con.Id,Name = 'Testinimi', Sidoksen_email_c__c = 'testi.testinen@biit.fi');
       insert acc_con;

       //  Run test
       Test.startTest();
       /* this nulls SOQL limits after seed data insert */
       System.assertEquals('1', '1'); 

       // create Sopimustieto for XXX
       Sopimustieto__c st_mv = new Sopimustieto__c(
           recordtypeid=RecordTypeIdST, Asiakas_tekija__c = acc.Id, OnEri_kuin_postitusosoite__c = true,
           Laskutusosoite_Katu__c = 'Testikatu 1', Laskutusosoite_Kaupunki__c = 'Helsinki', Laskutusosoite_Postinumero__c = '00100',
           x_onTuoteosto__c = true, Vastuuhenkilo__c = acc_con.Id, Allekirjoittaja__c = acc_con.Id, Tila__c = 'Sopimus lähetetty',
           Alkupvm__c = Date.newInstance(2018, 01, 01), Sopimuksen_allekirjoituspvm__c = Date.newInstance(2018, 01, 01), 
           Sopimusvahvistus_generointipaiva_dt__c = DateTime.newInstance(2018, 01, 02, 0, 0, 0),
           Sopimusvahvistus_lahetyspaiva_dt__c = DateTime.newInstance(2018, 01, 02, 0, 0, 0),
           Muutosvahvistus_generointipaiva_dt__c = Date.newInstance(2018, 01, 03),
           Tuoteoston_muokkauspvm__c = DateTime.newInstance(2200, 04, 04, 0, 0, 0),
           x_Muutosvahvistus_pdf_Id_Kayt__c = '12345678'
           );
       insert st_mv;

       // create Tuoteosto
       Tuoteosto__c conData = new Tuoteosto__c(Sopimustieto__c = st_mv.Id, Asiakas__c = acc.Id);
       insert conData;
       
       KAYT_Batch_muutosvahvistusPDFgenerointi muutVahvGen = new KAYT_Batch_muutosvahvistusPDFgenerointi();
       ID batchprocessid1 = Database.executeBatch(muutVahvGen);
//       KAYT_Batch_muutosvahvistusLahetys muutVahvLah = new KAYT_Batch_muutosvahvistusLahetys();
//       ID batchprocessid2 = Database.executeBatch(muutVahvLah);

    }
}