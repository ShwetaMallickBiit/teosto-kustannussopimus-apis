global class DM_KAYT_KontaktiImportTrigger implements Database.stateful, Schedulable, Database.AllowsCallouts, Database.Batchable<sObject>{
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        String soql = 'SELECT Id, onDatamigraatio__c, Batch_Status__c FROM DM_Tasokas_kontakti__c WHERE onDatamigraatio__c = FALSE';
        return Database.getQueryLocator(soql);
    }

    public void execute(Database.BatchableContext bc, List<DM_Tasokas_kontakti__c> records) {

        for (DM_Tasokas_kontakti__c record : records) {
            try {
                /* TRIGGER BATCH */
                DM_Tasokas_kontakti__c kontakti = new DM_Tasokas_kontakti__c();
                kontakti.Id = record.Id;
                kontakti.onDatamigraatio__c = TRUE;
                kontakti.Batch_Status__c = 'SUCCESS';
                update kontakti;
                /* UPDATE SUCCESS */
                DM_Tasokas_kontakti__c kontakti_success = new DM_Tasokas_kontakti__c();
                kontakti_success.Id = record.Id;
                kontakti_success.Batch_Status__c = 'SUCCESS';
            }
            catch(Exception e) {
                System.debug(e.getMessage());

                /* UPDATE ERROR */
                DM_Tasokas_kontakti__c kontakti_error = new DM_Tasokas_kontakti__c(); kontakti_error.Id = record.Id; kontakti_error.Batch_Status__c = 'ERROR'; update kontakti_error;
            }

        }
    }
    public void finish(Database.BatchableContext bc) {
        
    }
    public void execute(SchedulableContext sc){
        Database.executeBatch(this,1);
    }
}