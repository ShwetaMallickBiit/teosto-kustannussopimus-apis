global class SELFADMIN_Notification_Scheduler implements Schedulable {

    global static SelfAdminRestApi__mdt getNotificationInterval() {
        return [SELECT Id, DeveloperName, mindatefromtoday__c, maxdatefromtoday__c FROM SelfAdminRestApi__mdt WHERE DeveloperName = 'FetchNotifications' LIMIT 1][0];
    }

    global static String getNotificationsFromDB2API(Date minDate, Date maxDate, String status) {
        string apiFetchStatus = 'OK';
        try{
            SELFADMIN_Notification_Controller.getAdministrationNotificationsForPerioid(minDate, maxDate, status);
        }             
        catch(Exception e) {
                System.debug(e.getMessage());
                Loki__c errorLog = new Loki__c(Objekti__c = 'Itsehallinnointi__c', Toiminnallisuus__c = 'SELFADMIN_Notification_Scheduler', Tyyppi__c = 'Virhe', Virheviesti__c = e.getMessage());
                insert errorLog;
                return 'NOTOK';
        }
        return apiFetchStatus;
    }
    global void execute(SchedulableContext sc) {

        // fetches notifications from last 7 days
        SelfAdminRestApi__mdt fetchParam = getNotificationInterval();
        string apiFetchStatus = getNotificationsFromDB2API(date.Today() - integer.valueOf(fetchParam.mindatefromtoday__c), date.Today() - integer.valueOf(fetchParam.maxdatefromtoday__c), '');
        
        // sends email notifications that are configured in custom metadata. Separate batch is triggered for each notification
        if(apiFetchStatus == 'OK') {
            List<Itsehallinnointisahkoposti__mdt> metadatas = New List<Itsehallinnointisahkoposti__mdt>();
            metadatas = 
                [SELECT MasterLabel, DeveloperName, Haku__c, Sahkopostilomake_FIN__c, Sahkopostilomake_SWE__c, Sahkopostilomake_EN__c, LahetaSahkopostiin__c, Lahetysviive__c, EraajonKoko__c, LahettavaSahkopostiosoite__c 
                 FROM Itsehallinnointisahkoposti__mdt
                 WHERE Aktiivinen__c = TRUE];

            integer minutesFromNow;
            integer batchSize;
            for(Itsehallinnointisahkoposti__mdt metadata : metadatas) {
                minutesFromNow = (integer)metadata.Lahetysviive__c;
                batchSize = (integer)metadata.EraajonKoko__c;
                SELFADMIN_NotificationEmailBatch b = new SELFADMIN_NotificationEmailBatch(metadata);
                if(!test.isRunningTest())
                    system.scheduleBatch(b, metadata.DeveloperName, minutesFromNow, batchSize);
            }
        }
    }
}