global class KAYT_Batch_WekaratunnustenPassivointi implements Database.Batchable<sObject>, Database.Stateful, Schedulable, Database.AllowsCallouts {
        
    String query = 'SELECT x_Sopimusnro__c, Web_asiakasnumero__c, Web_kayttajatunnus__c, Wekara_status__c, Webtunnus_tila__c FROM Sopimuksen_yhteyshenkilo__c WHERE Webtunnus_tila__c = \'Aktiivinen\' AND Loppupvm__c < TODAY';
        
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Sopimuksen_yhteyshenkilo__c> scope) {
         
        List<Sopimuksen_yhteyshenkilo__c> updateList = new List<Sopimuksen_yhteyshenkilo__c>();
        List<KAYT_VerkkopalveluKayttajahallinta.UserCallInput> inputList = new List<KAYT_VerkkopalveluKayttajahallinta.UserCallInput>();
        KAYT_VerkkopalveluKayttajahallinta.UserCallInput input = new KAYT_VerkkopalveluKayttajahallinta.UserCallInput();

        // For every yhteyshenkilo in scope call KAYT_VerkkopalveluKayttajahallinta.KAYT_IntegrationCall
        // and save the output to change the Wekara status
        for (Sopimuksen_yhteyshenkilo__c yhteyshenkilo : scope) {
            inputList.clear();
            input.callType = 'User_Status_Update_Call';
            input.contractNumber = yhteyshenkilo.x_Sopimusnro__c;
            input.customerNumber = yhteyshenkilo.Web_asiakasnumero__c;
            input.customerType = yhteyshenkilo.Web_kayttajatunnus__c.mid(0, 2);
            input.userStatus = 'passive';
            inputList.add(input);
            List<KAYT_VerkkopalveluKayttajahallinta.UserCallOutput> output = KAYT_VerkkopalveluKayttajahallinta.KAYT_IntegrationCall(inputList);
            if (output.get(0).Status == 'ERROR') {
                yhteyshenkilo.Wekara_status__c = 'ERROR';
            } else {
                yhteyshenkilo.Webtunnus_tila__c = 'Passiivinen';
            }
            updateList.add(yhteyshenkilo);
        }
        try {
            update updateList;
            } 
        catch(Exception e) 
            {
            System.debug(e.getMessage());
            Loki__c errorLog = new Loki__c(Objekti__c = 'Sopimuksen_yhteyshenkilo__c', Toiminnallisuus__c = 'KAYT_Batch_WekaratunnustenPassivointi', Tyyppi__c = 'Virhe', Virheviesti__c = e.getMessage());
            insert errorLog;
            }    
    }
        
    global void finish(Database.BatchableContext BC) {
        
    }
    
    public void execute(SchedulableContext sc){
        Database.executeBatch(this,1);
    }
}