global class SELFADMIN_NotificationEmailBatch implements Database.Batchable<sObject>,  Database.Stateful{

    private Itsehallinnointisahkoposti__mdt metadata;
    public SELFADMIN_NotificationEmailBatch(Itsehallinnointisahkoposti__mdt inputParam) {
        metadata = inputParam;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(metadata.Haku__c);
    }
    global void execute(Database.BatchableContext BC, List<sObject> recs) {
        try {
            if(!recs.isEmpty())
                SELFADMIN_Notification_Handler.sendEmailsIfNeeded(metadata, recs);
        } catch(Exception e) {
                System.debug(e.getMessage());
                Loki__c errorLog = new Loki__c(Objekti__c = 'Itsehallinnointi__c', Toiminnallisuus__c = 'SELFADMIN_NotificationEmailBatch, ' + metadata.DeveloperName, Tyyppi__c = 'Virhe', Virheviesti__c = e.getMessage());
                insert errorLog;
        }
    }
    global void finish(Database.BatchableContext BC) {
        Loki__c errorLog = new Loki__c(Objekti__c = 'Itsehallinnointi__c', Toiminnallisuus__c = 'SELFADMIN_NotificationEmailBatch' + metadata.DeveloperName, Tyyppi__c = 'Merkintä', Virheviesti__c = 'SELFADMIN_NotificationEmailBatch run completed');
        insert errorLog;
    }
}