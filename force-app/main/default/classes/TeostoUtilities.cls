global with sharing class TeostoUtilities{
    public static Set<Id> getIdSet(List<sObject> recs) {
        Set<Id> ids = New Set<Id>();
        for(sObject rec : recs) {
            ids.add(rec.Id);
        }
        return ids;
    }
    public static Set<String> getStringSetOfSObjectList(List<sObject> recs, String fieldName) {
        Set<String> strings = New Set<String>();
        for(sObject rec : recs) {
            strings.add((string)rec.get(fieldName));
        }
        return strings;
    }
    
    public static Map<String, sObject> getMapOfSObjectList(List<sObject> recs, String fieldName) {
        Map<String, sObject> sObjMap = New Map<String, sObject>();
        for(sObject rec : recs) {
            sObjMap.put((string)rec.get(fieldName), rec);
        }
        return sObjMap;
    }
    public static List<Sopimustieto__c> getSopimustietoList(Set<Id> ids, String branch) {
        Set<String> sopimustyypit = New Set<String>();
        sopimustyypit.add('Tekijan_asiakassopimus');
        sopimustyypit.add('Kustantajan_asiakassopimus');
        return [SELECT Id, Asiakas_tekija__c, Vastuuhenkilo__c, Sopimuksen_vastuuhenkilo_email__c, Vastuuhenkilo__r.Id_c__c
                FROM Sopimustieto__c 
                WHERE Asiakas_tekija__c IN :ids AND RecordType.DeveloperName IN :sopimustyypit];
    }
    public static List<AccountContactRelation> getAccountContactRelationList(Set<Id> ids, String branch) {
        return [SELECT Id, AccountId, ContactId, x_sahkoposti__c, x_Aktiivinen__c
                FROM AccountContactRelation 
                WHERE AccountId IN :ids AND x_Aktiivinen__c = TRUE];
    }
    public static AccountContactRelation getSopimusEmail(String accountId, List<Sopimustieto__c> sops, Map<Id, AccountContactRelation> acrMap, String branch) {
        AccountContactRelation acrEmail = New AccountContactRelation();
        String contactId = '';
        String email = '';
        for(Sopimustieto__c sop : sops) {
            if(sop.Asiakas_tekija__c == accountId) {
                if(acrMap.containsKey(sop.Vastuuhenkilo__r.Id_c__c))
                    acrEmail = acrMap.get(sop.Vastuuhenkilo__r.Id_c__c);
                break;
            }
        }
        return acrEmail;
    }
    public static AccountContactRelation getAccountContactRelationEmail(String accountId, List<AccountContactRelation> acrs, String branch) {
        AccountContactRelation acrEmail = New AccountContactRelation();
        Integer counter = 0;
        for(AccountContactRelation acr : acrs) {
            if(accountId == acr.AccountId && !string.isBlank(acr.x_Sahkoposti__c)) {
                if(counter == 0) {
                    acrEmail = acr;
                } else {
                    acrEmail = null;
                    break;
                }
                counter += 1;
            }
        }
        return acrEmail;
    }    
    public static Map<Id, AccountContactRelation> getAccountPrimaryEmail(List<Account> accs, String branch) {
        Map<Id, AccountContactRelation> primaryEmailMap = New Map<Id, AccountContactRelation>();
        AccountContactRelation acr = New AccountContactRelation();
        Set<Id> ids = getIdSet(accs);
        String email = '';
        List<Sopimustieto__c> sops = getSopimustietoList(ids, 'OIKOM');
        List<AccountContactRelation> acrs = getAccountContactRelationList(ids, 'OIKOM');
        Map<Id, AccountContactRelation> acrMap = New Map<Id, AccountContactRelation>(acrs);
        for(Account acc : accs) {
            acr = getSopimusEmail(acc.Id, sops, acrMap, 'OIKOM');
            if(string.isBlank(acr.x_Sahkoposti__c)) acr = getAccountContactRelationEmail(acc.Id, acrs, 'OIKOM');
            primaryEmailMap.put(acc.Id, acr);
        }
        return primaryEmailMap;
    }

}