@isTest
private class KAYT_SopimusFetcherTest {
 @isTest
    static void testFetchContracts () {
            Account acc = new Account();
                    acc.name='Account';
                    acc.Etunimi__c = 'Account';
                    acc.Asiakasryhma__c = 'Tekijä';
                    acc.Ominaisuus__c = 'Säveltäjä';
                    acc.Kieli__c =  '   Suomi';
                    acc.Email__c = 'testaccount@test.com';
                    acc.Luokittelu__c = 'Perus';
                    acc.Asiakasnumero_Tepa__c = '123';
                    acc.Maksutapa__c = 'Sepa-maksu';
                    acc.Ohita_Y_tunnus_Hetu_validointi__c = true;
                  insert acc;
            Contact con = new Contact();
                    con.AccountId = acc.id;
                    con.LastName = 'Batman begins';
                  insert con;
           // create Acc_con__c
            AccountContactRelation_c__c acc_con = new AccountContactRelation_c__c();
                   acc_con.AccountId_c__c = acc.Id;
                   acc_con.ContactId_c__c = con.Id;
                   acc_con.Name = 'Testinimi';
                   acc_con.Sidoksen_email_c__c = 'testi.testinen@biit.fi';
                 insert acc_con;
            Sopimustieto__c sopimus = new Sopimustieto__c();
                   sopimus.Asiakas_tekija__c = acc.id;
                   sopimus.Yhteyshenkilo__c = con.id;
                   sopimus.Alkupvm__c = System.today();
                   sopimus.Tekija__c = acc.id;
                 insert sopimus;
        Sopimuksen_yhteyshenkilo__c sopYhthlo = new Sopimuksen_yhteyshenkilo__c();
           sopYhthlo.Alkupvm__c = Date.newInstance(2018, 01, 01);
           sopYhthlo.Loppupvm__c = Date.newInstance(2018, 01, 02);
           sopYhthlo.Rooli__c = 'Sopimuksen yhteyshenkilö';
           sopYhthlo.Sopimustieto__c = sopimus.Id;
           sopYhthlo.Webtunnus_tila__c = 'Aktiivinen';
           sopYhthlo.AccountContactRelation_c__c = acc_con.Id;
           sopYhthlo.Web_kayttajatunnus__c = 'AS123456';
                 insert sopYhthlo;
      
        List<Sopimuksen_yhteyshenkilo__c> sop = [SELECT Id, Name, x_Sopimusnro__c,
         Sopimustieto__r.Sopimustyyppi__c,Yhteyshenkilo_Id__c,
         Sopimustieto__r.Alkupvm__c, Sopimustieto__r.Loppupvm__c
         FROM Sopimuksen_yhteyshenkilo__c WHERE Yhteyshenkilo_Id__c = :con.Id];
        system.debug('this is query : ' + sop);  
            

      List<Sopimuksen_yhteyshenkilo__c> result = KAYT_SopimusFetcher.getContracts(con.id);
             System.debug('Result: ' + result);
       System.assertEquals(result, sop);

    }
}