/*
Description
            This class is test class for Apex Class KAYT_VerkkopalveluKayttajahallinta
*/

@isTest
private class KAYT_VerkkopalveluKayttajahallintaTest {
    
        @testSetup static void setup() {
            ExtranetAPI__c params = new ExtranetAPI__c(name='params', endpointURL__c ='https://extranet.teosto.fi/soratest/wekara/users',
            Value__c='eyJraWQiOiJmODBhMjY5YzRlM2Y0NDFmOTFjZTFiYmIxMWExZmM3NiIsInR5cCI6IkpXVCIsImFsZyI6IkhTMjU2In0.eyJ1c2VybnVtYmVyIjo2NjcsInVzZXJ0eXBlIjoiWVAiLCJleHAiOjQ2NTg0NDQ0MTZ9.boPtogjUGHychCcDvt4mQhy_ZqxAJec8PS_KppzfhsI');
            insert params;
        }

        static testMethod void IntegrationCallTest() {
            List<KAYT_VerkkopalveluKayttajahallinta.UserCallInput> usrInput = new List<KAYT_VerkkopalveluKayttajahallinta.UserCallInput>();
            KAYT_VerkkopalveluKayttajahallinta.UserCallInput input = new KAYT_VerkkopalveluKayttajahallinta.UserCallInput();
            input.callType ='User_Creation_Call';
            input.name ='Aku Ankka';
            input.customerNumber ='1122334455';
            input.requestType = 'AS';
            input.customerType='EP';
            input.contractNumber='99887766';
            input.locationNumber='123456';
            input.userStatus='passive';
            usrInput.add(input);
            
            Test.startTest();  
                    
            Test.setMock(HttpCalloutMock.class, new KAYT_VerkkopalveluKayttajahallintaMock());
            
            Test.stopTest();
            
            List<KAYT_VerkkopalveluKayttajahallinta.UserCallOutput> intCall = KAYT_VerkkopalveluKayttajahallinta.KAYT_IntegrationCall(usrInput);
            System.assertEquals(intCall[0].Status, 'SUCCESS');
            System.assertNOTEquals(intCall[0].password, NULL);
            System.assertNOTEquals(intCall[0].username, NULL);
            System.assertNOTEquals(intCall[0].customerNumber, NULL);
            
            usrInput[0].callType ='User_Status_Update_Call';
            intCall = KAYT_VerkkopalveluKayttajahallinta.KAYT_IntegrationCall(usrInput);
            System.assertEquals(intCall[0].Status, 'SUCCESS');
            System.assertEquals(intCall[0].errMsg, NULL);
            
            usrInput[0].callType ='User_New_Password_Call';
            intCall = KAYT_VerkkopalveluKayttajahallinta.KAYT_IntegrationCall(usrInput);
            System.assertEquals(intCall[0].Status, 'SUCCESS');
            System.assertEquals(intCall[0].errMsg, NULL);
            System.assertNOTEquals(intCall[0].password, NULL);
            
            usrInput[0].callType ='User_Name_Change_Call';
            KAYT_VerkkopalveluKayttajahallinta.KAYT_IntegrationCall(usrInput);
        }
    
        static testMethod void UserCallTest() {
       
            KAYT_VerkkopalveluKayttajahallinta.UserCallInput usrInput = new KAYT_VerkkopalveluKayttajahallinta.UserCallInput();
            usrInput.name = 'Mikko Mouse';  
            usrInput.requestType = 'EP';
            usrInput.contractNumber = '100010176';
            usrInput.locationNumber = '1122334455';     
            
            Test.startTest();  
                    
            Test.setMock(HttpCalloutMock.class, new KAYT_VerkkopalveluKayttajahallintaMock());
            
            Test.stopTest();
            
            KAYT_VerkkopalveluKayttajahallinta.UserCallOutput usrCall = KAYT_VerkkopalveluKayttajahallinta.userCreationCall(usrInput);
                        
            System.assertEquals(usrCall.Status, 'SUCCESS');
            System.assertNOTEquals(usrCall.password, NULL);
            System.assertNOTEquals(usrCall.username, NULL);
            System.assertNOTEquals(usrCall.customerNumber, NULL);
        }
           
        static testMethod void UserCallFailedTest() {
                  
            KAYT_VerkkopalveluKayttajahallinta.UserCallInput usrInput = new KAYT_VerkkopalveluKayttajahallinta.UserCallInput();
            usrInput.name = 'Mikko Mouse';  
            usrInput.requestType = 'EP';
            usrInput.contractNumber = '100010176';
            usrInput.locationNumber = '1122334455';     
            
            Test.startTest();  
                    
            KAYT_VerkkopalveluKayttajahallinta.UserCallOutput usrCall = KAYT_VerkkopalveluKayttajahallinta.userCreationCall(usrInput);
            
            Test.stopTest();    
                        
            System.assertEquals(usrCall.Status, 'ERROR');
            System.assertNOTEquals(usrCall.errMsg, NULL);        
        }
            
        static testMethod void UserStatusUpdateCallTest() {
            
            KAYT_VerkkopalveluKayttajahallinta.UserStatusCallInput usrInput = new KAYT_VerkkopalveluKayttajahallinta.UserStatusCallInput();
            usrInput.customertype = 'AS';
//            usrInput.customernumber = '100010187';
            usrInput.customernumber = 100010187;
            usrInput.userStatus = 'passive';
            
            Test.startTest();  
                
            Test.setMock(HttpCalloutMock.class, new KAYT_VerkkopalveluKayttajahallintaMock());
        
            Test.stopTest();
            
            KAYT_VerkkopalveluKayttajahallinta vpkh = new KAYT_VerkkopalveluKayttajahallinta();
            KAYT_VerkkopalveluKayttajahallinta.CallOutputStatus usrCall = vpkh.userStatusCall(usrInput);
            
            System.assertEquals(usrCall.Status, 'SUCCESS');
            System.assertEquals(usrCall.errMsg, NULL); 
            
        }
        
            static testMethod void UserStatusUpdateCallFailedTest() {
            
            KAYT_VerkkopalveluKayttajahallinta.UserStatusCallInput usrInput = new KAYT_VerkkopalveluKayttajahallinta.UserStatusCallInput();
            usrInput.customertype = 'AS';
//            usrInput.customernumber = '100010187';
            usrInput.customernumber = 100010187;
            usrInput.userStatus = 'statusfailed';
            
            Test.startTest();  
                
            Test.setMock(HttpCalloutMock.class, new KAYT_VerkkopalveluKayttajahallintaMock());
        
            Test.stopTest();
            
            KAYT_VerkkopalveluKayttajahallinta vpkh = new KAYT_VerkkopalveluKayttajahallinta();
            KAYT_VerkkopalveluKayttajahallinta.CallOutputStatus usrCall = vpkh.userStatusCall(usrInput);
            
            System.assertEquals(usrCall.Status, 'ERROR');
            System.assertNOTEquals(usrCall.errMsg, NULL); 
            
            KAYT_VerkkopalveluKayttajahallinta.UserCallInput usrCallInput = new KAYT_VerkkopalveluKayttajahallinta.UserCallInput();
            usrCallInput.name = 'Mikko Mouse';  
            usrCallInput.requestType = 'EP';
            usrCallInput.contractNumber = '100010176';
            usrCallInput.locationNumber = '1122334455'; 
            usrCallInput.customertype = 'statusfailed';
            
            KAYT_VerkkopalveluKayttajahallinta.UserCallOutput usrCreationCall = KAYT_VerkkopalveluKayttajahallinta.userCreationCall(usrCallInput);
            System.assertEquals(usrCreationCall.Status, 'ERROR');   

        }

            static testMethod void UserNameChangeCallTest() {
            
            KAYT_VerkkopalveluKayttajahallinta.UserCallInput usrCallInput = new KAYT_VerkkopalveluKayttajahallinta.UserCallInput();
            List<KAYT_VerkkopalveluKayttajahallinta.UserCallInput> usrCallInputList = new List<KAYT_VerkkopalveluKayttajahallinta.UserCallInput>();

            usrCallInput.callType ='User_Name_Change_Call';
            usrCallInput.name = 'Mikko Mouse';  
            usrCallInput.userNewName ='Mikki Mouse';
            usrCallInput.requestType = 'EP';
            usrCallInput.contractNumber = '100010176';
            usrCallInput.locationNumber = '1122334455'; 
            
            Test.startTest();  
                
            Test.setMock(HttpCalloutMock.class, new KAYT_VerkkopalveluKayttajahallintaMock());
        
            Test.stopTest();

            usrCallInputList.add(usrCallInput);
            KAYT_VerkkopalveluKayttajahallinta.KAYT_IntegrationCall(usrCallInputList);
            
            System.assertEquals([SELECT COUNT() FROM Loki__c WHERE Toiminnallisuus__c = 'Wekara: userNameChangeCall'], 0);

        }

}