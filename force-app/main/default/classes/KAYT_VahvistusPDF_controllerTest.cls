/*This is test class for apex classes KAYT_MuutosvahvistusPDF_controller and KAYT_SopimusvahvistusPDF_controller */
@isTest(seeAllData=true)
private class KAYT_VahvistusPDF_controllerTest {

    static testMethod void getContractData() {
       
       // create account
       Account acc = new Account();
       acc.Name ='TestAccount';
       acc.Kieli__c ='Suomi';
       acc.Asiakasryhma__c ='Käyttäjä';
       acc.Luokittelu__c ='Avain';
       acc.Ohita_Y_tunnus_Hetu_validointi__c = true;
       acc.ShippingStreet = 'Testikatu 10';
       acc.ShippingCity = 'Helsinki';
       acc.ShippingPostalCode = '00100';
       insert acc;

       // get record type id    
       Id RecordTypeIdST = Schema.SObjectType.Sopimustieto__c.getRecordTypeInfosByName().get('Musiikinkäyttösopimus, sopimusvahvistus').getRecordTypeId(); 

       // create Sopimustieto
       Sopimustieto__c st = new Sopimustieto__c();
       st.recordtypeid=RecordTypeIdST;
       st.Alkupvm__c = System.Today();
       st.Asiakas_tekija__c = acc.Id;
       st.OnEri_kuin_postitusosoite__c = true;
       st.Laskutusosoite_Katu__c = 'Testikatu 1';
       st.Laskutusosoite_Kaupunki__c = 'Helsinki';
       st.Laskutusosoite_Postinumero__c = '00100';
       insert st;
       
       // create Tuoteosto
       Tuoteosto__c conData = new Tuoteosto__c();
       conData.Sopimustieto__c = st.Id;
       conData.Asiakas__c = acc.Id;
       insert conData;

       //  Run test
       test.startTest();
       ApexPages.StandardController sc_sv = new ApexPages.StandardController(st);
       KAYT_SopimusvahvistusPDF_controller contract_sv = new KAYT_SopimusvahvistusPDF_controller(sc_sv);

       ApexPages.StandardController sc_mv = new ApexPages.StandardController(st);
       KAYT_MuutosvahvistusPDF_controller contract_mv = new KAYT_MuutosvahvistusPDF_controller(sc_mv);

       ApexPages.StandardController sc_ly = new ApexPages.StandardController(st);
       KAYT_YhteenvetoPDF_controller contract_ly = new KAYT_YhteenvetoPDF_controller(sc_ly);

       pageReference pageRef = Page.KAYT_VahvistusPDF_hintalaskenta;
       pageRef.getParameters().put('id',st.id);
       ApexPages.StandardController sc_hl = new ApexPages.StandardController(st);
       KAYT_VahvistusPDF_hintalaskenta contract_hl = new KAYT_VahvistusPDF_hintalaskenta(sc_hl);
       System.assertNotEquals(null, contract_hl.fetchPriceRows());

    }
}