public class KAYT_HintahakuJSON {
    
    public String id {get;set;}   
    public String code {get;set;} 
    public String name {get;set;} 
    public String startDate {get;set;} 
    public String endDate {get;set;}   
    public List<PriceRow> priceRows {get;set;} 
    public String message {get;set;}   
    
    public KAYT_HintahakuJSON() {
        priceRows = new List<PriceRow>();
    }
    
    public class PriceRow {
        public String id {get;set;}
        public String name {get;set;}
        public String account {get;set;}
        public String type {get;set;}
        public String reason {get;set;}
        public String title {get;set;}
        public String unit {get;set;}
        public String value {get;set;}
        
    }
}