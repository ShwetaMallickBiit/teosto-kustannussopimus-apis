/*
Description
            Class makes request (REST) to tax information API to Tepa and updates the values to Verotieto__c object. 
            Class is called from Lightning Component (e.g. when user navigates to record)
*/

global with sharing class OIKOM_VerotiedotCall {

    Verotieto__c rec {get; set;}
    String recId {get; set;}
    
    public OIKOM_VerotiedotCall(ApexPages.StandardController stdCtrl) 
    {
        this.rec = (Verotieto__c)stdCtrl.getRecord(); 
        recId = rec.Id;
    }
    
    public PageReference getData() {
        
        updateVerotiedotData(new List<String>{this.rec.Id});
        PageReference pg = new PageReference('/'+this.rec.Id);
        pg.setRedirect(true); 
        return pg;
    }
    
    @RemoteAction
    global static void updateVerotiedot(String idVal) {
        updateVerotiedotData(new List<String>{idVal});
    }
    
    @AuraEnabled
    global static Boolean updateRecord(String idVal) {
        updateVerotiedotData(new List<String>{idVal});
        return true;
    }

    static public void updateVerotiedotData(List<String> ids) {

    String asnro = '';
    String verotietoId = '';
    try{
        List<Verotieto__c> vs = [SELECT Id, 
                                        x_Asiakasnumero_Tepa_formula__c,
                                        x_Vuosi_verokysely__c,
                                        Ansiotulo_brutto__c,
                                        Ansiotulo_vero_osuus__c,
                                        Lahdevero_brutto__c,
                                        Lahdevero_vero_osuus__c,
                                        Paaomatulo_brutto__c,
                                        Paaomatulo_vero_osuus__c,
                                        Veroton__c,
                                        Verotieto__c,
                                        Maakoodi_verokysely__c FROM Verotieto__c WHERE Id IN :ids];

        ExtranetAPI__c params = ExtranetAPI__c.getValues('params');

        for(Verotieto__c v : vs) {

            asnro = v.x_Asiakasnumero_Tepa_formula__c;
            verotietoId = v.Id;
            
            Http http1=new Http();
            HttpRequest req1=new HttpRequest();
            req1.setendpoint(params.endpointURL__c+'tepa/'+v.x_Asiakasnumero_Tepa_formula__c+'/tax/years/'+v.x_Vuosi_verokysely__c);
            req1.Setheader('Authorization','Bearer '+ params.Value__c);
            req1.setmethod('GET');
            HttpResponse res1;
            res1 = http1.send(req1);
            String str=res1.getbody();
            List<OIKOM_VerotietoJSON> rows = (List<OIKOM_VerotietoJSON>) JSON.deserialize(str, List<OIKOM_VerotietoJSON>.class);
            v.Ansiotulo_brutto__c = 0;
            v.Ansiotulo_vero_osuus__c = 0;
            v.Lahdevero_brutto__c = 0;
            v.Lahdevero_vero_osuus__c = 0;
            v.Paaomatulo_brutto__c = 0;
            v.Paaomatulo_vero_osuus__c = 0;
            v.Veroton__c = false;
            for(OIKOM_VerotietoJSON row : rows) {
                if(row.abrutto != null) v.Ansiotulo_brutto__c += row.abrutto;
                if(row.avero != null) v.Ansiotulo_vero_osuus__c += row.avero;
                if(row.lbrutto != null) v.Lahdevero_brutto__c += row.lbrutto;
                if(row.lbrutto != null) v.Lahdevero_vero_osuus__c += row.lbrutto;
                if(row.pbrutto != null) v.Paaomatulo_brutto__c += row.pbrutto;
                if(row.pvero != null) v.Paaomatulo_vero_osuus__c += row.pvero;
                if(row.veroton == 1) v.Veroton__c = true;
                if(row.verotieto != null) v.Verotieto__c = row.verotieto;
                if(row.maakoodi != null) v.Maakoodi_verokysely__c = row.maakoodi;
            }
            System.debug(rows);
        }
        update vs;
    } catch(Exception e) {
        System.debug(e.getMessage());
        Loki__c errorLog = new Loki__c(Objekti__c = 'Verotieto__c', Toiminnallisuus__c = 'OIKOM_VerotiedotCall', Tyyppi__c = 'Virhe', Virheviesti__c = asnro + ' ' + e.getMessage());
        insert errorLog;
    }
    }
}