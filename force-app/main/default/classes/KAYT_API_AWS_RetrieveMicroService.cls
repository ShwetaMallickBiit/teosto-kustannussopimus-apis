@RestResource(urlMapping='/restSalesforceAPIService/*')
global class KAYT_API_AWS_RetrieveMicroService {

@HttpPost
global static void postProcessor() {

    Set<String> jasenParams = new Set<String>();
    Set<String> ipiParams = new Set<String>();
    Set<String> agreementParams = new Set<String>();
    Set<String> allParams = new Set<String>();
    Set<String> invalidParams = new Set<String>();

    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;       
        
    KAYT_API_SalesforceRequest requestMessage = KAYT_API_AWS_MicroServiceHelper.deserializeRequestMsg(req);
    /* decides whether cases should be created */
    if(KAYT_API_AWS_MicroServiceHelper.createCases(requestMessage)) {
        res = Teosto_API_createCases.createCase(requestMessage.SalesforceRequest.setTapaus.tapaukset);
        return;
    }
    if(KAYT_API_AWS_MicroServiceHelper.createAgreements(requestMessage)) {
        res = Teosto_API_createAgreements.createAgreement(requestMessage.SalesforceRequest.setSopimus.sopimukset);
        return;
    }
    /* queries data from Salesforce and forms the response message with queried data */
    res = Teosto_API_getData.getData(requestMessage);
}

public class cException extends exception {}

}