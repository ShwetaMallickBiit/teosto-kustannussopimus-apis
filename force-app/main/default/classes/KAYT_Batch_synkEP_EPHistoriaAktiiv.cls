/*
Description
            Class synchronizes Asiakas and Esityspaikka fields on Esityspaikka object based on active Esityspaikkahistoria records
*/
global class    KAYT_Batch_synkEP_EPHistoriaAktiiv implements Database.stateful, Schedulable, Database.Batchable<sObject>{

    public Database.QueryLocator start(Database.BatchableContext bc) {

        // query fetches all Esityspaikkahistoria__c records that are active and parent Esityspaikka__c records does not match (Asiakas__c, Sopimus__c fields)
        String soql = '';
        soql = 'SELECT id, Asiakas__c, Esityspaikka__c, Alkupvm__c, Loppupvm__c, x_Aktiivinen__c, Sopimustieto__c ' +
               'FROM Esityspaikkahistoria__c ' +
               'WHERE x_Asiakas_Sopimusasetusajossa__c = true';
        return Database.getQueryLocator(soql);
    }

    public void execute(Database.BatchableContext bc, List<Esityspaikkahistoria__c> ephs) {

        Esityspaikka__c ep = new Esityspaikka__c();
        List<Esityspaikka__c> eps = new List<Esityspaikka__c>();
        for (Esityspaikkahistoria__c eph: ephs) {
            ep.Id = eph.Esityspaikka__c;
            ep.Sopimus__c = eph.Sopimustieto__c;
            ep.Asiakas__c = eph.Asiakas__c;
            eps.add(ep);
        }
        
        try {
            update eps;
        }
        catch(Exception e) {
            System.debug(e.getMessage());
            Loki__c errorLog = new Loki__c(Objekti__c = 'Esityspaikka__c / Esitypaikkahistoria__c', Toiminnallisuus__c = 'KAYT_Batch_SynkronoiEPAsiakasSopimus', Tyyppi__c = 'Virhe', Virheviesti__c = e.getMessage());
            insert errorLog;
        }

    }
    public void finish(Database.BatchableContext bc) {
        database.executeBatch(new KAYT_Batch_synkEP_EPHistoriaInaktiiv());
    }
    public void execute(SchedulableContext sc){
        Database.executeBatch(this,1);
    }
}