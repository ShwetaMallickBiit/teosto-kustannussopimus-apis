@isTest(SeeAllData=false)
public class KAYT_API_AWS_RetrieveMicroServiceTest {
    private static String accountId;
    
  

    @testSetup 
    static void GenerateData() {
        Account acc=new Account();
                    acc.name='Account used for Testing class';
                    acc.Etunimi__c = 'Account';
                    acc.Asiakasryhma__c = 'Tekijä';
                    acc.Ominaisuus__c = 'Säveltäjä';
                    acc.Kieli__c =  '   Suomi';
                    acc.Email__c = 'testaccount@test.com';
                    acc.Luokittelu__c = 'Perus';
                    acc.Asiakasnumero_Tepa__c = '123';
                    acc.Maksutapa__c = 'Sepa-maksu';
                    acc.Ohita_Y_tunnus_Hetu_validointi__c = true;
                  insert acc;
                  accountId = acc.id;
        
                system.debug('this is data on account ' + acc);
                
                Id jasenRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Jäsenjärjestö').getRecordTypeId();

                //create yhdistys linkage 
                Account yhdistys = new Account();
                    yhdistys.name='Bunny singer';
                    yhdistys.Asiakasryhma__c = 'Jäsenjärjestö';
                    yhdistys.Kieli__c =  'Suomi';
                    yhdistys.Asiakasryhma__c = 'Jäsenjärjestö';
                    yhdistys.Etunimi__c='test';
                    yhdistys.Asiakasnumero_Tepa__c = '111';
                    yhdistys.Ohita_Y_tunnus_Hetu_validointi__c = true;
                    yhdistys.recordtypeid =jasenRecordTypeId;
                 insert yhdistys;
        
               system.debug('this is data on yhdistys ' + yhdistys);
                               

                //create jasenyys__c
                 Jasenyys__c jasenyys = new Jasenyys__c(); 
                   jasenyys.Asiakas__c = acc.Id;
                   jasenyys.Yhdistys__c  = yhdistys.Id;                                      
                   jasenyys.Jasenmaksu_peritaan__c = true;
                   jasenyys.Alkupvm__c = date.parse(System.Today().format());
                   jasenyys.Loppupvm__c = date.parse(System.Today().format());
                 insert jasenyys;
        
               system.debug('this is data on jasenyys ' + jasenyys);
               
               IPI_numero__c ipi = new IPI_numero__c();
               ipi.name='test';
               ipi.Tyyppi__c='Different spelling';
               ipi.Asiakas__c=acc.Id;
               insert ipi;
       
        }
        
      @isTest 
      static void testServiceCall() {
   
       String inputRequest = '{"SalesforceRequest":{"getJasenyys":{"tekijanro":["123", "00"]},"getIPINumero":{"tekijanro":["123", "00"]}}}';
       String expectedResponse = '{"SalesforceResponse":{"Asiakkaat":{"asiakas":[{"tekijanro":"123","nimi":"Account used for Testing class","jasenyydet":[{"yhdistysnro":"111","yhdistysnimi":"Bunny singer","yhdistysid":"0010Q000009LH16QAG","tekijanro":null,"tekijaid":"0010Q000009LH15QAG","loppupvm":"2018-11-09","jasenmaksuperitaan":"true","id":"a0A0Q000000I2DgUAK","alkupvm":"2018-11-09"},{"yhdistysnro":"111","yhdistysnimi":"Bunny singer","yhdistysid":"0010Q000009LH18QAG","tekijanro":null,"tekijaid":"0010Q000009LH17QAG","loppupvm":"2018-11-09","jasenmaksuperitaan":"true","id":"a0A0Q000000I2DhUAK","alkupvm":"2018-11-09"},{"yhdistysnro":"111","yhdistysnimi":"Bunny singer","yhdistysid":"0010Q000009LH16QAG","tekijanro":null,"tekijaid":"0010Q000009LH15QAG","loppupvm":"2018-11-09","jasenmaksuperitaan":"true","id":"a0A0Q000000I2DgUAK","alkupvm":"2018-11-09"},{"yhdistysnro":"111","yhdistysnimi":"Bunny singer","yhdistysid":"0010Q000009LH18QAG","tekijanro":null,"tekijaid":"0010Q000009LH17QAG","loppupvm":"2018-11-09","jasenmaksuperitaan":"true","id":"a0A0Q000000I2DhUAK","alkupvm":"2018-11-09"}],"ipinumerot":[{"tyyppi":"Patronyymi","tekijanro":"123","tekijaid":"0010Q000009LH15QAG","sukunimi":"Account used for Testing class","selite":null,"ominaisuus":"Säveltäjä","name":"Account used for Testing class Account","ipinro":null,"id":"a0B0Q0000012VEZUA2","etunimi":"Account","aliasnro":null},{"tyyppi":"Different spelling","tekijanro":"123","tekijaid":"0010Q000009LH15QAG","sukunimi":null,"selite":null,"ominaisuus":null,"name":"test","ipinro":null,"id":"a0B0Q0000012VEaUAM","etunimi":null,"aliasnro":"1"},{"tyyppi":"Patronyymi","tekijanro":"123","tekijaid":"0010Q000009LH17QAG","sukunimi":"Account used for Testing class","selite":null,"ominaisuus":"Säveltäjä","name":"Account used for Testing class Account","ipinro":null,"id":"a0B0Q0000012VEbUAM","etunimi":"Account","aliasnro":null},{"tyyppi":"Different spelling","tekijanro":"123","tekijaid":"0010Q000009LH17QAG","sukunimi":null,"selite":null,"ominaisuus":null,"name":"test","ipinro":null,"id":"a0B0Q0000012VEcUAM","etunimi":null,"aliasnro":"1"},{"tyyppi":"Patronyymi","tekijanro":"123","tekijaid":"0010Q000009LH15QAG","sukunimi":"Account used for Testing class","selite":null,"ominaisuus":"Säveltäjä","name":"Account used for Testing class Account","ipinro":null,"id":"a0B0Q0000012VEZUA2","etunimi":"Account","aliasnro":null},{"tyyppi":"Different spelling","tekijanro":"123","tekijaid":"0010Q000009LH15QAG","sukunimi":null,"selite":null,"ominaisuus":null,"name":"test","ipinro":null,"id":"a0B0Q0000012VEaUAM","etunimi":null,"aliasnro":"1"},{"tyyppi":"Patronyymi","tekijanro":"123","tekijaid":"0010Q000009LH17QAG","sukunimi":"Account used for Testing class","selite":null,"ominaisuus":"Säveltäjä","name":"Account used for Testing class Account","ipinro":null,"id":"a0B0Q0000012VEbUAM","etunimi":"Account","aliasnro":null},{"tyyppi":"Different spelling","tekijanro":"123","tekijaid":"0010Q000009LH17QAG","sukunimi":null,"selite":null,"ominaisuus":null,"name":"test","ipinro":null,"id":"a0B0Q0000012VEcUAM","etunimi":null,"aliasnro":"1"}],"id":"0010Q000009LH15QAG","etunimi":"Account"},{"tekijanro":"123","nimi":"Account used for Testing class","jasenyydet":[{"yhdistysnro":"111","yhdistysnimi":"Bunny singer","yhdistysid":"0010Q000009LH16QAG","tekijanro":null,"tekijaid":"0010Q000009LH15QAG","loppupvm":"2018-11-09","jasenmaksuperitaan":"true","id":"a0A0Q000000I2DgUAK","alkupvm":"2018-11-09"},{"yhdistysnro":"111","yhdistysnimi":"Bunny singer","yhdistysid":"0010Q000009LH18QAG","tekijanro":null,"tekijaid":"0010Q000009LH17QAG","loppupvm":"2018-11-09","jasenmaksuperitaan":"true","id":"a0A0Q000000I2DhUAK","alkupvm":"2018-11-09"},{"yhdistysnro":"111","yhdistysnimi":"Bunny singer","yhdistysid":"0010Q000009LH16QAG","tekijanro":null,"tekijaid":"0010Q000009LH15QAG","loppupvm":"2018-11-09","jasenmaksuperitaan":"true","id":"a0A0Q000000I2DgUAK","alkupvm":"2018-11-09"},{"yhdistysnro":"111","yhdistysnimi":"Bunny singer","yhdistysid":"0010Q000009LH18QAG","tekijanro":null,"tekijaid":"0010Q000009LH17QAG","loppupvm":"2018-11-09","jasenmaksuperitaan":"true","id":"a0A0Q000000I2DhUAK","alkupvm":"2018-11-09"}],"ipinumerot":[{"tyyppi":"Patronyymi","tekijanro":"123","tekijaid":"0010Q000009LH15QAG","sukunimi":"Account used for Testing class","selite":null,"ominaisuus":"Säveltäjä","name":"Account used for Testing class Account","ipinro":null,"id":"a0B0Q0000012VEZUA2","etunimi":"Account","aliasnro":null},{"tyyppi":"Different spelling","tekijanro":"123","tekijaid":"0010Q000009LH15QAG","sukunimi":null,"selite":null,"ominaisuus":null,"name":"test","ipinro":null,"id":"a0B0Q0000012VEaUAM","etunimi":null,"aliasnro":"1"},{"tyyppi":"Patronyymi","tekijanro":"123","tekijaid":"0010Q000009LH17QAG","sukunimi":"Account used for Testing class","selite":null,"ominaisuus":"Säveltäjä","name":"Account used for Testing class Account","ipinro":null,"id":"a0B0Q0000012VEbUAM","etunimi":"Account","aliasnro":null},{"tyyppi":"Different spelling","tekijanro":"123","tekijaid":"0010Q000009LH17QAG","sukunimi":null,"selite":null,"ominaisuus":null,"name":"test","ipinro":null,"id":"a0B0Q0000012VEcUAM","etunimi":null,"aliasnro":"1"},{"tyyppi":"Patronyymi","tekijanro":"123","tekijaid":"0010Q000009LH15QAG","sukunimi":"Account used for Testing class","selite":null,"ominaisuus":"Säveltäjä","name":"Account used for Testing class Account","ipinro":null,"id":"a0B0Q0000012VEZUA2","etunimi":"Account","aliasnro":null},{"tyyppi":"Different spelling","tekijanro":"123","tekijaid":"0010Q000009LH15QAG","sukunimi":null,"selite":null,"ominaisuus":null,"name":"test","ipinro":null,"id":"a0B0Q0000012VEaUAM","etunimi":null,"aliasnro":"1"},{"tyyppi":"Patronyymi","tekijanro":"123","tekijaid":"0010Q000009LH17QAG","sukunimi":"Account used for Testing class","selite":null,"ominaisuus":"Säveltäjä","name":"Account used for Testing class Account","ipinro":null,"id":"a0B0Q0000012VEbUAM","etunimi":"Account","aliasnro":null},{"tyyppi":"Different spelling","tekijanro":"123","tekijaid":"0010Q000009LH17QAG","sukunimi":null,"selite":null,"ominaisuus":null,"name":"test","ipinro":null,"id":"a0B0Q0000012VEcUAM","etunimi":null,"aliasnro":"1"}],"id":"0010Q000009LH17QAG","etunimi":"Account"}]}}}';
       //String inputRequest = '{"SalesforceRequest": { "getJasenyys": { "tekijanro": [ "12345678910", "zzzz" ]}, "getIPINumero": { "tekijanro": [ "12345678910", "zzzz", ]}}}';
          
       Test.startTest();
       GenerateData();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/restSalesforceAPIService';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(inputRequest);

        RestContext.request = req;
        RestContext.response= res;
        
        KAYT_API_AWS_RetrieveMicroService.postProcessor();
        System.assertEquals(res.statusCode, 200);
        System.assert(true, (KAYT_API_SalesforceResponse)JSON.deserialize(res.responseBody.toString(),KAYT_API_SalesforceResponse.Class));

        Test.stopTest();
   
    }
      @isTest 
      static void testEmptyList() {
       
       String inputRequest='{"SalesforceRequest":{"getJasenyys":{"tekijanro":["1233232323"]},"getIPINumero":{"tekijanro":["1231111111111111111"]}}}';
       String expectedError = '{"SalesforceResponse":{"Asiakkaat":{"asiakas":[{"tekijanro":"1233232323","status":"ERROR","nimi":null,"jasenyydet":null,"ipinumerot":null,"id":null,"etunimi":null,"email":null,"description":"Does not exit in Salesforce"},{"tekijanro":"1231111111111111111","status":"ERROR","nimi":null,"jasenyydet":null,"ipinumerot":null,"id":null,"etunimi":null,"email":null,"description":"Does not exit in Salesforce"}]}}}';
       Test.startTest();
       GenerateData();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/restSalesforceAPIService';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(inputRequest);

        RestContext.request = req;
        RestContext.response= res;
        
        KAYT_API_AWS_RetrieveMicroService.postProcessor();
        System.assertEquals(res.statusCode, 200);
        String actualResponseErr = EncodingUtil.base64Decode(EncodingUtil.base64Encode(res.responseBody)).toString();
//        System.assertEquals(actualResponseErr, expectedError);

        Test.stopTest();
   
    }
 
 }