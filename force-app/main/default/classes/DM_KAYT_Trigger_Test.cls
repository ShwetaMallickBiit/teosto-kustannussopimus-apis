@isTest(seeAllData=true)
private class DM_KAYT_Trigger_Test {

    static testMethod void testDMBatches() {
       
       // create DM_Tasokas_asiakas__c
       DM_Tasokas_asiakas__c accDM = new DM_Tasokas_asiakas__c();
       accDM.AL__c ='Test';
       insert accDM;

       // create DM_Tasokas_yhthlo__c
       DM_Tasokas_yhthlo__c conDM = new DM_Tasokas_yhthlo__c();
       accDM.Asiakasnro__c ='Test';
       insert conDM;

       // create DM_Tasokas_tuoteosto__c 
       DM_Tasokas_tuoteosto__c toDM = new DM_Tasokas_tuoteosto__c();
       toDM.Asiakasnro__c ='Test';
       insert toDM;

       // create DM_Tasokas_esityspaikkasidos__c
       DM_Tasokas_esityspaikkasidos__c epsDM = new DM_Tasokas_esityspaikkasidos__c();
       epsDM.Asiakasnro__c ='Test';
       insert epsDM;

       // create DM_Tasokas_sidos__c
       DM_Tasokas_sidos__c sidDM = new DM_Tasokas_sidos__c();
       sidDM.Asiakasnro__c ='Test';
       insert sidDM;

       // create DM_Tasokas_sidos__c
       DM_Tasokas_kontakti__c konDM = new DM_Tasokas_kontakti__c();
       konDM.Asiakasnro__c ='Test';
       insert konDM;
       
       //  Run test for Asiakas
       Test.startTest();
       DM_KAYT_AsiakasImportTrigger accDMBatch = new DM_KAYT_AsiakasImportTrigger();
       ID batchprocessid1 = Database.executeBatch(accDMBatch);
       
       //  Run test for Yhthlo
       DM_KAYT_YhthloImportTrigger conDMBatch = new DM_KAYT_YhthloImportTrigger();
       ID batchprocessid2 = Database.executeBatch(conDMBatch);

       //  Run test for Tuoteosto
       DM_KAYT_TuoteostoImportTrigger toDMBatch = new DM_KAYT_TuoteostoImportTrigger();
       ID batchprocessid3 = Database.executeBatch(toDMBatch);

       //  Run test for Esityspaikkasidos
       DM_KAYT_EsityspaikkasidosImportTrigger epsDMBatch = new DM_KAYT_EsityspaikkasidosImportTrigger();
       ID batchprocessid4 = Database.executeBatch(epsDMBatch);

       //  Run test for Sidos
       DM_KAYT_SidosImportTrigger sidDMBatch = new DM_KAYT_SidosImportTrigger();
       ID batchprocessid5 = Database.executeBatch(sidDMBatch);

       //  Run test for Kontakti
       DM_KAYT_KontaktiImportTrigger konDMBatch = new DM_KAYT_KontaktiImportTrigger();
       ID batchprocessid6 = Database.executeBatch(konDMBatch);
       Test.stopTest();       

    }
}