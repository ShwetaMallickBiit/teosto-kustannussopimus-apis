public with sharing class GEN_Batch_synkYhthloEmailHandler{

    @future
    public static void paivitaYhteyshenkiloEmailAsync(Set<Id> ids, String toiminnallisuus) {
        List<Contact> cons = [SELECT Id, Email,
                              (SELECT Id, x_sahkoposti__c FROM AccountContactRelations WHERE x_Aktiivinen__c = TRUE)
                              FROM Contact WHERE Id IN :ids];
        paivitaYhteyshenkiloEmail(cons, toiminnallisuus);
    }

    public static void paivitaYhteyshenkiloEmail(List<Contact> cons, String toiminnallisuus) {

        List<Contact> consNew = new List<Contact>();
        Boolean emailOk = false;
        String acrEmail = '';
        Boolean accConRelationsExist = false;
        for (Contact con: cons) {
            Contact conNew = new Contact();
            acrEmail = '';
            emailOk = false;
            accConRelationsExist = false;
            
            for(AccountContactRelation acr : con.AccountContactRelations) {
                accConRelationsExist = true;
                if(con.Email == acr.x_sahkoposti__c) {
                    emailOk = true;
                    break;
                }
                else {
                    acrEmail = acr.x_sahkoposti__c;
                }
            }
            /* active relationships do not exist --> update contact email to null */
            if(!accConRelationsExist) {
                conNew.Id = con.Id;
                conNew.Email = '';
                consNew.add(conNew);            
            }
            /* contact having email that is not related to active relationship --> update contact email to active relationship email */
            else if(!emailOk) {
                conNew.Id = con.Id;
                conNew.Email = acrEmail;
                consNew.add(conNew);
            }
        }
        
        try {
            update consNew;
        }
        catch(Exception e) {
            System.debug(e.getMessage());
            Loki__c errorLog = new Loki__c(Objekti__c = 'Contact', Toiminnallisuus__c = toiminnallisuus, Tyyppi__c = 'Virhe', Virheviesti__c = e.getMessage());
            insert errorLog;
        }

    }
}