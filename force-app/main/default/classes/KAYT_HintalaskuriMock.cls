/**
 */
@isTest
public class KAYT_HintalaskuriMock implements HttpCalloutMock{
    
 public HttpResponse respond(HTTPRequest req)
    {
        HttpResponse res = new HttpResponse();
        res.setStatusCode(200);        
        res.setHeader('Content-Type', 'application/json');
        
        res.setBody('{"id":"30000999","code":"PORKKII","name":"PORKKANA 2017 Kiinteä 01.01.2017-","startDate":"2017-01-01","endDate":"2017-12-31",'+
        '"priceRows":[{"id":"40009991","name":"PORKKANA Kiinteä 901-1000 XXS 10-90","account":"4999","type":"KIINTEÄ","reason":"/tapahtuma","title":"Hinta","unit":"EUR","value":"99.00000"},'+
        '{"id":"40009992","name":"PORKKANA Kiinteä XXS 10-90","account":"4999","type":"PROSENTTI","reason":"lipputuloista","title":"Jos lipunhinta on vähintään 99 euroa","unit":"%","value":"9.990000"},'+
        '{"id":"40009993","name":"PORKKANA Kiinteä Something","account":"4999","type":"ALENNUS","reason":"hvz","title":"Vuosialennus","unit":"%","value":"9.990000"},'+
        '{"id":"40009994","name":"PORKKANA Kiinteä Something","account":"4999","type":"ALENNUS","reason":"hvz","title":"Vuosialennus","unit":"%","value":"9.990000"},'+
        '{"id":"40009995","name":"PORKKANA Kiinteä Vuosialennus","account":"4999","type":"ALENNUS","reason":"who knows","title":"Vuosialennus","unit":"%","value":"9.990000"}],'+
        '"message":"Porkkana Kiinteä hinnasto 2018"}');
                              
        return res;
    }   
    
}