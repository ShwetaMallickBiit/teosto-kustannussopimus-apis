public with sharing class SELFADMIN_REST_Client {

    public static final SelfAdminRestApi__mdt getConfig = [SELECT method__c, bearer__c, url__c, path__c FROM SelfAdminRestApi__mdt WHERE Label = 'FetchNotifications'];
    public static final SelfAdminRestApi__mdt putNotificationStatusConfig = [SELECT method__c, bearer__c, url__c, path__c FROM SelfAdminRestApi__mdt WHERE Label = 'UpdateNotificationStatus'];
    public static final SelfAdminRestApi__mdt putEmailSendConfig = [SELECT method__c, bearer__c, url__c, path__c FROM SelfAdminRestApi__mdt WHERE Label = 'UpdateEmailSendStatus'];

    public static List<SELFADMIN_Data.SelfAdministrationNotification> fetchDataByCustomerNumber(Integer customerNumber, String customerType) {
        String url = getConfig.url__c + getConfig.path__c + '?customerType=' + customerType + '&customerNumber=' + string.valueOf(customerNumber);
        HttpResponse response = doHTTPRequest(getConfig.method__c, url, 'application/json;charset=UTF-8', getConfig.bearer__c, null);
        if(response.getStatusCode() != 200) {
            throw new RemoteServiceException('Remote service returned status code was ' + response.getStatusCode() + '.');
        }
        String resp = response.getBody();
        System.debug('REST response: ' + resp);
        return (List<SELFADMIN_Data.SelfAdministrationNotification>) JSON.deserialize(resp, List<SELFADMIN_Data.SelfAdministrationNotification>.class);
    }

    public static List<SELFADMIN_Data.SelfAdministrationNotification> fetchDataByTimeWindow(Date minDate, Date maxDate, String status) {
        String url = getConfig.url__c + getConfig.path__c + '/changes?minDate=' +  + formatDate(minDate) + '&maxDate=' + formatDate(maxDate);
        HttpResponse response = doHTTPRequest(getConfig.method__c, url, 'application/json;charset=UTF-8', getConfig.bearer__c, null);
        if(response.getStatusCode() != 200) {
            throw new RemoteServiceException('Remote service returned status code was ' + response.getStatusCode() + '.');
        }
        String resp = response.getBody();
        System.debug('REST response: ' + resp);
        return (List<SELFADMIN_Data.SelfAdministrationNotification>) JSON.deserialize(resp, List<SELFADMIN_Data.SelfAdministrationNotification>.class);
    }

    // return true is successful
    public static boolean updateNotificationStatus(Integer notificationId, String notificationStatus) {
        Boolean success = true;
        if (notificationId == null) {
            System.debug('Unable to send notification status update because Notification Id is null.');
            return false;
        }
        if (!putNotificationStatusConfig.path__c.contains('--ID--')) {
            System.debug('Unable to send notification status update because path in Custom Metadata does not contain string --ID--.');
            return false;
        }
        String path = putNotificationStatusConfig.path__c.replace('--ID--', String.valueOf(notificationId));
        HttpResponse response = doHTTPRequest(putNotificationStatusConfig.method__c, putNotificationStatusConfig.url__c + path,
                                              'application/json;charset=UTF-8',
                                              putNotificationStatusConfig.bearer__c,
                                              createNotificationStatusJson(CustomMetadataHandler.getItsehallinnoinninTilaKey(notificationStatus)));
        System.debug('Remote service returned status code was ' + response.getStatusCode() + '.');
        if(response.getStatusCode() != 204) {
            success = false;
        }
        String resp = response.getBody();
        System.debug('REST response: ' + resp);
        return success;
    }

    // return true is successful
    public static boolean updateEmailSend(Integer notificationId, List<String> ipis) {
        Boolean success = true;
        if (ipis == null || ipis.isEmpty()) {
            return true;
        }
        if (!putEmailSendConfig.path__c.contains('--ID--')) {
            System.debug('Unable to send email send update because path in Custom Metadata does not contain string --ID--.');
            return false;
        }
        String path = putEmailSendConfig.path__c.replace('--ID--', String.valueOf(notificationId));
        HttpResponse response = doHTTPRequest(putEmailSendConfig.method__c, putEmailSendConfig.url__c + path,
                                              'application/json;charset=UTF-8',
                                              putEmailSendConfig.bearer__c,
                                              createNotificationEmailSendJson(ipis));
        System.debug('Remote service returned status code was ' + response.getStatusCode() + '.');
        if(response.getStatusCode() != 204) {
            success = false;
        }
        String resp = response.getBody();
        System.debug('REST response: ' + resp);
        return success;
    }

    // method is: 'GET', 'PUT' or 'POST'
    // ctHeader is, for instance: 'application/json;charset=UTF-8'
    private static HttpResponse doHTTPRequest(String method, String url, String ctHeader, String bearer, String body){
        System.Debug('Bearer ' + bearer);
        System.Debug('Sending HTTP ' + method + ' request to ' + url + ' with body: ' + body);
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setMethod(method);
        request.setHeader('Content-Type', ctHeader == null ? 'application/json;charset=UTF-8' : ctHeader);
        if (bearer != null) {
            request.setHeader('Authorization', 'Bearer ' + bearer);
        }
        if (body != null) {
            request.setBody(body);
            System.debug('Body: ' + body);
        }
        request.setEndpoint(url);
        return http.send(request);
    }

    public static String createNotificationStatusJson(String status) {
        return '{"status": "' + status + '"}';
    }

    public static String createNotificationEmailSendJson(List<String> ipis) {
        return '[' + String.join(ipis, ',') + ']';
    }

    public static String formatDate(Date d) {
        return formatDate(d, 'yyyy-MM-dd');
    }

    public static String formatDate(Date d, String format) {
        return DateTime.newInstance(d.year(), d.month(), d.day()).format(format);
    }

    public class RemoteServiceException extends Exception {

    }

}