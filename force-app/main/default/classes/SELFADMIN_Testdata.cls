public class SELFADMIN_Testdata  { 
    public static final String  MOCK_SUCCESS_GET_IHS_BY_TIME_WINDOW = '[' +      
    '{' +   
        '"notificationId": 100100,' +
        '"customerType": "TE",' +
        '"customerNumber": 34017,' +
        '"notificationType": "TE",' +
        '"status": "VA",' +
        '"terminationStatus": "EA",' +
        '"title": "Asioita",' +
        '"startDate": "",' +
        '"endDate": "",' +
        '"additionalInformation": "Lisätietoa",' +
        '"workList": [' +
            '{' +
                '"workKey": 28529832,' +
                '"title": "Asioita"' +
            '}' +
        '],' +
        '"interestedPartyList": [' +
            '{' +
                '"ipiNameNumber": 595617899,' +
                '"customerNumber": 0,' +
                '"acceptanceRole": "WI",' +
                '"acceptanceStatus": "EH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Shoanagon",' +
                '"firstName": "Sei"' +
            '},' +
            '{' +
                '"ipiNameNumber": 39861256,' +
                '"customerNumber": 34017,' +
                '"acceptanceRole": "HA",' +
                '"acceptanceStatus": "HV",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Länsiö",' +
                '"firstName": "Tapani Kari"' +
            '},' +
            '{' +
                '"ipiNameNumber": 43087593,' +
                '"customerNumber": 37507,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Nieminen",' +
                '"firstName": "Kai Tapani"' +
            '}' +
        ']' +
    '},' +
    '{' +
        '"notificationId": 100101,' +
        '"customerType": "TE",' +
        '"customerNumber": 693,' +
        '"notificationType": "TE",' +
        '"status": "OH",' +
        '"terminationStatus": "EA",' +
        '"title": "Nalle maailmalla",' +
        '"startDate": "",' +
        '"endDate": "",' +
        '"additionalInformation": "Lisätietoa",' +
        '"workList": [' +
            '{' +
                '"workKey": 28512333,' +
                '"title": "Nalle maailmalla"' +
            '}' +
        '],' +
        '"interestedPartyList": [' +
            '{' +
                '"ipiNameNumber": 541646456,' +
                '"customerNumber": 693,' +
                '"acceptanceRole": "HA",' +
                '"acceptanceStatus": "HV",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Pokela",' +
                '"firstName": "Eeva-Leena Marjatta"' +
            '},' +
            '{' +
                '"ipiNameNumber": 272404877,' +
                '"customerNumber": 73389,' +
                '"acceptanceRole": "TI",' +
                '"acceptanceStatus": "EH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Kätkä",' +
                '"firstName": "Mari Katariina"' +
            '},' +
            '{' +
                '"ipiNameNumber": 254250391,' +
                '"customerNumber": 5391,' +
                '"acceptanceRole": "TI",' +
                '"acceptanceStatus": "EH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Sopanen",' +
                '"firstName": "Satu Marita"' +
            '},' +
            '{' +
                '"ipiNameNumber": 283862532,' +
                '"customerNumber": 59156,' +
                '"acceptanceRole": "TI",' +
                '"acceptanceStatus": "EH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Kallio",' +
                '"firstName": "Pasi Markus"' +
            '},' +
            '{' +
                '"ipiNameNumber": 56136096,' +
                '"customerNumber": 49882,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Perkiö",' +
                '"firstName": "Soili Mirjami"' +
            '},' +
            '{' +
                '"ipiNameNumber": 60404125,' +
                '"customerNumber": 13482,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Huovi",' +
                '"firstName": "Hannele Päivi Marja"' +
            '},' +
            '{' +
                '"ipiNameNumber": 32747587,' +
                '"customerNumber": 28159,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Werner Söderström Osakeyhtiö",' +
                '"firstName": ""' +
            '}' +
        ']' +
    '},' +
    '{' +
        '"notificationId": 100200,' +
        '"customerType": "TE",' +
        '"customerNumber": 14696,' +
        '"notificationType": "LI",' +
        '"status": "VA",' +
        '"terminationStatus": "EA",' +
        '"title": "Keskiviikko keikka",' +
        '"additionalInformation": "tässä jotakin sekalaista lisätietoa",' +
        '"startDate": "2019-10-02",' +
        '"endDate": "2019-10-02",' +
        '"workList": [' +
            '{' +
                '"workKey": 20109138,' +
                '"title": "Aikaa vielä on",' +
                '"status": "OH"' +
            '},' +
            '{' +
                '"workKey": 28655078,' +
                '"title": "1001 yötä",' +
                '"status": "OH"' +
            '}' +
        '],' +
        '"interestedPartyList": [' +
            '{' +
                '"ipiNameNumber": 255295942,' +
                '"customerNumber": 27524,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Warner Chappell Music Finland Oy",' +
                '"firstName": ""' +
            '},' +
            '{' +
                '"ipiNameNumber": 15741993,' +
                '"customerNumber": 14696,' +
                '"acceptanceRole": "HA",' +
                '"acceptanceStatus": "HV",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Kärki",' +
                '"firstName": "Toivo Pietari Johannes"' +
            '},' +
            '{' +
                '"ipiNameNumber": 55519963,' +
                '"customerNumber": 22715,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Länsi",' +
                '"firstName": "Leo"' +
            '},' +
            '{' +
                '"ipiNameNumber": 255295942,' +
                '"customerNumber": 27524,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Warner Chappell Music Finland Oy",' +
                '"firstName": ""' +
            '},' +
            '{' +
                '"ipiNameNumber": 72448277,' +
                '"customerNumber": 22715,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Salmi",' +
                '"firstName": "Vexi"' +
            '},' +
            '{' +
                '"ipiNameNumber": 15741993,' +
                '"customerNumber": 14696,' +
                '"acceptanceRole": "HA",' +
                '"acceptanceStatus": "HV",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Kärki",' +
                '"firstName": "Toivo Pietari Johannes"' +
            '}' +
        '],' +
        '"eventInformation": {' +
            '"eventNumber": 1627662,' +
            '"eventDate": "2019-10-02",' +
            '"venueNumber": 11809962,' +
            '"organizerNumber": 70002812' +
        '},' +
        '"emailNotificationList": [' +
            '{' +
                '"email": "no.no@ouu.com.invalid",' +
                '"order": 1' +
            '},' +
            '{' +
                '"email": "moretests@testing.com",' +
                '"order": 2' +
            '}' +
        ']' +
    '},' +
    '{' +
        '"notificationId": 100201,' +
        '"customerType": "TE",' +
        '"customerNumber": 693,' +
        '"notificationType": "LI",' +
//        '"status": "OH",' +
        '"status": "VA",' +
        '"terminationStatus": "EA",' +
        '"title": "Keskiviikko keikka",' +
        '"additionalInformation": "tässä jotakin sekalaista lisätietoa",' +
        '"startDate": "2019-11-15",' +
        '"endDate": "2019-11-15",' +
        '"workList": [' +
            '{' +
                '"workKey": 27967554,' +
                '"title": "Ruoho huojuu tuulessa",' +
                '"status": "OH"' +
            '},' +
            '{' +
                '"title": "Suomen presidentit",' +
                '"workKey": 36578870,' +
                '"status": "OH"' +
            '},' +
            '{' +
                '"workKey": 28512333,' +
                '"title": "Nalle maailmalla",' +
                '"status": "OH"' +
            '},' +
            '{' +
                '"workKey": 28495970,' +
                '"title": "Velho tanssii taikameren navalla",' +
                '"status": "OH"' +
            '}' +
        '],' +
        '"interestedPartyList": [' +
            '{' +
                '"ipiNameNumber": 291084367,' +
                '"customerNumber": 0,' +
                '"acceptanceRole": "WI",' +
                '"acceptanceStatus": "EH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Makkonen (teosto)",' +
                '"firstName": "Christina"' +
            '},' +
            '{' +
                '"ipiNameNumber": 541646456,' +
                '"customerNumber": 693,' +
                '"acceptanceRole": "HA",' +
                '"acceptanceStatus": "HV",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Pokela",' +
                '"firstName": "Eeva-Leena Marjatta"' +
            '},' +
            '{' +
                '"ipiNameNumber": 291084073,' +
                '"customerNumber": 62151,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Laulajainen",' +
                '"firstName": "Leena Salme Tuulikki"' +
            '},' +
            '{' +
                '"ipiNameNumber": 255295942,' +
                '"customerNumber": 27524,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Warner Chappell Music Finland Oy",' +
                '"firstName": ""' +
            '},' +
            '{' +
                '"ipiNameNumber": 212244224,' +
                '"customerNumber": 50628,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Nättinen",' +
                '"firstName": "Pekka Tapio"' +
            '},' +
            '{' +
                '"ipiNameNumber": 420026421,' +
                '"customerNumber": 6149,' +
                '"acceptanceRole": "TI",' +
                '"acceptanceStatus": "EH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Nurmi",' +
                '"firstName": "Tomi Petteri"' +
            '},' +
            '{' +
                '"ipiNameNumber": 723220782,' +
                '"customerNumber": 76067,' +
                '"acceptanceRole": "TI",' +
                '"acceptanceStatus": "EH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Törmänen",' +
                '"firstName": "Pekka Olavi"' +
            '},' +
            '{' +
                '"ipiNameNumber": 541646456,' +
                '"customerNumber": 693,' +
                '"acceptanceRole": "HA",' +
                '"acceptanceStatus": "HV",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Pokela",' +
                '"firstName": "Eeva-Leena Marjatta"' +
            '},' +
            '{' +
                '"ipiNameNumber": 541646456,' +
                '"customerNumber": 693,' +
                '"acceptanceRole": "HA",' +
                '"acceptanceStatus": "HV",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Pokela",' +
                '"firstName": "Eeva-Leena Marjatta"' +
            '},' +
            '{' +
                '"ipiNameNumber": 272404877,' +
                '"customerNumber": 73389,' +
                '"acceptanceRole": "TI",' +
                '"acceptanceStatus": "EH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Kätkä",' +
                '"firstName": "Mari Katariina"' +
            '},' +
            '{' +
                '"ipiNameNumber": 254250391,' +
                '"customerNumber": 5391,' +
                '"acceptanceRole": "TI",' +
                '"acceptanceStatus": "EH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Sopanen",' +
                '"firstName": "Satu Marita"' +
            '},' +
            '{' +
                '"ipiNameNumber": 283862532,' +
                '"customerNumber": 59156,' +
                '"acceptanceRole": "TI",' +
                '"acceptanceStatus": "EH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Kallio",' +
                '"firstName": "Pasi Markus"' +
            '},' +
            '{' +
                '"ipiNameNumber": 56136096,' +
                '"customerNumber": 49882,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Perkiö",' +
                '"firstName": "Soili Mirjami"' +
            '},' +
            '{' +
                '"ipiNameNumber": 60404125,' +
                '"customerNumber": 13482,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Huovi",' +
                '"firstName": "Hannele Päivi Marja"' +
            '},' +
            '{' +
                '"ipiNameNumber": 32747587,' +
                '"customerNumber": 28159,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Werner Söderström Osakeyhtiö",' +
                '"firstName": ""' +
            '},' +
            '{' +
                '"ipiNameNumber": 255295942,' +
                '"customerNumber": 27524,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Warner Chappell Music Finland Oy",' +
                '"firstName": ""' +
            '},' +
            '{' +
                '"ipiNameNumber": 139348750,' +
                '"customerNumber": 693,' +
                '"acceptanceRole": "HA",' +
                '"acceptanceStatus": "HV",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Sariola",' +
                '"firstName": "Eeva-Leena Marjatta"' +
            '},' +
            '{' +
                '"ipiNameNumber": 291084073,' +
                '"customerNumber": 62151,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Laulajainen",' +
                '"firstName": "Leena Salme Tuulikki"' +
            '}' +
        '],' +
        '"eventInformation": {' +
            '"eventNumber": 1627662,' +
            '"eventDate": "2019-11-15",' +
            '"venueNumber": 11809962,' +
            '"organizerNumber": 70002812' +
        '},' +
        '"emailNotificationList": [' +
            '{' +
                '"email": "no.no@ouu.com.invalid",' +
                '"order": 1' +
            '},' +
            '{' +
                '"email": "moretests@testing.com",' +
                '"order": 2' +
            '}' +
        ']' +
    '},' +
    '{' +
        '"notificationId": 100301,' +
        '"customerType": "TE",' +
        '"customerNumber": 693,' +
        '"notificationType": "AV",' +
        '"status": "VA",' +
//        '"status": "OH",' +
        '"terminationStatus": "EA",' +
        '"title": "AV-tuotannon nimi",' +
        '"startDate": "2019-11-25",' +
        '"endDate": "2020-12-31",' +
        '"additionalInformation": "Lisätietoa",' +
        '"workList": [' +
            '{' +
                '"workKey": 27967554,' +
                '"title": "Ruoho huojuu tuulessa",' +
                '"status": "OH"' +
            '},' +
            '{' +
                '"title": "Suomen presidentit",' +
                '"workKey": 36578870,' +
                '"status": "OH"' +
            '},' +
            '{' +
                '"workKey": 28512333,' +
                '"title": "Nalle maailmalla",' +
                '"status": "OH"' +
            '},' +
            '{' +
                '"workKey": 28495970,' +
                '"title": "Velho tanssii taikameren navalla",' +
                '"status": "OH"' +
            '}' +
        '],' +
        '"interestedPartyList": [' +
            '{' +
                '"ipiNameNumber": 291084367,' +
                '"customerNumber": 0,' +
                '"acceptanceRole": "WI",' +
                '"acceptanceStatus": "EH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Makkonen (teosto)",' +
                '"firstName": "Christina"' +
            '},' +
            '{' +
                '"ipiNameNumber": 541646456,' +
                '"customerNumber": 693,' +
                '"acceptanceRole": "HA",' +
                '"acceptanceStatus": "HV",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Pokela",' +
                '"firstName": "Eeva-Leena Marjatta"' +
            '},' +
            '{' +
                '"ipiNameNumber": 291084073,' +
                '"customerNumber": 62151,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Laulajainen",' +
                '"firstName": "Leena Salme Tuulikki"' +
            '},' +
            '{' +
                '"ipiNameNumber": 255295942,' +
                '"customerNumber": 27524,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Warner Chappell Music Finland Oy",' +
                '"firstName": ""' +
            '},' +
            '{' +
                '"ipiNameNumber": 212244224,' +
                '"customerNumber": 50628,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Nättinen",' +
                '"firstName": "Pekka Tapio"' +
            '},' +
            '{' +
                '"ipiNameNumber": 420026421,' +
                '"customerNumber": 6149,' +
                '"acceptanceRole": "TI",' +
                '"acceptanceStatus": "EH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Nurmi",' +
                '"firstName": "Tomi Petteri"' +
            '},' +
            '{' +
                '"ipiNameNumber": 723220782,' +
                '"customerNumber": 76067,' +
                '"acceptanceRole": "TI",' +
                '"acceptanceStatus": "EH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Törmänen",' +
                '"firstName": "Pekka Olavi"' +
            '},' +
            '{' +
                '"ipiNameNumber": 541646456,' +
                '"customerNumber": 693,' +
                '"acceptanceRole": "HA",' +
                '"acceptanceStatus": "HV",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Pokela",' +
                '"firstName": "Eeva-Leena Marjatta"' +
            '},' +
            '{' +
                '"ipiNameNumber": 541646456,' +
                '"customerNumber": 693,' +
                '"acceptanceRole": "HA",' +
                '"acceptanceStatus": "HV",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Pokela",' +
                '"firstName": "Eeva-Leena Marjatta"' +
            '},' +
            '{' +
                '"ipiNameNumber": 272404877,' +
                '"customerNumber": 73389,' +
                '"acceptanceRole": "TI",' +
                '"acceptanceStatus": "EH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Kätkä",' +
                '"firstName": "Mari Katariina"' +
            '},' +
            '{' +
                '"ipiNameNumber": 254250391,' +
                '"customerNumber": 5391,' +
                '"acceptanceRole": "TI",' +
                '"acceptanceStatus": "EH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Sopanen",' +
                '"firstName": "Satu Marita"' +
            '},' +
            '{' +
                '"ipiNameNumber": 283862532,' +
                '"customerNumber": 59156,' +
                '"acceptanceRole": "TI",' +
                '"acceptanceStatus": "EH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Kallio",' +
                '"firstName": "Pasi Markus"' +
            '},' +
            '{' +
                '"ipiNameNumber": 56136096,' +
                '"customerNumber": 49882,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Perkiö",' +
                '"firstName": "Soili Mirjami"' +
            '},' +
            '{' +
                '"ipiNameNumber": 60404125,' +
                '"customerNumber": 13482,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Huovi",' +
                '"firstName": "Hannele Päivi Marja"' +
            '},' +
            '{' +
                '"ipiNameNumber": 32747587,' +
                '"customerNumber": 28159,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Werner Söderström Osakeyhtiö",' +
                '"firstName": ""' +
            '},' +
            '{' +
                '"ipiNameNumber": 255295942,' +
                '"customerNumber": 27524,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Warner Chappell Music Finland Oy",' +
                '"firstName": ""' +
            '},' +
            '{' +
                '"ipiNameNumber": 139348750,' +
                '"customerNumber": 693,' +
                '"acceptanceRole": "HA",' +
                '"acceptanceStatus": "HV",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Sariola",' +
                '"firstName": "Eeva-Leena Marjatta"' +
            '},' +
            '{' +
                '"ipiNameNumber": 291084073,' +
                '"customerNumber": 62151,' +
                '"acceptanceRole": "HY",' +
                '"acceptanceStatus": "OH",' +
                '"acceptanceTimestamp": null,' +
                '"emailTimestamp": null,' +
                '"cancellationStatus": "EA",' +
                '"name": "Laulajainen",' +
                '"firstName": "Leena Salme Tuulikki"' +
            '}' +
        '],' +
        '"avProduction": {' +
            '"year": 2019,' +
            '"type": "TV",' +
            '"spotgateId": "1234",' +
            '"channel": "TV"' +
        '}' +
    '}' +
']';
}