@isTest
public with sharing class Teosto_API_createCasesTest {
    
    @isTest
    public static void Teosto_API_createCases_Test() {
       // String resString = '{"statusMessage":"Success","statusCode":"200","createdcases":[{"caseNumber":"TA-00261868","caseId":"a0t0Q000000MYUDQA4"},{"caseNumber":"TA-00261869","caseId":"a0t0Q000000MYUEQA4"},{"caseNumber":"TA-00261870","caseId":"a0t0Q000000MYUFQA4"}]}';
        String inString1 ='{"SalesforceRequest": { "setTapaus": { "tapaukset": [ { "tyyppi": "Tekijä- ja kustantaja-asiat", "aihe": "test Data from test class Tekijä- ja kustantaja-asiat ", "customerId": "123Hello321" }, { "type": "Esiintymisasiat-123", "subject": "test case without Customer", "customerId": "" }, { "type": "", "subject": "test case without Type field", "customerId": "123Hello321" } ] }, "getJasenyys": { "tekijanro": [ "123Hello321" ] }, "getIPINumero": { "tekijanro": [ "123Hello321" ] } } }';
        String accountId;
        RestResponse res = new RestResponse();
        Account acc=new Account();
                    acc.name='Account used for Testing class';
                    acc.Etunimi__c = 'Account';
                    acc.Asiakasryhma__c = 'Tekijä';
                    acc.Ominaisuus__c = 'Säveltäjä';
                    acc.Kieli__c =  '   Suomi';
                    acc.Email__c = 'testaccount@test.com';
                    acc.Luokittelu__c = 'Perus';
                    acc.Asiakasnumero_Tepa__c = '123Hello321';
                    acc.Maksutapa__c = 'Sepa-maksu';
                    acc.Ohita_Y_tunnus_Hetu_validointi__c = true;
                  insert acc;
                  accountId = acc.id;
        
                system.debug('this is data on account ' + acc);
        

        Test.startTest();
        res=callsetCases(inString1); // Scenario 1
        // System.assertEquals(res.statusCode, 200);
        System.debug(res.responseBody.toString());//(KAYT_API_SalesforceResponse)JSON.deserialize(res.responseBody.toString(),KAYT_API_SalesforceResponse.Class));
        // System.assert(true, (KAYT_API_SalesforceResponse)JSON.deserialize(res.responseBody.toString(),KAYT_API_SalesforceResponse.Class));
        String inString2 = '{"SalesforceRequest":{"setCases":{"cases":[]}}}';
        res=callsetCases(inString2) ;//Scenario 2;
        System.debug(res.responseBody.toString());
        Test.stopTest();
    }

    public static RestResponse callsetCases(String inString){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/restSalesforceAPIService';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(inString);

        RestContext.request = req;
        RestContext.response= res;
        KAYT_API_AWS_RetrieveMicroService.postProcessor();
        return res;
    }
}