public with sharing class SELFADMIN_Notification_Handler implements Database.AllowsCallouts {

    @future(callout=true)
    public static void executeFuture(Date minDate, Date maxDate, String status) {
        SELFADMIN_Notification_Controller.getAdministrationNotificationsForPerioid(minDate, maxDate, status);
    }
    public static Map<Id, AccountContactRelation> getEmailMap(List<sObject> authors) {
        Set<Id> accountIds = New Set<Id>();
        for(sObject rec : authors) {
            accountIds.add((Id)rec.get('Asiakas__c'));
        }
        List<Account> accs = [SELECT Id FROM Account WHERE Id IN :accountIds];
        Map<Id, AccountContactRelation> emailMap = TeostoUtilities.getAccountPrimaryEmail(accs, 'OIKOM');
        return emailMap;
    }
    public static String getOrganizationWideEmailAddressId(Itsehallinnointisahkoposti__mdt metadata) {
        return [SELECT Id 
                FROM OrgWideEmailAddress 
                WHERE Address = :metadata.LahettavaSahkopostiosoite__c][0].Id;
    }
    public static void sendEmailsIfNeeded(Itsehallinnointisahkoposti__mdt metadata, List<sObject> authors) {

        String template;
        Map<String, String> emailTemplateMap = getEmailTemplates();
        List<SahkopostiIlmoitus__c> sentAuthors = new List<SahkopostiIlmoitus__c>();
        List<SahkopostiIlmoitus__c> sentAdditionalEmails = new List<SahkopostiIlmoitus__c>();
        Map<Id, AccountContactRelation> emailMap = New Map<Id, AccountContactRelation>();
        String orgWideEmailAddrId = getOrganizationWideEmailAddressId(metadata);
        
        // all authors that have not yet been sent an email to
        if(!metadata.LahetaSahkopostiin__c) {
            emailMap = getEmailMap(authors);
            if (authors != null) {
                for (sObject so : authors) {
                    Itsehallinnoinnin_Teoksen_Tekija__c author = (Itsehallinnoinnin_Teoksen_Tekija__c)so;
                    AccountContactRelation acrEmail = emailMap.get(author.Asiakas__c);
                    template = getTemplateByContactLanguage(metadata, author.Asiakas__r.Kieli__c, emailTemplateMap);
                    // send email
                    sendEmail(template, acrEmail.x_Sahkoposti__c, author.Id, acrEmail.ContactId, orgWideEmailAddrId);
                    // create record to note that email was sent
                    SahkopostiIlmoitus__c si = new SahkopostiIlmoitus__c(Name = metadata.DeveloperName, Itsehallinnoinnin_Teoksen_Tekija__c=author.Id, Email__c=author.Asiakas__r.Email__c, Paiva__c=DateTime.now());
                    sentAuthors.add(si);
                }
                insert sentAuthors;
            }
        }
        else {
            // all additional emails that have not yet been sent an email to
            for (sObject so : authors) {
                AdditionalEmail__c ae = (AdditionalEmail__c)so;
                template = getTemplateByContactLanguage(metadata, 'English', emailTemplateMap); // language for additional emails is not available in the data coming through the API
                // send email
                sendEmail(template, ae.Email__c, ae.Id, '', orgWideEmailAddrId);
                // create record to note that email was sent
                SahkopostiIlmoitus__c si = new SahkopostiIlmoitus__c(Name = metadata.DeveloperName, AdditionalEmail__c=ae.Id, Email__c=ae.Email__c, Paiva__c=DateTime.now());
                sentAdditionalEmails.add(si);
            }
            insert sentAdditionalEmails;
        }
    }
    public static void sendEmail(String templateId, String emailAddress, String whatId, String whoId, String orgWideEmailAddrId) {
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setTemplateId(Id.valueOf(templateId));
        email.setWhatId(whatId);
        if(!string.isBlank(whoId)) email.setTargetObjectId(whoId);
        else email.setTargetObjectId('0030Y00000eLqSzQAK');
        email.setOrgWideEmailAddressId(orgWideEmailAddrId);
        system.debug('XXXXX + ' + whatId + ' ' + whoId + ' ' + emailAddress + ' ' + templateId);
        if (emailAddress != null && emailAddress != 'null') {
            email.setToAddresses(New List<String> {emailAddress});
            email.setTreatTargetObjectAsRecipient(False);
            if (!Test.isRunningTest()) {
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
            }
        }
    }
    public static String getTemplateByContactLanguage(Itsehallinnointisahkoposti__mdt metadata, String accountKieli, Map<String, String> emailTemplateMap) {
        switch on accountKieli {
            when 'Suomi' {return emailTemplateMap.get(metadata.Sahkopostilomake_FIN__c);}
            when 'Ruotsi' {return emailTemplateMap.get(metadata.Sahkopostilomake_SWE__c);}
            when 'Englanti' {return emailTemplateMap.get(metadata.Sahkopostilomake_EN__c);}
            when else {return emailTemplateMap.get(metadata.Sahkopostilomake_FIN__c);}
        }
    }
    public static Map<String, String> getEmailTemplates() {
        List<EmailTemplate> ets = [SELECT Id, Name, DeveloperName FROM EmailTemplate];
        Map<String, String> emailTemplateMap = New Map<String, String>();
        for(EmailTemplate et : ets) {
            emailTemplateMap.put(et.DeveloperName, et.Id);
        }
        return emailTemplateMap;
    }
}