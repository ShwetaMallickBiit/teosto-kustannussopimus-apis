@isTest
private class GEN_Email2TapausTest {

    static testMethod void GEN_Email2Tapaus_Test() 
    {
        List<User> users = [Select Id, Email from User where isActive = true limit 1];
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        string emailSubject = 'UNIT TEST GEN_Email2Tapaus'+string.valueof(Date.today());
        
        Messaging.Inboundemail.Binaryattachment binaryAttachment = new Messaging.Inboundemail.Binaryattachment();
        binaryAttachment.body = Blob.valueOf('UNIT TEST BINARY ATTACHMENT');
        binaryAttachment.fileName = 'A_UnitTest.gif';
        email.binaryAttachments = new List<Messaging.Inboundemail.Binaryattachment>{binaryAttachment};
        
        Messaging.Inboundemail.Textattachment textAttachment = new Messaging.Inboundemail.Textattachment();
        textAttachment.body = 'Unit Test XYZ ABC 123';
        textAttachment.fileName = 'B_UnitTest.txt';
        email.textAttachments = new List<Messaging.Inboundemail.Textattachment>{textAttachment};
        
        email.plainTextBody = 'Unit Test Body: ' + Date.today();
        email.fromAddress = users.get(0).Email;
        email.toAddresses = new List<String>{users.get(0).Email};
        email.ccAddresses = new List<String>{'test1@test.com','test2@test.com','test3@test.com' };
        email.subject = emailSubject;
        
        GEN_Email2Tapaus emailHandler = new GEN_Email2Tapaus();
        Messaging.InboundEmailResult testResult = emailHandler.handleInboundEmail(email, env);
        
        List<Tapaus__c> ticketLst = [SELECT Id FROM Tapaus__c WHERE Aihe__c =: emailSubject AND Lahettaja__c =: email.fromAddress];
        List<Viesti__c> viestiLst = [SELECT Id FROM Viesti__c WHERE Aihe__c =: emailSubject AND Lahettaja_text__c =: email.fromAddress];
        
        system.assert(testResult.success);
        system.assert(ticketLst.size() == 1);
        system.assert(viestiLst.size() == 1);
        
    }
    
    
    static testMethod void GEN_Email2Tapaus_TicketFound_Test() 
    {
        List<User> users = [Select Id, Email from User where isActive = true limit 1];
        Tapaus__c newTicket = new Tapaus__c();
        insert newTicket;
        Tapaus__c ticket = [Select Name,x_Inbound_Id__c from Tapaus__c where Id=: newTicket.Id limit 1];
        string emailSubject = ticket.x_Inbound_Id__c;
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        Messaging.Inboundemail.Binaryattachment binaryAttachment = new Messaging.Inboundemail.Binaryattachment();
        binaryAttachment.body = Blob.valueOf('UNIT TEST BINARY ATTACHMENT');
        binaryAttachment.fileName = 'A_UnitTest.gif';
        email.binaryAttachments = new List<Messaging.Inboundemail.Binaryattachment>{binaryAttachment};
        
        Messaging.Inboundemail.Textattachment textAttachment = new Messaging.Inboundemail.Textattachment();
        textAttachment.body = 'Unit Test XYZ ABC 123';
        textAttachment.fileName = 'B_UnitTest.txt';
        email.textAttachments = new List<Messaging.Inboundemail.Textattachment>{textAttachment};
        
        email.plainTextBody = 'Unit Test Body: ' + Date.today();
        email.fromAddress = users.get(0).Email;
        email.toAddresses = new List<String>{users.get(0).Email};
        email.ccAddresses = new List<String>{'test1@test.com','test2@test.com','test3@test.com' };
        email.subject = emailSubject;
        
        GEN_Email2Tapaus emailHandler = new GEN_Email2Tapaus();
        Messaging.InboundEmailResult testResult = emailHandler.handleInboundEmail(email, env);
        
        List<Tapaus__c> ticketLst = [SELECT Id FROM Tapaus__c WHERE x_Inbound_Id__c =: emailSubject];
        List<Viesti__c> viestiLst = [SELECT Id FROM Viesti__c WHERE Aihe__c =: emailSubject AND Lahettaja_text__c =: email.fromAddress];
        
        system.assert(testResult.success);
        system.assert(ticketLst.size() == 1);
        system.assert(viestiLst.size() == 1);
        
    }
    
    
    static testMethod void GEN_Email2Tapaus_ErrorLog_Test() 
    {
        List<User> users = [Select Id, Email from User where isActive = true limit 1];
        string emailSubject;
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        Messaging.Inboundemail.Binaryattachment binaryAttachment = new Messaging.Inboundemail.Binaryattachment();
        binaryAttachment.body = Blob.valueOf('UNIT TEST BINARY ATTACHMENT');
        binaryAttachment.fileName = 'A_UnitTest.gif';
        email.binaryAttachments = new List<Messaging.Inboundemail.Binaryattachment>{binaryAttachment};
        
        Messaging.Inboundemail.Textattachment textAttachment = new Messaging.Inboundemail.Textattachment();
        textAttachment.body = 'Unit Test XYZ ABC 123';
        textAttachment.fileName = 'B_UnitTest.txt';
        email.textAttachments = new List<Messaging.Inboundemail.Textattachment>{textAttachment};
        
        email.plainTextBody = 'Unit Test Body: ' + Date.today();
        email.fromAddress = users.get(0).Email;
        email.toAddresses = new List<String>{users.get(0).Email};
        email.ccAddresses = new List<String>{'test1@test.com','test2@test.com','test3@test.com' };
        email.subject = emailSubject;
        
        GEN_Email2Tapaus emailHandler = new GEN_Email2Tapaus();
        Messaging.InboundEmailResult testResult = emailHandler.handleInboundEmail(email, env);
        
        List<Loki__c> ticketLst = [SELECT Id FROM Loki__c WHERE Toiminnallisuus__c = 'GEN_Email2Tapaus'];
        List<Viesti__c> viestiLst = [SELECT Id FROM Viesti__c WHERE Aihe__c =: emailSubject AND Lahettaja_text__c =: email.fromAddress];
        
        system.assert(!testResult.success);
        
    }
}