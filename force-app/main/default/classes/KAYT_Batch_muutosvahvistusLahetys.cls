/*
Description
            Class sends generated muutosvahvistus pdf documents in email to "contacts"
*/
global class KAYT_Batch_muutosvahvistusLahetys implements Database.stateful, Schedulable, Database.Batchable<sObject>{

    public Database.QueryLocator start(Database.BatchableContext bc) {

        String soql;
        if(Test.isRunningTest()) {
            soql = 'SELECT Id, x_Muutosvahvistus_pdf_Id_Kayt__c, x_Kieli_sv__c, Sopimuksen_vastuuhenkilo_email__c, Sopimuksen_vastuuhenkilo_Id__c FROM Sopimustieto__c WHERE x_Muutosvahvistus_lahetysajossa_Kayt__c = TRUE AND Asiakas_tekija__r.Name = \'TestAccount\' LIMIT 1';
        }
        else {
            soql = 'SELECT Id, x_Muutosvahvistus_pdf_Id_Kayt__c, x_Kieli_sv__c, Sopimuksen_vastuuhenkilo_email__c, Sopimuksen_vastuuhenkilo_Id__c FROM Sopimustieto__c WHERE x_Muutosvahvistus_lahetysajossa_Kayt__c = TRUE LIMIT 10';
        }        
        return Database.getQueryLocator(soql);
    }

    public void execute(Database.BatchableContext bc, List<Sopimustieto__c> records) {
        for (Sopimustieto__c record : records) {
            Boolean limitsAllowSending = true;
            string status;
            try {
                Messaging.reserveSingleEmailCapacity(1001);
                if(limitsAllowSending) {
                    status = KAYT_emailLahetys.lahetaEmail(record.Id, 'KAYT_Muutosvahvistus_saatekirje', record.x_Kieli_sv__c, 
                                                           record.x_Muutosvahvistus_pdf_Id_Kayt__c, record.Sopimuksen_vastuuhenkilo_email__c, 
                                                           record.Sopimuksen_vastuuhenkilo_Id__c, TRUE);
                    /*update sopimustieto record*/
                    IF(status <> 'ERROR') {
                        Sopimustieto__c sopimustieto = new Sopimustieto__c();
                        sopimustieto.Id = record.Id;
                        sopimustieto.Muutosvahvistus_lahetyspaiva_dt__c = Datetime.Now();
                        Update sopimustieto;
                    }
                }
            } 
            catch(Exception e) {
                System.debug(e.getMessage());
                Loki__c errorLog = new Loki__c(Objekti__c = 'Sopimustieto__c', Toiminnallisuus__c = 'KAYT_Batch_muutosvahvistusLahetys, KAYT_Batch_PDFlahetys', Tyyppi__c = 'Virhe', Virheviesti__c = record.Id + ' ' + e.getMessage());
                insert errorLog;
            }
        }
    }
    public void finish(Database.BatchableContext bc) {
    
    }
    public void execute(SchedulableContext sc){
        Database.executeBatch(this,1);
    }
}