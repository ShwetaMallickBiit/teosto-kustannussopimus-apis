/*
Description
            Class synchronizes Contact Email with AccountContactRelation email
*/
global class GEN_Batch_synkYhthloEmailBatch implements Database.stateful, Schedulable, Database.Batchable<sObject>{

    public Database.QueryLocator start(Database.BatchableContext bc) {

        String soql = '';
        soql = 'SELECT Id, Email, (' +
               'SELECT Id, x_sahkoposti__c FROM AccountContactRelations WHERE x_Aktiivinen__c = TRUE) ' + 
               'FROM Contact';
        return Database.getQueryLocator(soql);
    }

    public void execute(Database.BatchableContext bc, List<Contact> cons) {
        GEN_Batch_synkYhthloEmailHandler.paivitaYhteyshenkiloEmail(cons, 'GEN_Batch_synkYhthloEmailBatch');
    }
    public void finish(Database.BatchableContext bc) {

    }
    public void execute(SchedulableContext sc){
        Database.executeBatch(this, 5);
    }
}