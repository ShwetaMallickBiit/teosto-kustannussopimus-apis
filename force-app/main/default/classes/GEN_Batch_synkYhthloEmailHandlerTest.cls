@isTest(seeAllData=false)
private class GEN_Batch_synkYhthloEmailHandlerTest{

    public static void createTestData() {
        Account acc1 = GEN_TestFactory.createAccount();
        Contact con1 = GEN_TestFactory.createContact(acc1.Id, 'Etunimi1', 'Sukunimi1');        
        Account acc2 = GEN_TestFactory.createAccount();
        Contact con2 = GEN_TestFactory.createContact(acc2.Id, 'Etunimi2', 'Sukunimi2');
        
        List<AccountContactRelation> accCon = [SELECT Id, Sidoksen_email__c 
                                               FROM AccountContactRelation];
        accCon[0].EndDate = date.today() + 1;
        accCon[1].StartDate = date.today() - 10;
        accCon[1].EndDate = date.today() - 1;
        update accCon;
    }

    static testMethod void testSuccess() {
        createTestData();
        Test.startTest();
        GEN_Batch_synkYhthloEmailBatch newBatch = new GEN_Batch_synkYhthloEmailBatch();
        ID batchprocessid1 = Database.executeBatch(newBatch);
        Test.stopTest();
    }
}