public with sharing class SELFADMIN_Notification_Controller {

    // status can be "OH", "VA", "HL", "PE", null
    public static void getAdministrationNotificationsForPerioid(Date minDate, Date maxDate, String status) {
        List<SELFADMIN_Data.SelfAdministrationNotification> notifications = SELFADMIN_REST_Client.fetchDataByTimeWindow(minDate, maxDate, status);
        generateSObjects(notifications);
    }

    public static void getAdministrationNotificationByCustomerNumber(Integer customerNumber, String customerType) {
        List<SELFADMIN_Data.SelfAdministrationNotification> notifications = SELFADMIN_REST_Client.fetchDataByCustomerNumber(customerNumber, customerType);
        generateSObjects(notifications);
    }

    // return true is successful
    public static boolean updateNotificationStatus(Itsehallinnointi__c notification) {
        return SELFADMIN_REST_Client.updateNotificationStatus(Integer.valueOf(notification.NotificationId__c), notification.status__c);
    }

    // return true is successful
    public static boolean updateEmailSend(Itsehallinnointi__c notification, List<String> ipis) { // SHOULD GET IPIS FROM DB
        //List<String> ipis = new List<String>();
        // SELECT ALL EmailNotification WHERE notification = notification.Id AND tekijä = notification->work->tekijä !!!!!!!!!!!!!
        return SELFADMIN_REST_Client.updateEmailSend(Integer.valueOf(notification.NotificationId__c), ipis);
    }

    // convert parsed IH json objects into sObjects and upsert into DB
    //public static void generateSObjects(List<SELFADMIN_Data.SelfAdministrationNotification> notifications, Id account, Id contract) {
    public static void generateSObjects(List<SELFADMIN_Data.SelfAdministrationNotification> notifications) {
        List<Itsehallinnointi__c> ihs = new List<Itsehallinnointi__c>();
        Map<Integer, List<ItsehallinnoinninTeos__c> > worksMap = new Map<Integer, List<ItsehallinnoinninTeos__c> >();
        Map<Integer, List<Itsehallinnoinnin_Teoksen_Tekija__c> > autMap = new Map<Integer, List<Itsehallinnoinnin_Teoksen_Tekija__c> >();
        Map<String, List<AdditionalEmail__c> > additionalEmailMap = new Map<String, List<AdditionalEmail__c> >();
        Map<Integer, Event> eventMap = new Map<Integer, Event>();
        List<String> ipiNumbers = new List<String>();
        List<String> customerNumbers = new List<String>();
        // sets to store ids so that duplicates can be ignored
        Set<String> ih_ids = new Set<String>();
        Set<String> work_ids = new Set<String>();
        Set<String> author_ids = new Set<String>();
        // loop and make recursively records
        for (SELFADMIN_Data.SelfAdministrationNotification notification : notifications) {
            if (ih_ids.contains(String.valueOf(notification.notificationId))) {
                continue; // ih with same id already processed, ignoring this duplicate
            }
            Itsehallinnointi__c ih = new Itsehallinnointi__c();
            ih.Tunniste__c = String.valueOf(notification.notificationId);
            ih.notificationId__c = notification.notificationId;
            //ih.Asiakas__c = account; this is done later in a loop
            //ih.Sopimustieto__c = contract; this is done later in a loop
            ih.customerType__c = notification.customerType;
            ih.customerNumber__c = notification.customerNumber;
            ih.notificationType__c = CustomMetadataHandler.getItsehallinnoinninTyyppiLabel(notification.notificationType);
            ih.status__c = CustomMetadataHandler.getItsehallinnoinninTilaLabel(notification.status);
            ih.Name = notification.title;
            ih.startDate__c = parseDate(notification.startDate);
            ih.endDate__c = parseDate(notification.endDate);
            ih.additionalInformation__c = notification.additionalInformation;
            if (notification.eventInformation != null) {
                Event e = generateEvent((SELFADMIN_Data.EventInformation)notification.eventInformation);
                if (e != null) {
                    eventMap.put(notification.notificationId, e);
                }
            }
            List<AdditionalEmail__c> additionalEmails = new List<AdditionalEmail__c>();
            if (notification.emailNotificationList != null) {
                for (SELFADMIN_Data.EmailNotification en : notification.emailNotificationList) {
                    additionalEmails.add(new AdditionalEmail__c(Name = en.email, Email__c = en.email));
                }
            }
            additionalEmailMap.put(ih.Tunniste__c, additionalEmails);
            List<ItsehallinnoinninTeos__c> worksForCurrentNotification = new List<ItsehallinnoinninTeos__c>();
            if (notification.workList != null) {
                for (SELFADMIN_Data.SelfAdministrationWork work : notification.workList) {
                    if (work_ids.contains(ih.Tunniste__c + '-' +  String.valueOf(work.workKey))) {
                        continue; // work with same id already processed, ignoring this duplicate
                    }
                    ItsehallinnoinninTeos__c teos = new ItsehallinnoinninTeos__c();
                    teos.Tunniste__c = ih.Tunniste__c + '-' +  String.valueOf(work.workKey);
                    teos.workKey__c = work.workKey;
                    teos.Name = work.title;
                    //teos.status__c = CustomMetadataHandler.getItsehallinnoinninTeoksenTilaLabel(work.status); // deprecated
                    //teos.statusTimestamp__c = parseTimeStamp(work.statusTimestamp); // deprecated
                    //teos.cancellationStatus__c = CustomMetadataHandler.getItsehallinnoinninTyonPeruutusTilaLabel(work.cancellationStatus); // deprecated
                    //teos.endDate__c = parseDate(work.endDate); // deprecated
                    worksForCurrentNotification.add(teos);
                    work_ids.add(ih.Tunniste__c + '-' +  String.valueOf(work.workKey));
                }
            }
            worksMap.put(notification.notificationId, worksForCurrentNotification);
            List<Itsehallinnoinnin_Teoksen_Tekija__c> autsForCurrentNotification = new List<Itsehallinnoinnin_Teoksen_Tekija__c>();
            if (notification.interestedPartyList != null) {
                for (SELFADMIN_Data.SelfAdministrationWorkInterestedParty aut : notification.interestedPartyList) {
                    if (author_ids.contains(ih.Tunniste__c + '-' +  String.valueOf(aut.ipiNameNumber))) {
                        continue; // work with same id already processed, ignoring this duplicate
                    }
                    Itsehallinnoinnin_Teoksen_Tekija__c tek = new Itsehallinnoinnin_Teoksen_Tekija__c();
                    tek.Tunniste__c = ih.Tunniste__c + '-' +  String.valueOf(aut.ipiNameNumber);
                    tek.ipiNameNumber__c = aut.ipiNameNumber;
                    if (aut.ipiNameNumber != null) {
                        ipiNumbers.add(String.valueOf(aut.ipiNameNumber));
                    }
                    tek.customerNumber__c = aut.customerNumber;
                    if (aut.customerNumber != null) {
                        customerNumbers.add(String.valueOf(aut.customerNumber));
                    }
                    tek.Name = aut.name;
                    tek.firstName__c = aut.firstName;
                    tek.acceptanceRole__c = CustomMetadataHandler.getItsehallinnoinninTekijanRooliLabel(aut.acceptanceRole);
                    tek.acceptanceStatus__c = CustomMetadataHandler.getHyvaksynnanTilaLabel(aut.acceptanceStatus);
                    tek.emailTimestamp__c = aut.emailTimestamp;
                    tek.cancellationStatus__c = CustomMetadataHandler.getItsehallinnoinninTekijanPeruutusTilaLabel(aut.cancellationStatus);
                    autsForCurrentNotification.add(tek);
                    author_ids.add(ih.Tunniste__c + '-' +  String.valueOf(aut.ipiNameNumber));
                }
            }
            autMap.put(notification.notificationId, autsForCurrentNotification);
            ihs.add(ih);
            ih_ids.add(String.valueOf(notification.notificationId));
        }
        Map<String, IPI_numero__c> ipiMap = getIpisFromDB(ipiNumbers);
        Map<String, Account> accountMap = getAccountsFromDB(customerNumbers);
        Map<String, Sopimustieto__c> contractMap = getContractsFromDB(accountMap);
        // Set master-detail relations by adding masters first to the database and then rolling down
        try {
            // set account and contract lookups for IHs
            for (Itsehallinnointi__c ih : ihs) {
                if (accountMap.containsKey(String.valueOf(ih.customerNumber__c))) {
                    ih.Asiakas__c = accountMap.get(String.valueOf(ih.customerNumber__c)).Id;
                }
                if (contractMap.containsKey(String.valueOf(ih.customerNumber__c))) {
                    ih.Sopimustieto__c = contractMap.get(String.valueOf(ih.customerNumber__c)).Id;
                }
            }
            // Upsert notifications using an external ID field
            upsert ihs Tunniste__c;
            // Add master-details to all works and authors, and lookups for AdditionalEmails
            List<ItsehallinnoinninTeos__c> works = new List<ItsehallinnoinninTeos__c>();
            List<AdditionalEmail__c> additionalEmails = new List<AdditionalEmail__c>();
            List<Itsehallinnoinnin_Teoksen_Tekija__c> auts = new List<Itsehallinnoinnin_Teoksen_Tekija__c>();
            for (Itsehallinnointi__c ih : ihs) {
                for (ItsehallinnoinninTeos__c work : worksMap.get(Integer.valueOf(ih.Tunniste__c))) {
                    work.Itsehallinnointi__c = ih.Id;
                    if (ih.customerNumber__c != null && contractMap.containsKey(String.valueOf(ih.customerNumber__c))) {
                        work.Sopimustieto__c = contractMap.get(String.valueOf(ih.customerNumber__c)).Id;
                    }
                    works.add(work);
                }
                for (AdditionalEmail__c ae : additionalEmailMap.get(ih.Tunniste__c)) {
                    ae.Itsehallinnointi__c = ih.Id;
                    ae.CompositeKey__c = ih.Id + '-' + ae.Email__c;
                    additionalEmails.add(ae);
                }
                for (Itsehallinnoinnin_Teoksen_Tekija__c tek : autMap.get(Integer.valueOf(ih.Tunniste__c))) {
//                    tek.Itsehallinnoinnin_Teos__c = work.Id;
                    tek.Itsehallinnointi__c = ih.Id;
                    if (tek.ipiNameNumber__c != null && ipiMap.containsKey(String.valueOf(tek.ipiNameNumber__c))) {
                        tek.IPI_numero__c = ipiMap.get(String.valueOf(tek.ipiNameNumber__c)).Id;
                    }
                    if (tek.customerNumber__c != null && accountMap.containsKey(String.valueOf(tek.customerNumber__c))) {
                        tek.Asiakas__c = accountMap.get(String.valueOf(tek.customerNumber__c)).Id;
                    }
                    auts.add(tek);
                }
            }
            // Upsert works using an external ID field
            upsert works Tunniste__c;
            // Upsert AdditionalEmails using the composite key of IH.ID + '-' + Email__c
            upsert additionalEmails CompositeKey__c;
            // Add master-details and lookups (to account and IPI-number) to all authors
            //for (ItsehallinnoinninTeos__c work : works) {
            //}
            // Upsert authors using an external ID field
            upsert auts Tunniste__c;
            // Upsert events
            List<Event> events = new List<Event>();
            for (Itsehallinnointi__c ih : ihs) {
                Event event = eventMap.get((Integer)ih.notificationId__c);
                if (event != null) {
                    //event.Itsehallinnointi__c = ih.Id; // UNCOMMENT WHEN FIELD MADE TO EVENT
                    events.add(event);
                }
            }
            //upsert events; // CHANGE AND UNCOMMENT
        } catch (DmlException e) {
            System.debug('Upserting Self Administration Notifications failed: ' + e.getMessage());
        }

    }

    // *** FOR TESTING, REMOVE THIS FUNCTION *** !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    public static void deleteAll() {
        List<Itsehallinnointi__c> ns = [SELECT Id FROM Itsehallinnointi__c];
        delete ns;
    }

    public static Event generateEvent(SELFADMIN_Data.EventInformation eventInfo) { // IMPLEMENT THIS !!!!!!!!!!!!!!!!!!!!!!!!!!!
        return new Event();
    }

    public static Map<String, IPI_numero__c> getIpisFromDB(List<String> ipiNumbers) {
        Map<String, IPI_numero__c> ret = new Map<String, IPI_numero__c>();
        try {
            List<IPI_numero__c> ipis = [SELECT Id, IPI_numero_tekstina__c FROM IPI_numero__c WHERE IPI_numero_tekstina__c in :ipiNumbers];
            if (ipis != null) {
                for (IPI_numero__c i : ipis) {
                    ret.put(i.IPI_numero_tekstina__c, i);
                }
            }
            return ret;
        } catch (Exception e) {
            return ret;
        }
    }

    public static Map<String, Account> getAccountsFromDB(List<String> customerNumbers) {
        Map<String, Account> ret = new Map<String, Account>();
        try {
            List<Account> accs = [SELECT Id, Asiakasnumero_Tepa__c FROM Account WHERE Asiakasnumero_Tepa__c in :customerNumbers];
            if (accs != null) {
                for (Account a : accs) {
                    ret.put(a.Asiakasnumero_Tepa__c, a);
                }
            }
            return ret;
        } catch (Exception e) {
            return ret;
        }
    }

    public static Map<String, Sopimustieto__c> getContractsFromDB(Map<String, Account> accountMap) {
        List<String> accIds = new List<String>();
        for (Account a : accountMap.values()) {
            accIds.add(a.Id);
        }
        Map<String, Sopimustieto__c> ret = new Map<String, Sopimustieto__c>();
        try {
            List<Sopimustieto__c> contrs = [SELECT Id, Asiakas_tekija__c, Asiakas_tekija__r.Asiakasnumero_Tepa__c FROM Sopimustieto__c WHERE Asiakas_tekija__c in :accIds];
            if (contrs != null) {
                for (Sopimustieto__c c : contrs) {
                    ret.put(c.Asiakas_tekija__r.Asiakasnumero_Tepa__c, c);
                }
            }
            return ret;
        } catch (Exception e) {
            return ret;
        }
    }

    public static Date parseDate(String dateString) {
        try {
            return Date.valueOf(dateString);
        } catch (Exception e) {
            return null;
        }
    }

    public static DateTime parseTimeStamp(String timeStamp) {
        try {
            return DateTime.valueOf(timeStamp);
        } catch (Exception e) {
            return null;
        }
    }

}