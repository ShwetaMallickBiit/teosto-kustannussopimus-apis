/*
Description: 
            Apex Class manages user rights to Teosto web portal. User rights are managed in application called Wekara. This class sends integration messages to Wekara for 
            following user management actions: 
                1) User Creation
                2) User status change (from active to passive / from passive to active)
                3) User name change
                4) User password change
            
            A few notifications below
                1) Invocable method is created so that Salesforce flows / process builders can call the class
                2) User name change call is triggered via Process Builder, and therefore, user name change is done using @future method
*/
global class KAYT_VerkkopalveluKayttajahallinta {   
    
    @InvocableMethod  
    public static List<UserCallOutput> KAYT_IntegrationCall(List<UserCallInput> usrInput)
    { 
        List<UserCallOutput> callOutput = new List<UserCallOutput>();
        UserCallOutput output = new UserCallOutput();

        string callType = usrInput[0].callType;
        KAYT_VerkkopalveluKayttajahallinta vkh = new KAYT_VerkkopalveluKayttajahallinta();
        //userCreationCall method
        if( callType == 'User_Creation_Call')
        { 
            // Create input for userCreationCall method
            UserCallInput uInput = new UserCallInput();            
            uInput = new UserCallInput();
            uInput.name = usrInput[0].name;
            uInput.customerNumber = usrInput[0].customerNumber;
            uInput.requestType = usrInput[0].requestType;
            uInput.customerType = usrInput[0].customerType;
            uInput.contractNumber = usrInput[0].contractNumber;
            uInput.locationNumber = usrInput[0].locationNumber;
            // call the userCreationCall method
            UserCallOutput ucOutput = userCreationCall(uInput);
            // Update output result
            output.Status = ucOutput.Status;
            output.errMsg = ucOutput.errMsg;
            output.username = ucOutput.username;
            output.password = ucOutput.password;
            output.customerNumber = ucOutput.customerNumber;
            callOutput.add(output);
        }
        else if( callType == 'User_Creation_Call_Async')
        { 
            userCreationCallAsync(usrInput[0].name, usrInput[0].firstname, usrInput[0].lastname, usrInput[0].email, usrInput[0].customerNumber, usrInput[0].requestType, usrInput[0].customerType, 
                                  usrInput[0].contractNumber, usrInput[0].locationNumber, usrInput[0].sopimusID, usrInput[0].sopYhthloID,
                                  usrInput[0].yhtHloID);
        }
        // userStatusCall method
        else if( callType == 'User_Status_Update_Call')
        { 
            // Create input for userStatusCall method
            UserStatusCallInput usInput = new UserStatusCallInput();
            usInput.userStatus = usrInput[0].userStatus;
            usInput.customerType = usrInput[0].customerType;
//            usInput.customerNumber = usrInput[0].customerNumber;
            usInput.customerNumber = integer.valueOf(usrInput[0].customerNumber);
            // call the userStatusCall method
            CallOutputStatus cos = vkh.userStatusCall(usInput);
            // Update output result
            output.Status = cos.Status;
            output.errMsg = cos.errMsg;
            callOutput.add(Output);
        }
        // userPasswordChangeCall method
        else if( callType == 'User_New_Password_Call')
        { 
            // Create input for userStatusCall method
            UserPasswordChangeCallInput usInput = new UserPasswordChangeCallInput();
            usInput.userNewPassword = generatePassword(10);
            usInput.customerType = usrInput[0].customerType;
            usInput.customerNumber = integer.valueOf(usrInput[0].customerNumber);
            // call the userStatusCall method
            CallOutputStatus cos = vkh.userPasswordChangeCall(usInput);
            // Update output result
            output.Status = cos.Status;
            output.errMsg = cos.errMsg;
            output.password = usInput.userNewPassword;
            callOutput.add(Output);
        }
        // userNameChangeCall method
        else if( callType == 'User_Name_Change_Call')
        {
            userNameChangeCall(usrInput[0].userNewName, usrInput[0].customerType, usrInput[0].customerNumber);
        }
        return callOutput;
    }
      
    public CallOutputStatus userStatusCall(UserStatusCallInput input)
    { 
        CallOutputStatus res = new CallOutputStatus();
        String Status ='ERROR';
        try{
            
            // Create request message
            ExtranetAPI__c params = ExtranetAPI__c.getValues('params');
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(params.endpointURL__c+'wekara/users/'+input.customerType+'/'+input.customerNumber+'/status');
            system.debug('URL='+params.endpointURL__c+'wekara/users/'+input.customerType+'/'+input.customerNumber+'/status');
            string header = 'Bearer ' +params.Value__c;
            request.setHeader('Content-Type', 'application/json;charset=UTF-8');
            request.setHeader('Authorization', header);     
            request.setMethod('PUT');   
            String jsonBody = '{"status":"'+input.userStatus+'"}';      
            request.setBody(jsonBody);
            system.debug('request='+request);
            HttpResponse response = http.send(request);
            // Parse response message
            if (response.getStatusCode() == 204) 
            {
                Status = 'SUCCESS';
            }
            else
                res.errMsg = response.getStatusCode()+': '+response.getStatus();
        }catch (Exception e) 
        {
            res.errMsg = e.getMessage();
        }
        finally
        {       
            res.Status = Status;
        }
        return res;
    }

    /* Name change to Wekara is triggered by Process Builder. Due to Salesforce restrictions, the integration must be called in Future-method */
    @future(callout=true)
    public static void userNameChangeCall(string userName, string customerType, string customerNumber)
    { 
        CallOutputStatus res = new CallOutputStatus();
        String Status ='ERROR';
        try{
            
            // Create request message
            ExtranetAPI__c params = ExtranetAPI__c.getValues('params');
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(params.endpointURL__c+'wekara/users/'+customerType+'/'+customerNumber+'/name');
            system.debug('URL='+params.endpointURL__c+'wekara/users/'+customerType+'/'+customerNumber+'/name');
            string header = 'Bearer ' +params.Value__c;
            request.setHeader('Content-Type', 'application/json;charset=UTF-8');
            request.setHeader('Authorization', header);     
            request.setMethod('PUT');   
            String jsonBody = '{"name":"'+userName+'"}';      
            request.setBody(jsonBody);
            system.debug('request='+request);
            HttpResponse response = http.send(request);
            // Parse response message
            if (response.getStatusCode() == 204) 
            {
                Status = 'SUCCESS';
            }
            else {
                Loki__c errorLog = new Loki__c(Objekti__c = 'Contact', Toiminnallisuus__c = 'Wekara: userNameChangeCall', Tyyppi__c = 'Virhe', Virheviesti__c = userName + ' ' + response.getStatusCode());
                insert errorLog; 
            }
        }catch (Exception e) 
        {
            System.debug(e.getMessage());
            Loki__c errorLog = new Loki__c(Objekti__c = 'Contact', Toiminnallisuus__c = 'Wekara: userNameChangeCall', Tyyppi__c = 'Virhe', Virheviesti__c = userName + ' ' + e.getMessage());
            insert errorLog;
        }
        finally
        {       
        }
    }

    public CallOutputStatus userPasswordChangeCall(UserPasswordChangeCallInput input)
    { 
        CallOutputStatus res = new CallOutputStatus();
        String Status ='ERROR';
        try{
            
            // Create request message
            ExtranetAPI__c params = ExtranetAPI__c.getValues('params');
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(params.endpointURL__c+'wekara/users/'+input.customerType+'/'+input.customerNumber+'/password');
            system.debug('URL='+params.endpointURL__c+'wekara/users/'+input.customerType+'/'+input.customerNumber+'/password');
            string header = 'Bearer ' +params.Value__c;
            request.setHeader('Content-Type', 'application/json;charset=UTF-8');
            request.setHeader('Authorization', header);     
            request.setMethod('PUT');   
            String jsonBody = '{"password":"'+input.userNewPassword+'"}';      
            request.setBody(jsonBody);
            system.debug('request='+request);
            HttpResponse response = http.send(request);
            // Parse response message
            if (response.getStatusCode() == 204) 
            {
                Status = 'SUCCESS';
            }
            else
                res.errMsg = response.getStatusCode()+': '+response.getStatus();
        }catch (Exception e) 
        {
            res.errMsg = e.getMessage();
        }
        finally
        {       
            res.Status = Status;
        }
        return res;
    }

    @future(callout=true)
    public static void userCreationCallAsync(string name, string firstname, string lastname, string email, string customerNumber, string requestType, string customerType, string contractNumber, 
                                             string locationNumber, string sopimusID, string sopYhthloID, string yhtHloID)
    {
        UserCallInput uInput = new UserCallInput();
        UserCallOutput output = new UserCallOutput();
        Map<String, Object> params = new Map<String, Object>();
        String status;
        String errMsg;

        try {
            uInput.name = name;
            uInput.customerNumber = customerNumber;
            uInput.requestType = requestType;
            uInput.customerType = customerType;
            uInput.contractNumber = contractNumber;
            uInput.locationNumber = locationNumber;
            /* Wekara-integration call*/
            UserCallOutput ucOutput = userCreationCall(uInput);

            if(ucOutput.Status == 'ERROR') {
                Loki__c errorLog = new Loki__c(Objekti__c = 'Sopimuksen_yhteyshenkilo__c', Toiminnallisuus__c = 'KAYT_VerkkopalveluKayttajahallinta, userCreationCallAsync', Tyyppi__c = 'Virhe', Virheviesti__c = sopYhthloID + ' ' + ucOutput.errMsg);
                insert errorLog;
            }                 
            else {
                /* activity creation, credentials update to sopimuksen_yhteyshenkilo__c and activity creation is managed in flow. */
                params.put('varWekaraAsiakasnumero', ucOutput.customerNumber);
                params.put('varKayttajatunnus', ucOutput.username);
                params.put('varYhteyshenkiloID', yhtHloID);
                params.put('varSopimustietoID', sopimusID);
                params.put('varEmail', email);
                params.put('varEtunimi', firstname);
                params.put('varSukunimi', lastname);
                params.put('varEsityspaikkaNro', '');
                params.put('varSopimusnumero', sopimusID);
                params.put('varSopYhthloID', sopYhthloID);
                params.put('varSalasana', ucOutput.password);
                Flow.Interview.KAYT_Wekara_Kayttajahallinta_tunnusluonti newFlow = new Flow.Interview.KAYT_Wekara_Kayttajahallinta_tunnusluonti(params);
                newFlow.start();
                status = (String) newFlow.getVariableValue('varTunnusluontiTila');
                errMsg = (String) newFlow.getVariableValue('varTunnusluontiMsg');
                if(status == 'error') {
                    Loki__c errorLog = new Loki__c(Objekti__c = 'Sopimuksen_yhteyshenkilo__c', Toiminnallisuus__c = 'KAYT_VerkkopalveluKayttajahallinta, userCreationCallAsync', Tyyppi__c = 'Virhe', Virheviesti__c = sopYhthloID + ' ' + errMsg);
                    insert errorLog;
                }            
            }
        }
        catch(Exception e) {
            e.getMessage();
            Loki__c errorLog = new Loki__c(Objekti__c = 'Sopimuksen_yhteyshenkilo__c', Toiminnallisuus__c = 'KAYT_VerkkopalveluKayttajahallinta, userCreationCallAsync', Tyyppi__c = 'Virhe', Virheviesti__c = sopYhthloID + ' ' + e.getMessage());
            insert errorLog;
        }

    }

    public static UserCallOutput userCreationCall(UserCallInput usrInput) 
    {   
        UserCallOutput userOutput = new UserCallOutput();
    
        String Status = 'ERROR';
        try{
            // Create request body message
            UserJSON usrJSON = new UserJSON();
            // Add webCustomerId substructure to request message
            WebCustomerId wcid = new WebCustomerId();
            wcid.customerType = usrInput.customerType;
//            wcid.customerNumber = '0';
            wcid.customerNumber = 0;
            usrJSON.webCustomerId = wcid;
            
            Credentials cred = new Credentials();
            usrJSON.credentials = cred;
            usrJSON.name = usrInput.name;
            usrJSON.status = 'active';
            usrJSON.iso639code = 'fi';
            // Add applicationPrivileges array structure to request message
            // The request structure always contains only one applicationPrivileges record even defined as array
            ApplicationPrivileges appPriv = new ApplicationPrivileges();
            appPriv.privilegeClass = 'TAPAHTUMA';
            List<ApplicationPrivileges> appPrvLst = new List<ApplicationPrivileges>();
            appPrvLst.add(appPriv);
            usrJSON.applicationPrivileges = appPrvLst;
            // Add dataPrivileges array structure to request message
            List<DataPrivileges> dPriv = new List<DataPrivileges>();
            DataPrivileges priv2 = new DataPrivileges();
            // Add webCustomerId substructure for first dataPrivilege record
            WebCustomerId wcid2 = new WebCustomerId();
            wcid2.customerType = 'MA';
//            wcid2.customerNumber = usrInput.contractNumber;
            wcid2.customerNumber = integer.valueOf(usrInput.contractNumber);
            priv2.webCustomerId = wcid2;
            priv2.privilegeClass = 'MA';
            if(usrInput.requestType <> 'EP')
                priv2.privilegeClass = 'TAPAHTUMA';
            
            priv2.privilegeLevel = 'UPD';
            
            if(usrInput.requestType == 'EP')
            {
                DataPrivileges priv1 = new DataPrivileges();
                // Add webCustomerId substructure for second dataPrivilege record
                WebCustomerId wcid1 = new WebCustomerId();
                wcid1.customerType = 'EP';
//                wcid1.customerNumber = usrInput.locationNumber;
                wcid1.customerNumber = integer.valueOf(usrInput.locationNumber);
                priv1.webCustomerId = wcid1;
                
                priv1.privilegeClass = 'TAPAHTUMA';
                priv1.privilegeLevel = 'UPD';
                dPriv.add(priv1);
                
                priv2.privilegeClass = 'MAOIKEUS';              
            }
            dPriv.add(priv2);
            usrJSON.dataPrivileges = dPriv;         
            // Create request message
            ExtranetAPI__c params = ExtranetAPI__c.getValues('params');
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(params.endpointURL__c+'wekara/users');
            string header = 'Bearer ' +params.Value__c;
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Authorization', header);     
            request.setMethod('POST');          
//            String jsonBody = JSON.serialize(usrJSON).replace('null', '" "').replace('"0"', '0').replace('"70132177"', '70132177'); //Mikko modified for debugging
            String jsonBody = JSON.serialize(usrJSON).replace('null', '" "');
            request.setBody(jsonBody);
            system.debug('jsonBody='+jsonBody);    
            HttpResponse response = http.send(request);
            // Parse response message
            if (response.getStatusCode() == 201) 
            {
                List<UserJSON> results = (List<UserJSON>) JSON.deserialize('['+response.getBody()+']',List<UserJSON>.class);
                for (UserJSON res: results)
                {
                    Status = 'SUCCESS';
                    UserOutput.username = res.credentials.username;
                    UserOutput.password = res.credentials.password;
//                    UserOutput.customerNumber = res.webCustomerId.customerNumber;
                    UserOutput.customerNumber = string.valueOf(res.webCustomerId.customerNumber);
                }   
            }
            else
                UserOutput.errMsg = response.getStatusCode()+': '+response.getStatus();
        }catch (Exception e) 
        {
            UserOutput.errMsg = e.getMessage();
        }
        finally
        {       
            UserOutput.Status = Status;
        }
                
        return userOutput;
    }        

    public static String generatePassword(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz123456789';
        String password = '';
        while (password.length() < len) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            password += chars.substring(idx, idx+1);
        }    
        return password; 
    }

    public class WebCustomerId
    {
        public String customerType;
//        public String customerNumber;
        public Integer customerNumber;
    }
    
    public class Credentials
    {
        public String username;
        public String password;
    }
    
    public class ApplicationPrivileges
    {
        public String privilegeClass;
    }
    
    public class DataPrivileges
    {
        public WebCustomerId webCustomerId;
        public String privilegeClass;
        public String privilegeLevel;
    }
    
    public class UserJSON
    {
        public WebCustomerId webCustomerId;
        public Credentials credentials;
        public String name;
        public String status;
        public String iso639code;
        public List<ApplicationPrivileges> applicationPrivileges;
        public List<DataPrivileges> dataPrivileges;
    }
    
    public class UserStatusCallInput
    {
        public String customerType;
//        public String customerNumber;
        public Integer customerNumber;
        public String userStatus;
    }

    public class UserPasswordChangeCallInput
    {
        public String customerType;
//        public String customerNumber;
        public Integer customerNumber;
        public String userNewPassword;
    }

    public class UserNameChangeCallInput
    {
        public String customerType;
//        public String customerNumber;
        public Integer customerNumber;
        public String userName;
    }
    
    global class UserCallInput
    {
                
        @InvocableVariable(required=true)
        public String callType;
        
        @InvocableVariable
        public String name;

        @InvocableVariable
        public String firstname;

        @InvocableVariable
        public String lastname;

        @InvocableVariable
        public String email;
        
        @InvocableVariable
        public String customerNumber;
        
        @InvocableVariable
        public String requestType;
        
        @InvocableVariable
        public String customerType;
        
        @InvocableVariable
        public String contractNumber;
        
        @InvocableVariable
        public String locationNumber;   
        
        @InvocableVariable
        public String userStatus;

        @InvocableVariable
        public String userNewPassword;

        @InvocableVariable
        public String userNewName;

        @InvocableVariable
        public String sopimusID;

        @InvocableVariable
        public String sopYhthloID;

        @InvocableVariable
        public String yhtHloID;

    }
    
    public class CallOutputStatus
    {
        public String Status;
        public String errMsg;
    }
    
    public class UserCallOutput
    {
        @InvocableVariable
        public String username;
        
        @InvocableVariable
        public String password;
        
        @InvocableVariable
        public String customerNumber;
    
        @InvocableVariable(required=true)
        public String Status;
        
        @InvocableVariable
        public String errMsg;
    }
}