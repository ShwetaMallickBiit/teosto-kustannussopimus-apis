/*
Description
            This class makes queries to identify records in error related to integration and
            identifies records that may have issues as polling has not picked those up (delays)
*/
public class GEN_MonitorointiBatch implements Schedulable, Database.Batchable<sObject> {

    /* limits to define how many id:s are displayed, what is hold limit and soql query limit */
    List<Arvolista__c> defs = [SELECT Arvo_1_Teksti__c, Arvo_1_numero_0_desim__c, Arvo_2_numero_0_desim__c, Arvo_3_numero_0_desim__c, Arvo_1_datetime__c FROM Arvolista__c WHERE Ryhma__c = 'NOTIFIKAATIO' LIMIT 1];

    integer detailLimitNr = defs[0].Arvo_1_numero_0_desim__c.intValue();                           // how many id:s are put into report
    integer hoursDelayLimit = defs[0].Arvo_2_numero_0_desim__c.intValue();                         // hold limit in hours
    integer queryLimit = defs[0].Arvo_3_numero_0_desim__c.intValue();                              // soql query limit
    dateTime querySD = defs[0].Arvo_1_datetime__c;                                      // soql monitor start date limit
    string emailSubject = Date.today().format() + defs[0].Arvo_1_teksti__c;

/*
    integer detailLimitNr = 10;                            // how many id:s are put into report
    integer hoursDelayLimit = -6;                          // hold limit in hours
    integer queryLimit = 100;                              // soql query limit
    dateTime querySD = DateTime.newInstance(2018, 03, 11); // soql monitor start date limit
    string emailSubject = Date.today().format() + ' Teosto Monitoring Issues Summary';
*/
    /* transformations of fields to different format */
    DateTime limitDT = datetime.now().addhours(hoursDelayLimit);
    string limitDT_string = limitDT.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');

    string querySD_string = querySD.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');

    public Database.QueryLocator start(Database.BatchableContext bc) {
        String soql = 'SELECT Arvo_1_Teksti__c FROM Arvolista__c WHERE Ryhma__c = \'NOTIFIKAATIOEMAIL\'';
        if (Test.isRunningTest()) soql += ' ORDER BY Id DESC LIMIT 1';
        
        return Database.getQueryLocator(soql); 
    }

    public List<String> queryIds(string objectName, string filter, string monitorMsg) {

        List<String> IDs = New List<String>();
        List<sObject> sfObject = New List<sObject>();
        String sfQuery = 'SELECT Id, CreatedDate, LastModifiedDate FROM ' + objectName + ' WHERE ' + filter;

        sfObject = Database.query(sfQuery);
        for(sObject record : sfObject) {
            Object dateC = record.get('CreatedDate');
            Object dateM = record.get('LastModifiedDate');
            
            if(dateC != dateM) {
                IDs.add(record.Id);
            }
        }

        return IDs;
        
    }
    
    public string defineValidation(string objectName, string filter, string monitorMsg) {
        string text = '';
        integer N;

        List<String> records = queryIDs(objectName, filter, monitorMsg);
        N = records.size();

        /* check if there are any errors */        
        if(N == 0) {
            return text;
        }

        /* add errors number */
        text += 'Number of ' + objectName + ' ' + monitorMSG + ': ' + N + '\n';
        text += '     ';

        /* add errors details if only limited amount of errors */
        if(N > detailLimitNr) {
            text += ' | over N errors, id:s not listed';
        } else {
            text += ' | Id:s: ';
            for(string rec : records) {
                text += rec + ', ';
            }
        }
        return text + '\n';
    }

    public string lokiValidation() {
        string text = '';
        integer N;
        
        List <Loki__c> lokit = [SELECT Id FROM Loki__c WHERE Suljettu__c = FALSE LIMIT :queryLimit];
        N = lokit.size();
        
        /* check if any records */
        if(N == 0) {
            return text;
        }
        
        /* add errors number */
        text += 'Number of non-closed Loki__c records: ' + N + '\n';
        return text + '\n';
    }

    public string tapausValidation() {
        string text = '';
        integer N;
        
        List <Tapaus__c> tapaukset = [SELECT Id FROM Tapaus__c WHERE Tila__c != 'Hoidettu' AND OwnerId = '00G0Y000002WNpZ' LIMIT :queryLimit];
        N = tapaukset.size();
        
        /* check if any records */
        if(N == 0) {
            return text;
        }
        
        /* add tapaus number */
        text += 'Number of non-Hoidettu Tapaus__c records: ' + N + '\n';
        return text + '\n';
    }



    public void execute(Database.BatchableContext bc, List<Arvolista__c> arvolistaEmails) { 

            Boolean limitsAllowSending = true; 
            string filter = '';
            string monitorMsg = '';
            string emailBody = '';
                            
            try {
                Messaging.reserveSingleEmailCapacity(1001);
            } 
            catch(Exception e) { 
                if(!Test.isRunningTest()) limitsAllowSending = false;
            } 
            if(limitsAllowSending) {

                List<String> toEmails = New List<String>();
                for(Arvolista__c ae: arvolistaEmails) {
                    toEmails.add(ae.Arvo_1_Teksti__c);
                }
                /* construct the body of email */
                
                filter = 'x_Tepa_Synch_status__c = \'ERROR\' LIMIT ' + queryLimit;
                monitorMsg = 'Tepa Errors ';                
                emailBody += defineValidation('Account', filter, monitorMsg);
                emailBody += defineValidation('Ipi_numero__c', filter, monitorMsg);
                emailBody += defineValidation('Verotieto__c', filter, monitorMsg);
                emailBody += defineValidation('Jasenyys__c', filter, monitorMsg);
                emailBody += defineValidation('Edunsaantitiedot__c', filter, monitorMsg);
                emailBody += defineValidation('Osoite__c', filter, monitorMsg);
                emailBody += defineValidation('Sopimustieto__c', filter, monitorMsg);

                if(emailBody != null && emailBody != '') emailBody = 'TEPA ERRORS' + '\n' + emailBody + '\n \n' + 'MUSKA ERRORS' + '\n';

                filter = 'x_Muska_Synch_status__c = \'ERROR\' LIMIT ' + queryLimit;
                monitorMsg = 'Muska Errors ';
                emailBody += defineValidation('Sopimustieto__c', filter, monitorMsg);
                emailBody += defineValidation('Tuoteosto__c', filter, monitorMsg);
                emailBody += defineValidation('Esityspaikka__c', filter, monitorMsg);
                emailBody += defineValidation('Esityspaikkahistoria__c', filter, monitorMsg);
                emailBody += defineValidation('Sopimuksen_yhteyshenkilo__c', filter, monitorMsg);
                emailBody += defineValidation('Osoite__c', filter, monitorMsg);

                if(emailBody != null && emailBody != '') emailBody += '\n' + 'LASKUTIN ERRORS' + '\n';

                filter = 'x_Laskutin_Synch_status__c = \'ERROR\' LIMIT ' + queryLimit;
                monitorMsg = 'Laskutin Errors ';
                emailBody += defineValidation('Tuote__c', filter, monitorMsg);
                emailBody += defineValidation('Tuotteen_hinnasto__c', filter, monitorMsg);
                emailBody += defineValidation('Tuotteen_hintarivi__c', filter, monitorMsg);
                emailBody += defineValidation('Tuotteen_hintarivien_raja_arvo__c', filter, monitorMsg);
                emailBody += defineValidation('Tuotteen_laskuteksti__c', filter, monitorMsg);
                emailBody += defineValidation('Tuotteen_laskutuspaiva__c', filter, monitorMsg);
                emailBody += defineValidation('Tuotteen_parametri__c', filter, monitorMsg);

                if(emailBody != null && emailBody != '') emailBody += '\n' + 'TEPA DELAYS' + '\n';

                filter = 'x_Tepa_Synch_status__c != \'ERROR\' AND LastModifiedById != \'0050Y000000soAV\' AND x_Tepa_Synch__c = TRUE AND LastModifiedDate < ' + limitDT_string + ' AND LastModifiedDate > ' + querySD_string + ' LIMIT ' + queryLimit;
                monitorMsg = 'Tepa Delays ';
                emailBody += defineValidation('Account', filter, monitorMsg);
                emailBody += defineValidation('Ipi_numero__c', filter, monitorMsg);
                emailBody += defineValidation('Verotieto__c', filter, monitorMsg);
                emailBody += defineValidation('Jasenyys__c', filter, monitorMsg);
                emailBody += defineValidation('Edunsaantitiedot__c', filter, monitorMsg);
                emailBody += defineValidation('Osoite__c', filter, monitorMsg);
                emailBody += defineValidation('Sopimustieto__c', filter, monitorMsg);                

                if(emailBody != null && emailBody != '') emailBody += '\n' + 'MUSKA DELAYS' + '\n';

                filter = 'x_Muska_Synch_status__c != \'ERROR\' AND LastModifiedById != \'0050Y000000soAV\' AND x_Muska_Synch__c = TRUE AND LastModifiedDate < ' + limitDT_string + ' AND LastModifiedDate > ' + querySD_string + ' LIMIT ' + queryLimit;
                monitorMsg = 'Muska Delays ';
                emailBody += defineValidation('Sopimustieto__c', filter, monitorMsg);
                emailBody += defineValidation('Tuoteosto__c', filter, monitorMsg);
                emailBody += defineValidation('Esityspaikka__c', filter, monitorMsg);
                emailBody += defineValidation('Esityspaikkahistoria__c', filter, monitorMsg);
                emailBody += defineValidation('Sopimuksen_yhteyshenkilo__c', filter, monitorMsg);
                emailBody += defineValidation('Osoite__c', filter, monitorMsg);

                if(emailBody != null && emailBody != '') emailBody += '\n' + 'YHTIÖMALLISOPIMUS' + '\n';

                /*Tämä tarkistaa Teostotulojen yhtiömallisopimukset */
                filter = 'x_Tepa_Synch_status__c != \'SUCCESS\' AND Tila__c = \'Valmis\' AND RecordType.DeveloperName = \'Teostotulojen_yhtiomalli\' LIMIT ' + queryLimit;
                monitorMsg = 'Teostotulojen yhtiömalli ';
                emailBody += defineValidation('Sopimustieto__c', filter, monitorMsg);                

                if(emailBody != null && emailBody != '') emailBody += 'LASKUTIN DELAYS' + '\n';

                filter = 'x_Laskutin_Synch_status__c != \'ERROR\' AND LastModifiedById != \'0050Y000000soAV\' AND x_Laskutin_Synch__c = TRUE AND LastModifiedDate < ' + limitDT_string + ' AND LastModifiedDate > ' + querySD_string + ' LIMIT ' + queryLimit;
                monitorMsg = 'Laskutin Delays ';
                emailBody += defineValidation('Tuote__c', filter, monitorMsg);
                emailBody += defineValidation('Tuotteen_hinnasto__c', filter, monitorMsg);
                emailBody += defineValidation('Tuotteen_hintarivi__c', filter, monitorMsg);
                emailBody += defineValidation('Tuotteen_hintarivien_raja_arvo__c', filter, monitorMsg);
                emailBody += defineValidation('Tuotteen_laskuteksti__c', filter, monitorMsg);
                emailBody += defineValidation('Tuotteen_laskutuspaiva__c', filter, monitorMsg);
                emailBody += defineValidation('Tuotteen_parametri__c', filter, monitorMsg);

                if(emailBody != null && emailBody != '') emailBody += 'LOKI RECORDS' + '\n';
                emailBody += lokiValidation();

                if(emailBody != null && emailBody != '') emailBody += 'TAPAUS (INTEGRAATIO) RECORDS' + '\n';
                emailBody += tapausValidation();

                Messaging.SingleEmailMessage emailMsg= new Messaging.SingleEmailMessage(); 

                if(emailBody != null && emailBody != '') {
                    emailMsg.setSubject(emailSubject);
                    emailMsg.setPlainTextBody(emailBody);
                    emailMsg.setToAddresses(toEmails);
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] {emailMsg});
                }

            } 
    }
    public void finish(Database.BatchableContext bc) {
    
    } 
    public void execute(SchedulableContext sc){
        Database.executeBatch(this,1);
    } 
}