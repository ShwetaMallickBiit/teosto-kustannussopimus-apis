public with sharing class MarkkinointiaktiviteettiTriggerHandler {

    public static List<Markkinointiaktiviteetti__c> haeParentAktiviteetit(List<Markkinointiaktiviteetti__c> aktiviteetitParent) {
        Set<String> campaignMemberIds = New Set<String>();
        for(Markkinointiaktiviteetti__c ak : aktiviteetitParent) {
            campaignMemberIds.add(ak.Campaign_Member_Id__c);
        }
        return [
                SELECT Id, Campaign_Member_Id__c, Tyyppi__c, Aihe__c, Mail_Id__c, Mail_Id_Unique__c
                FROM Markkinointiaktiviteetti__c 
                WHERE Tyyppi__c = 'mail delivery' AND Campaign_Member_Id__c IN :campaignMemberIds
               ];        
    }

    public static Id haeParentAktiviteetti(List<Markkinointiaktiviteetti__c> aktiviteetitParent, Markkinointiaktiviteetti__c aktiviteettiUusi) {
        
        string parentId = null;
        for(Markkinointiaktiviteetti__c ak : aktiviteetitParent) {
            if(ak.Campaign_Member_Id__c == aktiviteettiUusi.Campaign_Member_Id__c && ak.Mail_Id_Unique__c == aktiviteettiUusi.Mail_Id_Unique__c) {
                parentId = ak.Id;
                break;
            }
        }
        return parentId;
    }

    public static void linkitaParentMarkkinointiaktiviteetti(Map<Id, Markkinointiaktiviteetti__c> aktiviteetitUusi) {

        string parentId;
        List<Markkinointiaktiviteetti__c> aktiviteetitPaivitys = New List<Markkinointiaktiviteetti__c>();
        List<Markkinointiaktiviteetti__c> aktiviteetitParent = haeParentAktiviteetit(aktiviteetitUusi.Values());
        for(Markkinointiaktiviteetti__c ak : aktiviteetitUusi.Values()) {
            Markkinointiaktiviteetti__c aktiviteettiPaivitys = New Markkinointiaktiviteetti__c();
            if(ak.Tyyppi__c != 'mail delivery') {
                aktiviteettiPaivitys.Id = ak.Id;
                aktiviteettiPaivitys.Markkinointiaktiviteetti_Parent__c = haeParentAktiviteetti(aktiviteetitParent, ak);
                aktiviteetitPaivitys.add(aktiviteettiPaivitys);
            }
        }
        if(!aktiviteetitPaivitys.isEmpty())
            update(aktiviteetitPaivitys);
    }  

    public static Map<Id, EmailMessage> haeEmailMessages(List<Markkinointiaktiviteetti__c> aktiviteetitUusi) {
        set<String> ids= New Set<String>();
        for(Markkinointiaktiviteetti__c ak : aktiviteetitUusi) {
            ids.add(ak.Campaign_Member_Id__c);
        }
        Map<Id, EmailMessage> emailMsgs = New Map<Id, EmailMessage>();
        emailMsgs.putAll([SELECT Id, ThreadIdentifier, FirstOpenedDate FROM EmailMessage WHERE Campaign_Member_Id__c IN :ids]);
        return emailMsgs;
    }

    public static EmailMessage luoEmailMsg(Markkinointiaktiviteetti__c ak) {
        EmailMessage msg = New EmailMessage();
        msg.TextBody = ak.Sahkopostin_sisalto__c;
        msg.HtmlBody = ak.Sahkopostin_sisalto__c;
        msg.Subject = ak.Aihe__c;
        msg.FromName = 'Teosto';
        msg.FromAddress = '';
        msg.ToAddress = ak.Email__c;
        msg.Status = '3';
        msg.MessageIdentifier = ak.Id;
        msg.ThreadIdentifier = ak.Id;
        msg.RelatedToId = ak.Campaign_Id__c;
        msg.LastOpenedDate = null;
        msg.IsBounced = false;
        msg.Contact_Id__c = ak.Contact_Id__c;
        msg.Campaign_Member_Id__c = ak.Campaign_Member_Id__c;
        msg.Campaign_Id__c = ak.Campaign_Id__c;
        msg.Lahde__c = 'Postiviidakko';
        msg.Syy__c = ak.Syy__c;
        return msg;
    }

    public static List<EmailMessage> emailMsgListLuotavat(List<Markkinointiaktiviteetti__c> aktiviteetitUusi) {

        List<EmailMessage> msgsUusi = New List<EmailMessage>();
        for(Markkinointiaktiviteetti__c ak : aktiviteetitUusi) {
            Markkinointiaktiviteetti__c akPaivitettava = New Markkinointiaktiviteetti__c();
            if(ak.Tyyppi__c == 'mail delivery') {
                msgsUusi.add(luoEmailMsg(ak));
            }
        }
        return msgsUusi;
    }

    public static List<EmailMessage> emailMsgPaivitettavat(List<Markkinointiaktiviteetti__c> aktiviteetitUusi, Map<Id, EmailMessage> msgsExisting, String tyyppi) {

        Set<Id> ids = New Set<Id>();
        for(Markkinointiaktiviteetti__c ak : aktiviteetitUusi) {
            ids.add(ak.Id);
        }
        List<Markkinointiaktiviteetti__c> aks = [
                                                 SELECT Id, Tyyppi__c, Email_Message_Id_Parent__c, Paivamaara__c, Klikattu_linkki__c
                                                 FROM Markkinointiaktiviteetti__c
                                                 WHERE Id in :ids AND Tyyppi__c = :tyyppi
                                                ];

        List<EmailMessage> msgs = New List<EmailMessage>();
        for(Markkinointiaktiviteetti__c ak : aks) {
            EmailMessage msgPaivitettava = New EmailMessage();
            EmailMessage msgExisting = msgsExisting.get(ak.Email_Message_Id_Parent__c);
            if(msgExisting == null) return msgs;
            
            if(ak.Tyyppi__c == 'mailopen') {
                msgPaivitettava.Id = ak.Email_Message_Id_Parent__c;
                msgPaivitettava.LastOpenedDate = ak.Paivamaara__c;
                if(msgExisting.FirstOpenedDate == null) msgPaivitettava.FirstOpenedDate = ak.Paivamaara__c;
                
                msgs.add(msgPaivitettava);
                
            }
            if(ak.Tyyppi__c == 'mailbounce') {
                msgPaivitettava.Id = ak.Email_Message_Id_Parent__c;
                msgPaivitettava.IsBounced = true;
                msgs.add(msgPaivitettava);
            }
            if(ak.Tyyppi__c == 'maillink') {
                msgPaivitettava.Id = ak.Email_Message_Id_Parent__c;
                msgPaivitettava.Viimeisin_klikattu_linkki_pvm__c = ak.Paivamaara__c;
                msgPaivitettava.Viimeisin_klikattu_linkki__c = ak.Klikattu_linkki__c;
                msgs.add(msgPaivitettava);
            }
        }
        return msgs;
    }
    public static List<Markkinointiaktiviteetti__c> maPaivitettavat(List<EmailMessage> msgs) {
        List<Markkinointiaktiviteetti__c> mas = New List<Markkinointiaktiviteetti__c>();
        for(EmailMessage msg : msgs) {
            Markkinointiaktiviteetti__c ma = New Markkinointiaktiviteetti__c();
            ma.Id = msg.ThreadIdentifier;
            ma.Email_Message_Id__c = msg.Id;
            mas.add(ma);
        }
        return mas;
    }

    public static List<Task> taskPaivitettavat(List<Markkinointiaktiviteetti__c> mas) {

        Set<Id> ids = New Set<Id>();
        for(Markkinointiaktiviteetti__c ma : mas) {
            ids.add(ma.Email_Message_Id__c);
        }
        
        List<EmailMessage> msgsHaetut = [
                                         SELECT Id, ActivityId, Contact_Id__c, Campaign_Member_Id__c, Campaign_Id__c
                                         FROM EmailMessage 
                                         WHERE Id IN :ids
                                        ];
        List<Task> tasks = New List<Task>();
        for(EmailMessage msg : msgsHaetut) {
            Task tsk = New Task();
            tsk.Id = msg.ActivityId;
            tsk.WhoId = msg.Contact_Id__c;
            tasks.add(tsk);
        }
        return tasks;
    }
    public static List<CampaignMember> campaignMemberStatusUpdate(List<Markkinointiaktiviteetti__c> mas, string tyyppi) {

        List<CampaignMember> cms = New List<CampaignMember>();
        for(Markkinointiaktiviteetti__c ma : mas) {
            CampaignMember cm = New CampaignMember();
            if(ma.Tyyppi__c == tyyppi) {
                cm.Id = ma.Campaign_Member_Id__c;
                cm.Status = 'Sent';
                cms.add(cm);
            }
        }
        return cms;
    }
    /* this method retriggers the linking etc. between maildelivery and other records. This is because the order of the 
       "sent record" and other records (open, link clicks, etc) cannot be guaranteed
    */
    public static void remanageRecords(List<Markkinointiaktiviteetti__c> mas) {
        Set<String> mailids = New Set<String>();
        Map<Id, Markkinointiaktiviteetti__c> masNewMap = New Map<Id, Markkinointiaktiviteetti__c>();
        for(Markkinointiaktiviteetti__c ma : mas) {
            if(ma.Tyyppi__c == 'mail delivery') {
                mailids.add(ma.Mail_Id_Unique__c);
            }
        }
        List<Markkinointiaktiviteetti__c> masNew = 
            [
                SELECT Id, Name, Aihe__c, Campaign__c, Campaign_Id__c, Campaign_Member_Id__c, Contact__c, Contact_Id__c,
                       Email__c, Email_Message_Id_Parent__c, Email_Message_Id__c, EmailOptOut__c, Klikattu_linkki__c, 
                       Mail_Id__c, Mail_Id_Unique__c, Markkinointiaktiviteetti_Parent__c, Paivamaara__c, Syy__c, Sahkopostin_sisalto__c, 
                       Tyyppi__c
                FROM Markkinointiaktiviteetti__c
                WHERE Tyyppi__c != 'mail delivery' AND Markkinointiaktiviteetti_Parent__c = NULL AND Mail_Id_Unique__c IN :mailids
            ];
        if(!masNew.isEmpty()) {
            for(Markkinointiaktiviteetti__c maNew : masNew) {
                masNewMap.put(maNew.Id, maNew);
            }
            linkitaParentMarkkinointiaktiviteetti(masNewMap);
            luoEmailMsg(masNewMap);
        }
    }
    @future
    public static void updateTasks(Set<Id> ids) {
                List<Markkinointiaktiviteetti__c> mas = [
                SELECT Id, Name, Aihe__c, Campaign__c, Campaign_Id__c, Campaign_Member_Id__c, Contact__c, Contact_Id__c,
                       Email__c, Email_Message_Id_Parent__c, Email_Message_Id__c, EmailOptOut__c, Klikattu_linkki__c, 
                       Mail_Id__c, Mail_Id_Unique__c, Markkinointiaktiviteetti_Parent__c, Paivamaara__c, Syy__c, Sahkopostin_sisalto__c, 
                       Tyyppi__c
                FROM Markkinointiaktiviteetti__c
                WHERE Id IN :ids AND Tyyppi__c = 'mail delivery'];
                List<Task> tasks = taskPaivitettavat(mas);
                system.debug('Tasks: ' + tasks);
                if(!tasks.isEmpty()) update tasks;
    }
    
    public static List<EmailMessage> removeDuplicatesFromEmailMessage(List<EmailMessage> msgs) {
        Map<Id, EmailMessage> msgMap = New Map<Id, EmailMessage>();
        for(EmailMessage msg : msgs) {
            if(!msgMap.containsKey(msg.Id)) msgMap.put(msg.Id, msg);
        }
        return msgMap.values();
    }
    
    public static void luoEmailMsg(Map<Id, Markkinointiaktiviteetti__c> aktiviteetitUusi) {

        List<EmailMessage> msgsUusi = emailMsgListLuotavat(aktiviteetitUusi.Values());
        /* updates campaign member status */
        List<CampaignMember> cmsSent = campaignMemberStatusUpdate(aktiviteetitUusi.Values(), 'mail delivery');
        if(!cmsSent.isEmpty()) update cmsSent;
        List<CampaignMember> cmsOpen = campaignMemberStatusUpdate(aktiviteetitUusi.Values(), 'mailopen');
        if(!cmsOpen.isEmpty()) update cmsOpen;        
        /* creates email message records */
        system.debug('EmailMsgs: ' + msgsUusi);
        if(!msgsUusi.isEmpty()) insert msgsUusi;
        /* updates email message id to Markkinointiaktiviteetti__c record */
        List<Markkinointiaktiviteetti__c> mas = maPaivitettavat(msgsUusi);
        system.debug('MAs: ' + mas);
        if(!mas.isEmpty()) update mas;
        /* updates tasks related to email message records to publish messages under contact / account */
        if(!aktiviteetitUusi.keyset().isEmpty()) updateTasks(aktiviteetitUusi.keyset());

        /* this is replicated to avoid duplicate id errors i.e. there are two Markkinointiaktiviteetti__c records for same email message such as maillink and mailopen */
        Map<Id, EmailMessage> msgsExisting = haeEmailMessages(aktiviteetitUusi.Values());
        List<EmailMessage> msgsOpen = emailMsgPaivitettavat(aktiviteetitUusi.Values(), msgsExisting, 'mailopen');
        if(!msgsOpen.isEmpty()) update removeDuplicatesFromEmailMessage(msgsOpen);
        List<EmailMessage> msgsLink = emailMsgPaivitettavat(aktiviteetitUusi.Values(), msgsExisting, 'maillink');
        if(!msgsLink.isEmpty()) update removeDuplicatesFromEmailMessage(msgsLink);
        List<EmailMessage> msgsBounce = emailMsgPaivitettavat(aktiviteetitUusi.Values(), msgsExisting, 'bounce');
        if(!msgsBounce.isEmpty()) update removeDuplicatesFromEmailMessage(msgsBounce);

        remanageRecords(aktiviteetitUusi.Values());
    }
    
    public static Set<String> getCampaignEmailsSet(List<Markkinointiaktiviteetti__c> maList) {
        Set<String> campaignEmails = New Set<String>();
        for(Markkinointiaktiviteetti__c ma : maList) {
            campaignEmails.add(ma.Email__c);
        }
        return campaignEmails;
    }
    
    public static Map<String, CampaignMember> getCampaignMemberMap(Set<String> emails) {
        Map<String, CampaignMember> campaignMemberMap = New Map<String, CampaignMember>();
        List<CampaignMember> campaignMemberList = [SELECT Id, CampaignId, Email, Campaign.Name, ContactId FROM CampaignMember WHERE Email IN :emails];
        for(CampaignMember c : campaignMemberList) {
            campaignMemberMap.put(c.Campaign.Name + c.Email, c);
        }
        return campaignMemberMap;
    }
    
    public static void getCampaign(List<Markkinointiaktiviteetti__c> aktiviteetitUusi) {
        Set<String> campaignEmails = getCampaignEmailsSet(aktiviteetitUusi);
        Map<String, CampaignMember> campaignMemberMap = getCampaignMemberMap(campaignEmails);
        for(Markkinointiaktiviteetti__c ma : aktiviteetitUusi) {
            CampaignMember cm = New CampaignMember();
            if(!string.isBlank(ma.Campaign_Name__c) && !string.isBlank(ma.Email__c)) {
                cm = campaignMemberMap.get(ma.Campaign_Name__c + ma.Email__c);
                ma.Campaign_Id__c = cm.CampaignId;
                ma.Campaign__c = cm.CampaignId;
                ma.Campaign_Member_Id__c = cm.Id;
                ma.Contact_Id__c = cm.ContactId;
                ma.Contact__c = cm.ContactId;
                ma.Mail_Id_Unique__c = ma.Mail_Id__c + ma.Email__c;
            }
        }
    }
}