@isTest(seeAllData=true)
global class KAYT_Batch_WekaratunnustenPassivointiTst {

    static testMethod void CreateData() {

       // create account
       Account acc = new Account(
           Name = 'TestAccount', Kieli__c = 'Suomi', Asiakasryhma__c = 'Käyttäjä', Luokittelu__c ='Avain',
           Ohita_Y_tunnus_Hetu_validointi__c = true, ShippingStreet = 'Testikatu 10', ShippingCity = 'Helsinki',
           ShippingPostalCode = '00100');
       insert acc;

       // get record type id    
       Id RecordTypeIdST = Schema.SObjectType.Sopimustieto__c.getRecordTypeInfosByName().get('Musiikinkäyttösopimus, sopimusvahvistus').getRecordTypeId(); 

       // create contact
       Contact con = new Contact(
           FirstName = 'Etunimi', LastName = 'Sukunimi');
       insert con;

       // create Acc_con__c
       AccountContactRelation_c__c acc_con = new AccountContactRelation_c__c(
           AccountId_c__c = acc.Id, ContactId_c__c = con.Id,Name = 'Testinimi', Sidoksen_email_c__c = 'testi.testinen@biit.fi');
       insert acc_con;

       // create Sopimustieto for KAYT_Batch_sopimusvahvistusPDFgenerointi
       Sopimustieto__c st_sv = new Sopimustieto__c(
           recordtypeid=RecordTypeIdST, Asiakas_tekija__c = acc.Id, OnEri_kuin_postitusosoite__c = true,
           Laskutusosoite_Katu__c = 'Testikatu 1', Laskutusosoite_Kaupunki__c = 'Helsinki', Laskutusosoite_Postinumero__c = '00100',
           x_onTuoteosto__c = true, Vastuuhenkilo__c = acc_con.Id, Allekirjoittaja__c = acc_con.Id, Tila__c = 'Valmis lähetettäväksi',
           Alkupvm__c = Date.newInstance(2018, 01, 01), Sopimuksen_allekirjoituspvm__c = Date.newInstance(2018, 01, 01));
       insert st_sv;

       

       // create Tuoteosto
       Tuoteosto__c conData = new Tuoteosto__c(Sopimustieto__c = st_sv.Id, Asiakas__c = acc.Id);
       insert conData;

       // create sopimuksen yhteyshenkilö
       Sopimuksen_yhteyshenkilo__c sopYhthlo = new Sopimuksen_yhteyshenkilo__c(
           Alkupvm__c = Date.newInstance(2018, 01, 01), Loppupvm__c = Date.newInstance(2018, 01, 02), 
           Rooli__c = 'Sopimuksen yhteyshenkilö', Sopimustieto__c = st_sv.Id, 
           Webtunnus_tila__c = 'Aktiivinen', AccountContactRelation_c__c = acc_con.Id,
           Web_kayttajatunnus__c = 'AS123456'
       );
       insert sopYhthlo;

       //  Run test
       Test.startTest();
       KAYT_Batch_WekaratunnustenPassivointi sopYhthloPassivointi = new KAYT_Batch_WekaratunnustenPassivointi();
       ID batchprocessid1 = Database.executeBatch(sopYhthloPassivointi);
       
       
    }
}