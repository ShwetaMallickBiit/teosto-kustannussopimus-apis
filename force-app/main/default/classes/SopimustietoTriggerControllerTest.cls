@isTest(SeeAllData=false) 
private class SopimustietoTriggerControllerTest{

    @isTest
    static void testSuccess() {
        Account a = GEN_TestFactory.createAccountTekija();
        a.Asiakasnumero_Tepa__c = '1234';
        update a;
        Contact con = GEN_TestFactory.createContact(a.Id, 'Test', 'Testinen');
        String yhteyshenkiloId = [SELECT Id FROM AccountContactRelation_c__c WHERE AccountId_c__c = :a.Id AND ContactId_c__c = :con.Id LIMIT 1][0].Id;
        Sopimustieto__c st = GEN_TestFactory.createSopimusTekija2(a.Id, yhteyshenkiloId, TRUE);

        Test.setMock(HttpCalloutMock.class, new SELFADMIN_Test.MockResponseUpdateNotificationStatus());

        Test.startTest();
            SopimustietoTriggerController.updateRecord(st.Id);
        Test.stopTest();
    }
    @isTest
    static void testPageSuccess() {
        Account a = GEN_TestFactory.createAccountTekija();
        a.Asiakasnumero_Tepa__c = '1234';
        update a;
        Contact con = GEN_TestFactory.createContact(a.Id, 'Test', 'Testinen');
        String yhteyshenkiloId = [SELECT Id FROM AccountContactRelation_c__c WHERE AccountId_c__c = :a.Id AND ContactId_c__c = :con.Id LIMIT 1][0].Id;
        Sopimustieto__c st = GEN_TestFactory.createSopimusTekija2(a.Id, yhteyshenkiloId, TRUE);
        ApexPages.StandardController stdController = new ApexPages.StandardController(st);
        SopimustietoTriggerController stCont = new SopimustietoTriggerController(stdController);
    }
}