global class DM_KAYT_YhthloImportTrigger implements Database.stateful, Schedulable, Database.AllowsCallouts, Database.Batchable<sObject>{
    
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        String soql = 'SELECT Id, onDatamigraatio__c, Batch_Status__c FROM DM_Tasokas_yhthlo__c WHERE onDatamigraatio__c = FALSE';
        return Database.getQueryLocator(soql);
    }

    public void execute(Database.BatchableContext bc, List<DM_Tasokas_yhthlo__c> records) {

        for (DM_Tasokas_yhthlo__c record : records) {
            try {
                /* TRIGGER BATCH */
                DM_Tasokas_yhthlo__c yhthlo = new DM_Tasokas_yhthlo__c();
                yhthlo.Id = record.Id;
                yhthlo.onDatamigraatio__c = TRUE;
                yhthlo.Batch_Status__c = 'SUCCESS';
                update yhthlo;
                /* UPDATE SUCCESS */
                DM_Tasokas_yhthlo__c yhthlo_success = new DM_Tasokas_yhthlo__c();
                yhthlo_success.Id = record.Id;
                yhthlo_success.Batch_Status__c = 'SUCCESS';
            }
            catch(Exception e) {
                System.debug(e.getMessage());

                /* UPDATE ERROR */
                DM_Tasokas_yhthlo__c yhthlo_error = new DM_Tasokas_yhthlo__c(); yhthlo_error.Id = record.Id; yhthlo_error.Batch_Status__c = 'ERROR'; update yhthlo_error;
            }

        }
    }
    public void finish(Database.BatchableContext bc) {
        
    }
    public void execute(SchedulableContext sc){
        Database.executeBatch(this,1);
    }
}