public with sharing class AccountContactRelationTriggerHandler{

    public static void paivitaYhteyshenkiloEmail(List<AccountContactRelation> accCons, Map<Id, AccountContactRelation> accConsOldMap) {
      Set<Id> conIds = New Set<Id>();
        for (AccountContactRelation accCon : accCons) {
            AccountContactRelation accConOld = accConsOldMap.get(accCon.Id);
            if(accConOld.Sidoksen_email__c != accCon.Sidoksen_email__c || accConOld.EndDate != accCon.EndDate) 
                conIds.add(accCon.ContactId);
        }
    
        if(!conIds.isEmpty()) 
            GEN_Batch_synkYhthloEmailHandler.paivitaYhteyshenkiloEmailAsync(conIds, 'AccountContactRelationTrigger');
    }
}