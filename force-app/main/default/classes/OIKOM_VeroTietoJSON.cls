public class OIKOM_VeroTietoJSON {
    
    public Integer asnro {get;set;}   //12345
    public String alkpvm {get;set;}   //2017-01-02
    public String paatpvm {get;set;}  //2017-12-31
    public Double abrutto {get;set;}  //6543.21
    public Double avero {get;set;}   //1234.56
    public Double lbrutto {get;set;}  //1234.56
    public Double lvero {get;set;}   //1234.56
    public Double pbrutto {get;set;}  //1234.56
    public Double pvero {get;set;}    //1234.56
    public Integer veroton {get;set;}  //0
    public String verotieto {get;set;}    //T
    public String maakoodi {get;set;} //FI
    public Integer rowid {get;set;}   //666
    
    public OIKOM_VeroTietoJSON() {
    
    }
}