@isTest(seeAllData=false)
public class KAYT_Batch_synkEP_EPHistoriaTest {

    @testSetup static void testCoverageAll() {

        Account acc = new Account();
        acc.Name = 'Account Old';
        acc.Asiakasryhma__c = 'Muu';
        acc.ShippingStreet = 'Mikonkatu 1';
        acc.ShippingPostalCode = '00100';
        acc.ShippingCity = 'Helsinki';
        acc.Ohita_Y_tunnus_Hetu_validointi__c = true;
        insert acc;
        string accOld = acc.Id;

        Account accN = new Account();
        accN.Name = 'Account New';
        accN.Asiakasryhma__c = 'Muu';
        accN.ShippingStreet = 'Mikonkatu 1';
        accN.ShippingPostalCode = '00100';
        accN.ShippingCity = 'Helsinki';
        accN.Ohita_Y_tunnus_Hetu_validointi__c = true;
        insert accN;
        string accNew = accN.Id;
                
        Arvolista__c maa = new Arvolista__c();
        maa.Name = 'Finland';
        maa.Ryhma__c = 'COUNTRY';
        insert maa;
        
        RecordType recType = [SELECT id FROM RecordType WHERE Name= 'Esityspaikka'];
        
        // create Esityspaikka__c record and link it to Account Old
        Esityspaikka__c place = new Esityspaikka__c();
        place.Name = 'Test';
        place.RecordTypeId = recType.id;
        place.Maa__c = maa.id;
        place.Asiakas__c = accOld;
        insert place;

        // create Esityspaikka__c without Esityspaikkahistoria__c record and link it to an Account
        Esityspaikka__c placeFree = new Esityspaikka__c();
        placeFree.Name = 'I am free';
        placeFree.RecordTypeId = recType.id;
        placeFree.Maa__c = maa.id;
        placeFree.Asiakas__c = accOld;
        insert placeFree;
        
        // create Esityspaikka__c record and link it to Account New
        Esityspaikkahistoria__c placeHistoryObject = new Esityspaikkahistoria__c();
        placeHistoryObject = new Esityspaikkahistoria__c();
        placeHistoryObject.Asiakas__c = accNew;
        placeHistoryObject.Alkupvm__c = System.today().addDays(-1);
        placeHistoryObject.Loppupvm__c  = System.today().addDays(1);
        placeHistoryObject.Esityspaikka__c = place.id;
        insert placeHistoryObject;
    }
    
    @isTest static void BatchUpdateAccountAndHistoryForPlaceTest() {
        
        Test.StartTest();
        KAYT_Batch_synkEP_EPHistoriaAktiiv cr = new KAYT_Batch_synkEP_EPHistoriaAktiiv();
        String sch = '0 0 2 * * ?'; 
        system.schedule('Batch', sch, cr); 
        Database.executeBatch(cr);
        Test.stopTest(); 
                
        // batch run should have transferred Esityspaikka__c record from Account Old to Account New
        List<Esityspaikka__c> place = [SELECT Asiakas__r.Name, x_Lisaa_yhteyshenkilo__c FROM Esityspaikka__c WHERE Asiakas__r.Name = 'Account New' LIMIT 1];
        // batch should have nulled Asiakas__c field
        List<Esityspaikka__c> placeFree = [SELECT Id FROM Esityspaikka__c WHERE Name = 'I am free' AND Asiakas__c = null LIMIT 1];
        System.assertEquals(1, place.size()); 
        System.assertEquals(1, placeFree.size()); 

    }

}