global class DM_KAYT_AsiakasImportTrigger implements Database.stateful, Schedulable, Database.AllowsCallouts, Database.Batchable<sObject>{
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        String soql = 'SELECT Id, onDatamigraatio__c, Batch_Status__c FROM DM_Tasokas_asiakas__c WHERE onDatamigraatio__c = FALSE';
        return Database.getQueryLocator(soql);
    }

    public void execute(Database.BatchableContext bc, List<DM_Tasokas_asiakas__c> records) {

        for (DM_Tasokas_asiakas__c record : records) {
            try {
                /* TRIGGER BATCH */
                DM_Tasokas_asiakas__c asiakas = new DM_Tasokas_asiakas__c();
                asiakas.Id = record.Id;
                asiakas.onDatamigraatio__c = TRUE;
                asiakas.Batch_Status__c = 'SUCCESS';
                update asiakas;
                /* UPDATE SUCCESS */
                DM_Tasokas_asiakas__c asiakas_success = new DM_Tasokas_asiakas__c();
                asiakas_success.Id = record.Id;
                asiakas_success.Batch_Status__c = 'SUCCESS';
            }
            catch(Exception e) {
                System.debug(e.getMessage());
                /* UPDATE ERROR */
                DM_Tasokas_asiakas__c asiakas_error = new DM_Tasokas_asiakas__c(); asiakas_error.Id = record.Id; asiakas_error.Batch_Status__c = 'ERROR'; update asiakas_error;
            }

        }
    }
    public void finish(Database.BatchableContext bc) {
        
    }
    public void execute(SchedulableContext sc){
        Database.executeBatch(this,1);
    }
}