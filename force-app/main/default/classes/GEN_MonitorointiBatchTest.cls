@isTest(seeAllData=false)
private class GEN_MonitorointiBatchTest{

    static testMethod void testCase() {

       Arvolista__c al1 = new Arvolista__c(
           Name = 'Temp', Ryhma__c = 'NOTIFIKAATIOEMAIL', Arvo_1_Teksti__c = 'dev@biit.fi');
       insert al1;

       Arvolista__c al2 = new Arvolista__c(
           Name = 'Temp', Ryhma__c = 'NOTIFIKAATIO', Arvo_1_Teksti__c = 'Test subject', 
           Arvo_1_numero_0_desim__c = 10, Arvo_2_numero_0_desim__c = -6, Arvo_3_numero_0_desim__c = 100,
           Arvo_1_datetime__c = DateTime.newInstance(2018, 01, 1));
       insert al2;

       //  Run test
       Test.startTest();
       GEN_MonitorointiBatch batch = new GEN_MonitorointiBatch();
       ID batchprocessid1 = Database.executeBatch(batch );
       Test.stopTest();
    }
}