/*
    Description: Class enables forwarding email message to recipient. Class uses contact (email) as recipient
*/
global without sharing class GEN_TapausEmailForward{

    @InvocableMethod(label = 'Flow Email')
    global static List<emailParams> emailLahetys_flow(List<emailParams> inputParams) {

        inputParams[0].status = lahetaEmail(inputParams[0].tapausId, inputParams[0].forwardedMsgId, inputParams[0].additionalBody, inputParams[0].toContactId, inputParams[0].toCCEmail, inputParams[0].orgWideEmailName, inputParams[0].sendAttachments);
        return inputParams;
    }

  global class emailParams{
    @InvocableVariable(required=true)
    global String tapausId;

    @InvocableVariable(required=true)
    global String forwardedMsgId;

    @InvocableVariable(required=true)
    global String additionalBody;    

    @InvocableVariable(required=true)
    global String toContactId;

    @InvocableVariable(required=false)
    global String toCCEmail;

    @InvocableVariable(required=true)
    global String orgWideEmailName;

    @InvocableVariable(required=true)
    global Boolean sendAttachments;    

    @InvocableVariable(required=false)
    global String status;    

    @InvocableVariable(required=false)
    global String errorMsg;    

    }
    
    public static string lahetaEmail(string tapausId, string forwardedMsgId, String additionalBody, string toContactId, string toCCEmail, string orgWideEmailName, Boolean sendAttachments) {

        string bodyHtml;
        string bodyText;
        string bodyEmail;
        string subject;
        List<String> toCCEmails = new List<String>();
        string orgWideEmailId;

        try {

            /* assign cc email to list */
            toCCEmails.add(toCCEmail);

            /* set sender email, note that non-admin profiles cannot access OrgWideEmailAddress object */
            List<Arvolista__c> orgwideEmailIds = [SELECT Arvo_2_Teksti__c FROM Arvolista__c WHERE Ryhma__c = 'TAPAUSEMAILFORWARD' AND Arvo_1_Teksti__c = :orgWideEmailName LIMIT 1];
            orgWideEmailId = orgWideEmailIds[0].Arvo_2_Teksti__c;
            
            /*query original email*/
            List<EmailMessage> emailMsgs = [select id, TextBody, HtmlBody, Subject from EmailMessage where Id = :forwardedMsgId];         
            subject = emailMsgs[0].Subject;
            bodyHtml = emailMsgs[0].HtmlBody;
            bodyText = emailMsgs[0].TextBody;
            if(string.isBlank(bodyHtml)) bodyEmail = additionalBody + bodyText;
            else bodyEmail = additionalBody + bodyHtml;
            
            /*query attachment records*/
            List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.EmailFileAttachment>();
            List<Attachment> attLst = (List<Attachment>) Database.query('SELECT Id,Name,Body,CreatedDate FROM Attachment WHERE parentid = :forwardedMsgId ORDER BY CreatedDate DESC'); 
            for ( Attachment att : attLst )
            { 
                Messaging.EmailFileAttachment mailAtt = new Messaging.EmailFileAttachment(); 
                mailAtt.setFileName(att.name); 
                mailAtt.setBody(att.body); 
                fileAttachments.add(mailAtt);
            }    
            
            /* set email information and send email */
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setOrgWideEmailAddressId(orgWideEmailId);
            email.setSubject(subject);
            email.setHtmlBody(bodyEmail);
            email.setTargetObjectId(toContactId);
            email.setCcAddresses(toCCEmails);
            email.setWhatId(tapausId);
            if(sendAttachments == true) email.setFileAttachments(fileAttachments);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});            

            return 'SUCCESS';

      } catch(Exception e) {
            System.debug(e.getMessage());
            Loki__c errorLog = new Loki__c(Objekti__c = 'Sopimustieto__c', Toiminnallisuus__c = 'KAYT_Batch_PDFlahetys', Tyyppi__c = 'Virhe', Virheviesti__c = e.getMessage());
            insert errorLog; return 'ERROR';
       }

    }
}