@isTest(seeAllData=false)
private class GEN_TapausEmailForwardTest{

    static testMethod void getTapausData() {

        // create account
        Account acc = new Account(
            Name = 'TestAccount', Kieli__c = 'Suomi', Asiakasryhma__c = 'Käyttäjä', Luokittelu__c ='Avain',
            Y_tunnus__c = '2647504-7', ShippingStreet = 'Testikatu 10', ShippingCity = 'Helsinki',
            ShippingPostalCode = '00100', RecordTypeId = '0120Y000000a0p2');
        insert acc;

        // create contact
        Contact con = new Contact(
            FirstName = '', LastName = 'Jono', AccountId = acc.Id);
        insert con;

        // create tapaus
        Tapaus__c tap = new Tapaus__c(
             Aihe__c = 'Testi aihe', Kuvaus__c = 'Lorem ipsum dolor sit amet', Alatyyppi__c = '', Alatyyppi_2__c = '');
        insert tap;

        // create email message
        EmailMessage msg = new EmailMessage();
        msg.RelatedToId = tap.Id;
        msg.FromAddress = 'dev@biit.fi';
        msg.FromName = 'Test Tester';
        msg.Subject = 'Test Subject';
        msg.TextBody = 'Lorem ipsum dolor sit amet';
        msg.HtmlBody = 'Lorem ipsum dolor sit amet';
        msg.MessageDate = system.now();
        msg.Incoming = TRUE;
        msg.Status = '1';
        insert msg;
        
        // update email message to tapaus
        tap.x_Sahkoposti_Id__c = msg.Id;
        update tap;

        // create arvolista__c record
        Arvolista__c al = new Arvolista__c();
        al.Ryhma__c = 'TAPAUSEMAILFORWARD';
        al.Arvo_1_teksti__c = 'Teosto tapahtumat';
        List<OrgWideEmailAddress> orgWideEmails = [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = 'Teosto tapahtumat' LIMIT 1];
        al.Arvo_2_teksti__c = orgWideEmails[0].Id;
        insert al;

        //  Run test
        Test.startTest();
        GEN_TapausEmailForward.emailParams param = new GEN_TapausEmailForward.emailParams();
        param.tapausId = tap.Id;
        param.forwardedMsgId = msg.Id;
        param.toContactId = con.Id;
        param.orgWideEmailName = 'Teosto tapahtumat';
        param.sendAttachments = true;
        List<GEN_TapausEmailForward.emailParams> params = new List<GEN_TapausEmailForward.emailParams>();
        params.add(param);
        GEN_TapausEmailForward.emailLahetys_flow(params);
        Test.stopTest();
    }
}