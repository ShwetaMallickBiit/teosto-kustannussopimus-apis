public with sharing class KAYT_Tuotteen_hinnastoHandler{

    @InvocableMethod(label = 'KAYT_Tuotteen_hinnastoHandler')
    public static List<flowParams> cloneTuotteen_hinnasto(List<flowParams> inputParams) {

        if(inputParams[0].type == 'KLOONI')
            inputParams = clonePriceBook(inputParams[0].priceBookIdNew, inputParams[0].priceBookIdOld, inputParams[0].rate, inputParams);

        if(inputParams[0].type == 'PAIVITYS')
            inputParams = modifyPrices(inputParams[0].priceBookIdNew, inputParams[0].rate, inputParams[0].pricetype, inputParams);

        return inputParams;
    }
    
    public static List<Tuotteen_hintarivi__c> queryPriceRows(String priceBookId) {

        List<Tuotteen_hintarivi__c> priceRows = [
        SELECT Id, Name, Arvo__c, Korvausperuste_fin__c, Korvausperuste_eng__c, Korvausperuste_swe__c, Hintariviselite__c, 
               Hintaselite__c, Hintatyyppi__c, Nakyvyys__c, Rivinro__c, Tili__c, Tuotteen_hinnasto__c, Yksikko__c, 
               x_Hintatyyppi_fin__c, 
               (SELECT Id, Name, Alaraja__c, Raja_arvo__c, Tuotteen_hintarivi__c, Tyyppi__c, Ylaraja__c
                FROM Tuotteen_hintarivien_raja_arvot__r)
        FROM Tuotteen_hintarivi__c 
        WHERE Tuotteen_hinnasto__c = :priceBookId];
        
        return priceRows;
    }

    public static string nullExternalId(List<Tuotteen_hintarivi__c> priceRows) {
        List<Tuotteen_hintarivi__c> priceRowsNew = New List<Tuotteen_hintarivi__c>();
        for(Tuotteen_hintarivi__c priceRow : priceRows) {
            Tuotteen_hintarivi__c priceRowNew = New Tuotteen_hintarivi__c();
            priceRowNew.Id = priceRow.Id;
            priceRowNew.x_External_Id__c = '';
            priceRowsNew.add(priceRowNew);
        }
        update priceRowsNew;
        return 'SUCCESS';
    }

    public static Decimal calculatePrice(Decimal price, Decimal rate) {
        Decimal priceNew = price*(1.0 + (rate*1.0)/100.0);

        if(priceNew < 1.0) priceNew = priceNew.setScale(4, RoundingMode.HALF_UP);
        else priceNew = priceNew.setScale(2, RoundingMode.HALF_UP);
        
        return priceNew;
    }

    public static List<flowParams> clonePriceBook(String priceBookIdNew, String priceBookIdOld, Decimal rate, List<flowParams> params) {

        List<Tuotteen_hintarivi__c> priceRows = queryPriceRows(priceBookIdOld);
        List<Tuotteen_hintarivi__c> priceRowsNew = New List<Tuotteen_hintarivi__c>();
        List<Tuotteen_hintarivien_raja_arvo__c> priceLimitsNew = New List<Tuotteen_hintarivien_raja_arvo__c>();
        Savepoint sp = Database.setSavepoint();
        nullExternalId(priceRows);

        try {
            for(Tuotteen_hintarivi__c priceRow : priceRows) {
                Tuotteen_hintarivi__c priceRowNew = New Tuotteen_hintarivi__c();
                Tuotteen_hintarivi__c priceRowReference = New Tuotteen_hintarivi__c();
                
                priceRowNew.Arvo__c = calculatePrice(priceRow.Arvo__c, rate);
                priceRowNew.Tuotteen_hinnasto__c = priceBookIdNew;
                priceRowNew.Korvausperuste_fin__c = priceRow.Korvausperuste_fin__c;
                priceRowNew.Korvausperuste_eng__c = priceRow.Korvausperuste_eng__c;
                priceRowNew.Korvausperuste_swe__c = priceRow.Korvausperuste_swe__c;
                priceRowNew.Hintariviselite__c = priceRow.Hintariviselite__c;
                priceRowNew.Hintaselite__c = priceRow.Hintaselite__c;
                priceRowNew.Hintatyyppi__c = priceRow.Hintatyyppi__c;
                priceRowNew.Nakyvyys__c = priceRow.Nakyvyys__c;
                priceRowNew.Rivinro__c = priceRow.Rivinro__c;
                priceRowNew.Tili__c = priceRow.Tili__c  ;
                priceRowNew.Yksikko__c = priceRow.Yksikko__c;
                priceRowNew.x_External_Id__c = priceRow.Id;
                priceRowNew.Id = null;
                priceRowsNew.add(priceRowNew);
                priceRowReference.x_External_Id__c = priceRow.Id;
                
                for(Tuotteen_hintarivien_raja_arvo__c priceLimit : priceRow.Tuotteen_hintarivien_raja_arvot__r) {
                    Tuotteen_hintarivien_raja_arvo__c priceLimitNew = New Tuotteen_hintarivien_raja_arvo__c ();
                    priceLimitNew.Alaraja__c = priceLimit.Alaraja__c;
                    priceLimitNew.Raja_arvo__c = priceLimit.Raja_arvo__c;
                    priceLimitNew.Tyyppi__c = priceLimit.Tyyppi__c;
                    priceLimitNew.Ylaraja__c = priceLimit.Ylaraja__c;
                    priceLimitNew.Tuotteen_hintarivi__r = priceRowReference;
                    priceLimitsNew.add(priceLimitNew);
                }
            }
    
            insert(priceRowsNew);
            Database.insert(priceLimitsNew);
            params[0].status = 'SUCCESS';
            
            return params;
        } 
        catch(Exception e) {
            Database.rollback(sp);
            params[0].status = 'ERROR';
            params[0].errorMsg = e.getMessage();
            return params;
        }
    }

    public static List<flowParams> modifyPrices(String priceBookId, Decimal rate, string priceType, List<flowParams> params) {

        List<Tuotteen_hintarivi__c> priceRows = queryPriceRows(priceBookId);
        List<Tuotteen_hintarivi__c> priceRowsNew = New List<Tuotteen_hintarivi__c>();
        for(Tuotteen_hintarivi__c priceRow : priceRows) {
            Tuotteen_hintarivi__c priceRowNew = New Tuotteen_hintarivi__c();
            priceRowNew.Id = priceRow.Id;
            priceRowNew.Arvo__c = calculatePrice(priceRow.Arvo__c, rate);
            if(priceType == 'All' || priceType == priceRow.x_Hintatyyppi_fin__c)
                priceRowsNew.add(priceRowNew);
        }
        Savepoint sp = Database.setSavepoint();
        try {
            update priceRowsNew;
            params[0].status = 'SUCCESS';
            return params;
        }
        catch(Exception e) {
            Database.rollback(sp); params[0].status = 'ERROR'; params[0].errorMsg = e.getMessage();
            return params;
        }

    }

    public class flowParams{
        @InvocableVariable(required=true)
        public String priceBookIdNew;

        @InvocableVariable(required=true)
        public String priceBookIdOld;
        
        @InvocableVariable(required=true)
        public Decimal rate;        

        @InvocableVariable(required=true)
        public String type;

        @InvocableVariable(required=true)
        public String pricetype;

        @InvocableVariable(required=false)
        public String status;

        @InvocableVariable(required=false)
        public String errorMsg;
    }

}