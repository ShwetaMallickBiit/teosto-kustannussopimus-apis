@isTest(seeAllData=true)
private class KAYT_Tuotteen_hinnastoHandlerTest {

    public static List<KAYT_Tuotteen_hinnastoHandler.flowParams> getflowParams(String type, String tuotekoodi1, String tuotekoodi2) {
        Tuote__c tOld = GEN_TestFactory.createTuote('Test product', tuotekoodi1, True);
        Tuote__c tNew = GEN_TestFactory.createTuote('Test product', tuotekoodi2, True);
        Tuotteen_hinnasto__c thOld = GEN_TestFactory.createTuotteenHinnasto(tOld.Id, True);
        Tuotteen_hinnasto__c thNew = GEN_TestFactory.createTuotteenHinnasto(tNew.Id, True);
        Tuotteen_hintarivi__c thr = GEN_TestFactory.createTuotteenHintarivi(thOld.Id, True);
        List<KAYT_Tuotteen_hinnastoHandler.flowParams> ps = New List<KAYT_Tuotteen_hinnastoHandler.flowParams>();
        KAYT_Tuotteen_hinnastoHandler.flowParams p = New KAYT_Tuotteen_hinnastoHandler.flowParams();
        p.priceBookIdNew = thNew.Id;
        p.priceBookIdOld = thOld.Id;
        p.rate = 10;
        p.Type = type;
        p.priceType = 'All';
        ps.add(p);
        return ps;
    }

    static testMethod void testSuccess() {
        Test.startTest();
        List<KAYT_Tuotteen_hinnastoHandler.flowParams> ps1 = getflowParams('KLOONI', 'T11', 'T12');
        List<KAYT_Tuotteen_hinnastoHandler.flowParams> ps2 = getflowParams('PAIVITYS', 'T21', 'T22');
        KAYT_Tuotteen_hinnastoHandler.cloneTuotteen_hinnasto(ps1);
        KAYT_Tuotteen_hinnastoHandler.cloneTuotteen_hinnasto(ps2);
        Test.stopTest();
        
    }
}