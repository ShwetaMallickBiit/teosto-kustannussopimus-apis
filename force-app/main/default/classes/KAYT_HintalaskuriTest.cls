@isTest
private class KAYT_HintalaskuriTest {
    
    @testSetup static void setup() {    
        insert new ExtranetAPI__c(Name='params',endpointUrl__c='https://extranet.teosto.fi/soratest/',value__c = '12345',priceRequestURL__c='products/prices/'); 
        
                Account account = new Account(
            Name = 'TEST FOR Unit tests LOREM IPSUM DOLOR SIT AMET',
            RecordTypeId = '0120Y000000a0p2QAA',
            ShippingStreet = 'Keilaranta 1 A',
            ShippingCity = 'Espoo',
            ShippingPostalCode = '02150',
            ShippingCountry = 'Finland',
            Kieli__c = 'Suomi',
            x_Piilota_asiakastili__c = false,
            Salainen_puhelinnro__c = false,
            Maksutapa__c = 'Sepa-maksu',
            ALV_velvollinen__c = false,
            VAT_numero__c = '2138825-1',
            Maksu_pysaytetty__c = 'Tilinumero virheellinen',
            Asiakasryhma__c = 'Käyttäjä',
            Postinimi__c = 'POSTINIMI',
            Ohita_Y_tunnus_Hetu_validointi__c = true,
            Lisaosoite__c = 'VIITE',
            Email__c = 'dev@biit.fi',
            Kuolinpesa__c = false,
            x_iSeriesDB2_synch_nf__c = false,
            Salainen_osoite__c = false,
            Salainen_email__c = false,
            x_Yhteystiedon_paattymispvm_suurin__c = Date.newInstance(2200,01,01),
            x_Saatavat_edut__c = '0',
            x_Tepa_synch_nf__c = true,
            HomePhone__c = '+358 44 7055063',
            x_Lahdeverollinen__c = false,
            Tietosuoja__c = false,
            Kotimaisuus__c = 'Kotimainen',
            Muutos__c = 'Tatjana Podvisocka, 10.10.2017 13:24',
            x_OnHenkiloItse__c = false,
            Luokittelu__c = 'Perus',
            x_OnKuolinpesa__c = false,
            x_Tepa_Perustaja__c = 'Fuse',
            x_Tepa_Perustamispvm__c = Date.newInstance(2017,10,04),
            onEri_kuin_postitusosoite__c = false,
            Arvonlisaverovelvollinen__c = true);
          insert account;
           
        // Create Contact
        Contact con = new Contact();
        con.LastName = 'Test Contact';
        con.AccountId= account.Id;
        con.email = 'test@test.com';
        con.x_Laheta_sahkopostit__c = TRUE;
        insert con; 
        
        AccountContactRelation_c__c accountcontactrelation_c = new AccountContactRelation_c__c(
            Name = 'MIKKO MIKKONEN',
            AccountId_c__c = account.Id,
            ContactId_c__c = con.Id,
            Id_c__c = '07k0E0000017juHQAQ',
            IsActive_c__c = true,
            IsDirect_c__c = true,
            Kaupunki_c__c = 'Kaupinki XXX',
            Lahiosoite_c__c = 'Osoite 1',
            Maa_Postitus_c__c = 'Finland',
            Postinumero_c__c = '00100',
            Postituksen_viite_c__c = 'Viite',
            Roles_c__c = 'Yhteyshenkilö',
            Sidoksen_email_c__c = 'mikko.mikkonen@biit.fi',
            Kaupunki_Kaynti_c__c = 'Helsinki',
            Kayntiosoitteen_viite__c = 'Viite',
            Lahiosoite_Kaynti_c__c = 'Osoite 2',
            Synkronoi_kayntiosoite__c = true,
            Synkronoi_postiosoite__c = true);
        insert accountcontactrelation_c;
        
        Sopimustieto__c sopimustieto = new Sopimustieto__c(
            RecordTypeId = '0120Y000000a0rmQAA',
            Alkupvm__c = Date.newInstance(2017,10,04),
            Tila__c = 'Avoin',
            Lisatiedot__c = 'Selite arvo',
            Sopimusvahvistus_lahetyspaiva__c = Date.newInstance(2017,10,30),
            x_Name__c = 'S-000004693',
            Luo_oikeusluokat__c = false,
            Maksuaika__c = 14.0,
            OnEri_kuin_postitusosoite__c = false,
            Vastuuhenkilo__c = accountcontactrelation_c.Id,
            Allekirjoittaja__c = accountcontactrelation_c.Id,
            x_Sidoksen_alkamispvm__c = Date.newInstance(2017,10,04),
            Asiakas_Sopimus__c = 'TEST FOR Unit tests / S-000004693',
            Asiakas_tekija__c = account.Id,
            Valittajatunnus__c = '11223344',
            OVT_tunnus__c = '33445566',
            Verkkopalvelutunnus__c = '9988776655',
            Laskutuksen_viite__c = '1234567890',
            Halytys__c = true,
            Sopimuksen_allekirjoituspvm__c = Date.newInstance(2017,10,04),
            Tyyppi__c = 'Sopimukseton',
            x_onTuoteosto__c = true);
            insert sopimustieto; 
           
            Arvolista__c arvolista = new Arvolista__c(
                Name = 'FINLAND',
                Ryhma__c = 'COUNTRY',
                Arvo_1_teksti__c = 'FINLAND',
                Arvo_2_teksti__c = 'FIN',
                Arvo_3_teksti__c = 'FI',
                Arvo_4_teksti__c = 'COUNTRY',
                Arvo_1_boolean__c = true,
                Arvo_2_boolean__c = false);
            insert arvolista;
            
            Esityspaikka__c esityspaikka = new Esityspaikka__c(
                Name = 'Test Karaoke',
                RecordTypeId = '0120Y000000a1LvQAI',
                Asiakas__c = account.Id,
                Maa__c = arvolista.Id,
                Postinumero__c = '00100',
                Postitoimipaikka__c = 'Helsinki',
                x_Alkupvm_sidos__c = Date.newInstance(2017,10,04),
                x_Sopimus_Uusi__c = sopimustieto.Id,
                Sopimus__c = sopimustieto.Id,
                Yleisokapasiteetti__c = 50.0,
                x_Alkupvm_sidos_viimeisin__c = Date.newInstance(2017,10,04),
                x_Aloita_sidos__c = true,
                x_Loppupvm_tuoteosto__c = Date.newInstance(2900,12,31),
                x_Loppuvm_sidos_viimeisin__c = Date.newInstance(2900,12,31),
                x_Paata_sidos__c = false);
            insert esityspaikka;
            
            Tuote__c tuote = new Tuote__c(
                Name = 'ELMU Kiinteä',
                Sisainen_selite__c = 'Varmista, että asiakas saa tunnukset.  Lippuhintaraja 1.1.2016-31.12.2017 on 28e eli 28,01e alkaen prosenttikorvauksella.',
                Tila__c = 'Myynnissä',
                Tuotenimi_eng__c = 'ELMU Kiinteä Englanniksi',
                Tuotenimi_swe__c = 'ELMU Kiinteä Ruotsiksi',
                Tuoteselite__c = '<p>Vakituinen esityspaikka, ykp 1-200: vähintään 10 tapahtumaa vuodessa, kiinteä hinta, raportointi ja laskutus kuukausittain. 3 % vuosialennus, jos kaikki tapahtumat ilmoitettu ajoissa.</p><p>Vakituinen esityspaikka, ykp 201-1500: vähintään 10 tapahtumaa vuodessa, kiinteä hinta, jos pääsylipun verollinen hinta alle 25€, prosenttiperusteinen hinta, jos pääsylipun verollinen hinta vähintään 25€, raportointi ja laskutus kuukausittain. 3 % vuosialennus, jos kaikki tapahtumat ilmoitettu ajoissa.</p>',
                Tuotetunnus__c = 'ELMU Kii',
                onEsityspaikka__c = true,
                Erityisehdot__c = '<p>Suomeksi</p>',
                Erityisehdot_eng__c = '<p>Englanniksi</p>',
                Erityisehdot_ruotsi__c = '<p>Ruotsiksi</p>',
                Tuoteselite_eng__c = '<p>Englanniksi</p>',
                Tuoteselite_swe__c = '<p>Ruotsiksi</p>');
            insert tuote;            
            
            Tuoteosto__c tuoteosto = new Tuoteosto__c(
                Alkupvm_laskutus__c = Date.newInstance(2017,10,05),
                Esityspaikka__c = Esityspaikka.Id,
                Alkupvm__c = Date.newInstance(2017,10,05),
                Sopimustieto__c = sopimustieto.Id,
                Tuote__c = tuote.Id,
                Asiakas__c = account.Id,
                x_Lupien_maara__c = 1.0,
                x_Jarjestys_sv__c = 1.0,
                x_OnTulosta_muutosvahvistukselle__c = true,
                x_OnTulosta_sopimusvahvistukselle__c = true,
                Integraatio_Tila_Hintakysely__c = 'SUCCESS');
            insert tuoteosto;
            
            Tuoterekisteriparametri__c tuoterekisteriparametri = new Tuoterekisteriparametri__c(
                Name = 'Yleisökapasiteetti',
                Luokka__c = 'Myyntiä ohjaava',
                Nakyy_asiakkaalle__c = true,
                Nimi_eng__c = 'Yleisökapasiteetti englanniksi',
                Nimi_swe__c = 'Yleisökapasiteetti ruotsiksi',
                Tunnus__c = 'YKP',
                Yksikko__c = 'henkeä',
                Yksikko_eng__c = 'henkeä englanniksi',
                Yksikko_swe__c = 'henkeä ruotsiksi');
            insert tuoterekisteriparametri;
            
            Tuoteoston_ohjausparametri__c tuoteoston_ohjausparametri = new Tuoteoston_ohjausparametri__c(
                Tuote__c = tuote.Id,
                Alaraja__c = 1.0,
                Jarjestys__c = 2.0,
                Muutettavissa_tuoteostolla__c = true,
                Oletusarvo__c = 200.0,
                Tuoterekisteriparametri__c = tuoterekisteriparametri.Id,
                Ylaraja__c = 2000.0,
                x_Tuoteostorivin_Record_Type_Id__c = '0120Y000000a1UOQAY');
            insert tuoteoston_ohjausparametri;
            
            Tuoteostorivi__c tuoteostorivi = new Tuoteostorivi__c(
                RecordTypeId = '0120Y000000a1UTQAY',
                Arvo__c = 1.0,
                Parametri__c = 'LUPAKPL',
                Tuoteoston_ohjausparametri__c = tuoteoston_ohjausparametri.Id,
                Tuoteosto__c = tuoteosto.Id);
            insert tuoteostorivi;
                
            Tuoteostorivi__c tuoteostorivi2 = new Tuoteostorivi__c(
                RecordTypeId = '0120Y000000a1UOQAY',
                Arvo__c = 200.0,
                Tuoteoston_ohjausparametri__c = tuoteoston_ohjausparametri.Id,
                Parametri__c = 'YKP',
                Tuoteosto__c = tuoteosto.Id);
            insert tuoteostorivi2;
        
    }

    private static testMethod void HintalaskuriCallTest() {

        Test.startTest();
        
        List<Tuoteosto__c> tuotLst = [SELECT Id FROM Tuoteosto__c Limit 1];
        ApexPages.StandardController sc = new ApexPages.StandardController(tuotLst[0]);
            
        Test.setMock(HttpCalloutMock.class, new KAYT_HintalaskuriMock());          
        KAYT_Hintalaskuri controller = new KAYT_Hintalaskuri(sc);
        controller.getData();
        KAYT_Hintalaskuri.updateHintarivit(tuotLst[0].Id);
        KAYT_Hintalaskuri.updateRecord(tuotLst[0].Id);
               
        Test.stopTest();
        
        List<Tuoteostohintarivi__c> hinnat = [SELECT Id FROM Tuoteostohintarivi__c];
        System.assertEquals(hinnat.size()>0,true);
        
        List<Tuoteosto__c> tuot= [SELECT Id, Integraatio_Tila_Hintakysely__c, Integraatio_Virheviesti_Hintakysely__c FROM Tuoteosto__c WHERE Id = :tuotLst[0].Id LIMIT 1];
//        System.assertEquals(tuot[0].Integraatio_Tila_Hintakysely__c,'ERROR');
//        System.assertEquals(tuot[0].Integraatio_Tila_Hintakysely__c,'SUCCESS');
        System.assertEquals(tuot[0].Integraatio_Tila_Hintakysely__c,'NOPRICEROWS');

    }
    
    private static testMethod void HintalaskuriPDFCallTest() {
        
        KAYT_Hintalaskuri.flowParams fPar = new KAYT_Hintalaskuri.flowParams();
        fPar.productId = '12345';
        fPar.priceDate = Date.newInstance(2017,10,05);
        fPar.param1 = 'param1';
        fPar.param2 = 'param2';
        fPar.param3 = 'param3';
        fPar.param1Value = 1;
        fPar.param2Value = 2;
        fPar.param3Value = 3;
        fPar.priceRow1 = '10000';
        fPar.priceRow1 = '20000';
        fPar.priceRow1 = '30000';
        fPar.priceRow1 = '40000';
        fPar.priceRow1 = '50000';
        
        List<KAYT_Hintalaskuri.flowParams> flowParamsList = new List<KAYT_Hintalaskuri.flowParams>();
        flowParamsList.add(fPar);

        Test.startTest();       
            
        Test.setMock(HttpCalloutMock.class, new KAYT_HintalaskuriMock());          
        
        List<KAYT_Hintalaskuri.flowParams> pdfSend = KAYT_Hintalaskuri.pdfLahetys_flow(flowParamsList);
        
        Test.stopTest();
        System.assertNOTEquals(flowParamsList[0].status,'ERROR');
        System.assertNOTEquals(pdfSend[0].priceRow1,null);
        System.assertEquals(pdfSend[0].priceRow2,'Jos lipunhinta on vähintään 99 euroa 9.990000 % lipputuloista');

    }
    
    private static testMethod void HintalaskuriPDFCallFailTest() {
        
        KAYT_Hintalaskuri.flowParams fPar = new KAYT_Hintalaskuri.flowParams();
        
        List<KAYT_Hintalaskuri.flowParams> flowParamsList = new List<KAYT_Hintalaskuri.flowParams>();
        flowParamsList.add(fPar);

        Test.startTest();       
            
        //Test.setMock(HttpCalloutMock.class, new KAYT_HintalaskuriMock());          
        
        List<KAYT_Hintalaskuri.flowParams> pdfSend = KAYT_Hintalaskuri.pdfLahetys_flow(flowParamsList);
        
        Test.stopTest();
        System.assertEquals(flowParamsList[0].status,'ERROR');
        System.assertEquals(pdfSend[0].priceRow1,null);

    }
}