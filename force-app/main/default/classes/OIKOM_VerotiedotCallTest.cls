@isTest(SeeAllData=true) 
private class OIKOM_VerotiedotCallTest {

    private static testMethod void VeroTiedotCallTest() {
        
        List<Verotieto__c> vt = createVerotiedotData();
        List<String> vtId = new List<String>();
        vtId.add(vt[0].Id);
        
        OIKOM_VeroTietoJSON vtjson = new OIKOM_VeroTietoJSON();
        vtjson.lvero = 0.00;
        vtjson.rowid = 666;
        vtjson.asnro = 12345;
        vtjson.alkpvm = null;
        vtjson.paatpvm = null;
                
        Test.startTest();
            
        Test.setMock(HttpCalloutMock.class, new OIKOM_VerotiedotCallMock());          
        OIKOM_VeroTiedotCall.updateVerotiedotData(vtId);
        
        Test.stopTest();
         List<Verotieto__c> rec = [SELECT Id, 
                                        x_Asiakasnumero_Tepa_formula__c,
                                        x_Vuosi_verokysely__c,
                                        Ansiotulo_brutto__c,
                                        Ansiotulo_vero_osuus__c,
                                        Lahdevero_brutto__c,
                                        Paaomatulo_brutto__c,
                                        Paaomatulo_vero_osuus__c,
                                        Veroton__c,
                                        Verotieto__c,
                                        Maakoodi_verokysely__c FROM Verotieto__c WHERE Id=:vt[0].Id];
        System.assertEquals(rec[0].Id,vt[0].Id);
        System.AssertNotEquals (rec[0].Ansiotulo_vero_osuus__c,vt[0].Ansiotulo_vero_osuus__c);
    }
    
    private static testMethod void UpdateVeroTiedotTest() {
        
        List<Verotieto__c> vt = createVerotiedotData();
        List<String> vtId = new List<String>();
        vtId.add(vt[0].Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(vt[0]);
        OIKOM_VerotiedotCall testVTCall = new OIKOM_VerotiedotCall(sc);
                
        Test.startTest();
            
        Test.setMock(HttpCalloutMock.class, new OIKOM_VerotiedotCallMock());          
        OIKOM_VeroTiedotCall.updateVerotiedot(vtId[0]);
        
        Test.stopTest();
        
        List<Verotieto__c> rec = [SELECT Id, 
                                        x_Asiakasnumero_Tepa_formula__c,
                                        x_Vuosi_verokysely__c,
                                        Ansiotulo_brutto__c,
                                        Ansiotulo_vero_osuus__c,
                                        Lahdevero_brutto__c,
                                        Paaomatulo_brutto__c,
                                        Paaomatulo_vero_osuus__c,
                                        Veroton__c,
                                        Verotieto__c,
                                        Maakoodi_verokysely__c FROM Verotieto__c WHERE Id=:vt[0].Id];
        System.assertEquals(rec[0].Id,vt[0].Id);
        System.AssertNotEquals (rec[0].Ansiotulo_vero_osuus__c,vt[0].Ansiotulo_vero_osuus__c);
    }
    
        private static testMethod void GetDataTest() {
        
        List<Verotieto__c> vt = createVerotiedotData();
        List<String> vtId = new List<String>();
        vtId.add(vt[0].Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(vt[0]);
        OIKOM_VerotiedotCall testVTCall = new OIKOM_VerotiedotCall(sc);
                
        Test.startTest();
            
        Test.setMock(HttpCalloutMock.class, new OIKOM_VerotiedotCallMock());          
        testVTCall.getData();
        
        Test.stopTest();
        
        List<Verotieto__c> rec = [SELECT Id, 
                                        x_Asiakasnumero_Tepa_formula__c,
                                        x_Vuosi_verokysely__c,
                                        Ansiotulo_brutto__c,
                                        Ansiotulo_vero_osuus__c,
                                        Lahdevero_brutto__c,
                                        Paaomatulo_brutto__c,
                                        Paaomatulo_vero_osuus__c,
                                        Veroton__c,
                                        Verotieto__c,
                                        Maakoodi_verokysely__c FROM Verotieto__c WHERE Id=:vt[0].Id];
        System.assertEquals(rec[0].Id,vt[0].Id);
        System.AssertNotEquals (rec[0].Ansiotulo_vero_osuus__c,vt[0].Ansiotulo_vero_osuus__c);
    }
    
        private static testMethod void UpdateRecordTest() {
        
        List<Verotieto__c> vt = createVerotiedotData();
        List<String> vtId = new List<String>();
        vtId.add(vt[0].Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(vt[0]);
        OIKOM_VerotiedotCall testVTCall = new OIKOM_VerotiedotCall(sc);
                
        Test.startTest();
            
        Test.setMock(HttpCalloutMock.class, new OIKOM_VerotiedotCallMock());          
        boolean recUpd = OIKOM_VeroTiedotCall.updateRecord(vtId[0]);
        
        Test.stopTest();
        
        System.assertEquals(recUpd,true);
    }
    
    private static List<Verotieto__c> createVerotiedotData()
    {
        String strRecordTypeId = [Select Id From RecordType Where SobjectType = 'Account' and Name = 'Henkilöasiakas'].Id;
        // Create an account 
        Account a = new Account();
        a.name = 'TestAccount';
        a.HETU__c='220398-991V';
        a.x_Verojakson_loppupvm__c = system.today().addYears(1);
        a.Ominaisuus__c = 'Säveltäjä';
        a.Asiakasnumero_Tepa__c = '73275';
        a.RecordTypeId=strRecordTypeId;
        insert a; 
        
        List<Verotieto__c> vtLst = new List<Verotieto__c>();
        
            Verotieto__c vt = new Verotieto__c();
            //vt.x_Asiakasnumero_Tepa_formula__c = Integer.valueOf('34017')+i;
            //vt.x_Vuosi_verokysely__c = System.Today().year();
            vt.Ansiotulo_brutto__c = 1546.76;
            vt.Ansiotulo_vero_osuus__c = 479.5;
            vt.Lahdevero_brutto__c = 18;
            vt.Paaomatulo_brutto__c = 1846.98;
            vt.Paaomatulo_vero_osuus__c = 23;
            vt.Veroton__c = false;
            vt.Verotieto__c = 'T';
            vt.Maakoodi_verokysely__c = '';
            vt.Asiakas__c = a.Id;
            vt.Lahdevero_brutto__c = 0;
            insert vt;
            vtLst.add(vt);
        
        return vtLst;
    }
}