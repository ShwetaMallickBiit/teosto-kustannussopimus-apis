@isTest(seeAllData=false)
private class MarkkinointiaktiviteettiTriggerTest{

    public static Campaign createCampaign(String campaignName, String status, String type, Date startDate, Date endDate) {
        Campaign camp = new Campaign(IsActive = true, Name = campaignName, EndDate = endDate, StartDate = startDate, 
                                     Status = status, Type = type);
        insert camp;
        return camp;
    }
    
    public static CampaignMember createCampaignMember(String campaignId, String contactId, Boolean doInsert) {
        CampaignMember cm = new CampaignMember(CampaignId = campaignId, ContactId = contactId);
        if(doInsert) insert cm;
        return cm;
    }
    
    public static Markkinointiaktiviteetti__c createMarkkinointiaktiviteetti(String campaignId, String campaignMemberId, String contactId, 
                                                                             String type, String email, Datetime activityDate, String subject, 
                                                                             String body, String link, Boolean emailOptOut, 
                                                                             String mailId, Boolean doInsert) {
    
        Markkinointiaktiviteetti__c ma = new Markkinointiaktiviteetti__c(Aihe__c = subject, Campaign_Id__c = campaignId, 
                                                                         Campaign_Member_Id__c = campaignMemberId, Contact_Id__c = contactId, 
                                                                         Email__c = email, EmailOptOut__c = emailOptOut, 
                                                                         Klikattu_linkki__c = link, Paivamaara__c = activityDate, 
                                                                         Sahkopostin_sisalto__c = body, Tyyppi__c = type,
                                                                         mail_Id__c = mailId);
        if(doInsert) insert ma;
        return ma;
    }

    static testMethod void testSuccess() {
        Account acc = GEN_TestFactory.createAccount();
        Contact con = GEN_TestFactory.createContact(acc.Id, 'Test', 'Testinen');
        Campaign camp = createCampaign('Email campaign 1', 'In progress', 'Email', date.Today(), date.Today() + 100);
        CampaignMember cm = createCampaignMember(camp.Id, con.Id, true);
        
        Test.startTest();       
        Markkinointiaktiviteetti__c  ma1 = createMarkkinointiaktiviteetti(camp.Id, cm.Id, con.Id, 'mail delivery', 
                                                                         'test.testinen@biit.fi', datetime.Now(), 'Welcome to Teosto', 
                                                                         'Lorem ipsum dolor sit amet', '', false, '123456', true);
        Markkinointiaktiviteetti__c  ma2 = createMarkkinointiaktiviteetti(camp.Id, cm.Id, con.Id, 'mailbounce', 
                                                                         'test.testinen@biit.fi', datetime.Now(), 'Welcome to Teosto', 
                                                                         'Lorem ipsum dolor sit amet', '', false, '123456', true);
        Markkinointiaktiviteetti__c  ma3 = createMarkkinointiaktiviteetti(camp.Id, cm.Id, con.Id, 'maillink', 
                                                                         'test.testinen@biit.fi', datetime.Now(), 'Welcome to Teosto', 
                                                                         'Lorem ipsum dolor sit amet', '', false, '123456', true);
        Test.stopTest();
    }
}