@isTest(seeAllData=false)
private class GEN_TapausAutoresponseTest{

    static testMethod void sendEmailSuccess() {

        // create account
        Account acc = new Account(
            Name = 'TestAccount', Kieli__c = 'Suomi', Asiakasryhma__c = 'Käyttäjä', Luokittelu__c ='Avain',
            Y_tunnus__c = '2647504-7', ShippingStreet = 'Testikatu 10', ShippingCity = 'Helsinki',
            ShippingPostalCode = '00100', RecordTypeId = '0120Y000000a0p2');
        insert acc;

        // create contact
        Contact con = new Contact(
            FirstName = '', LastName = 'Jono', AccountId = acc.Id);
        insert con;

        // create tapaus
        Tapaus__c tap = new Tapaus__c(
             Aihe__c = 'Testi aihe', Kuvaus__c = 'Lorem ipsum dolor sit amet', Alatyyppi__c = '', Alatyyppi_2__c = '');
        insert tap;

        //  Run test
        Test.startTest();
        GEN_TapausAutoresponse.emailParams param = new GEN_TapausAutoresponse.emailParams();
        param.tapausId = tap.Id;
        param.toContactId = con.Id;
        param.orgWideEmail= '0D20Y000000XcZc';
        param.emailTemplate = 'Autoreply_Tapahtumat';
        param.kieli = 'FIN';
        param.toEmail = 'dev@biit.fi';
        List<GEN_TapausAutoresponse.emailParams> params = new List<GEN_TapausAutoresponse.emailParams>();
        params.add(param);
        GEN_TapausAutoresponse.emailLahetys_flow(params);
        Test.stopTest();
    }
}